import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { AppearanceProvider } from 'react-native-appearance';
import { Provider } from 'react-redux';
import Toast from 'react-native-toast-message';
import { Host } from 'react-native-portalize';

import { ThemeProvider } from '~/utils/ThemeContext';
import { LocalizationProvider } from '~/utils/i18n';
import store from '~/redux/store';

import SplashScreen from '~/screens/SplashScreen';
import DevTestScreen from '~/screens/DevTestScreen';
import LoginScreen from '~/screens/LoginScreen';
import SignUpScreen from '~/screens/SignUpScreen';
import ForgotPasswordScreen from '~/screens/ForgotPasswordScreen';
import VerificationScreen from '~/screens/VerificationScreen';

import WithBottomTabScreen from '~/screens/WithBottomTabScreen';
import NotificationScreen from '~/screens/NotificationScreen';
import SearchFoodScreen from '~/screens/SearchFoodScreen';
import EditProfileScreen from '~/screens/EditProfileScreen';
import MealsScreen from '~/screens/MealsScreen';
import SidesScreen from '~/screens/SidesScreen';
import BeveragesScreen from '~/screens/BeveragesScreen';
import DessertsScreen from '~/screens/DessertsScreen';

const Stack = createStackNavigator();

const App = () => {
  return (
    <Provider store={store}>
      <AppearanceProvider>
        <LocalizationProvider>
          <ThemeProvider>
            <NavigationContainer>
              <Host>
                <Stack.Navigator initialRouteName="AuthStack">
                  <Stack.Screen
                    name="AuthStack"
                    component={AuthStack}
                    options={{ headerShown: false }}
                  />
                  <Stack.Screen
                    name="InAppStack"
                    component={InAppStack}
                    options={{ headerShown: false }}
                  />
                </Stack.Navigator>
              </Host>
              <Toast ref={(ref) => Toast.setRef(ref)} />
            </NavigationContainer>
          </ThemeProvider>
        </LocalizationProvider>
      </AppearanceProvider>
    </Provider>
  );
};

const authStackList = [
  {
    name: 'SplashScreen',
    component: SplashScreen,
  },
  {
    name: 'DevTestScreen',
    component: DevTestScreen,
  },
  {
    name: 'LoginScreen',
    component: LoginScreen,
  },
  {
    name: 'SignUpScreen',
    component: SignUpScreen,
  },
  {
    name: 'ForgotPasswordScreen',
    component: ForgotPasswordScreen,
  },
  {
    name: 'VerificationScreen',
    component: VerificationScreen,
  },
];

const AuthStack = () => {
  const screens = () => {
    return authStackList.map((item, index) => (
      <Stack.Screen
        key={index}
        name={item.name}
        component={item.component}
        options={{ headerShown: false }}
      />
    ));
  };

  return (
    <Stack.Navigator
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      {screens()}
    </Stack.Navigator>
  );
};

const inAppStackList = [
  {
    name: 'WithBottomTabScreen',
    component: WithBottomTabScreen,
  },
  {
    name: 'NotificationScreen',
    component: NotificationScreen,
  },
  {
    name: 'SearchFoodScreen',
    component: SearchFoodScreen,
  },
  {
    name: 'EditProfileScreen',
    component: EditProfileScreen,
  },
  {
    name: 'MealsScreen',
    component: MealsScreen,
  },
  {
    name: 'SidesScreen',
    component: SidesScreen,
  },
  {
    name: 'BeveragesScreen',
    component: BeveragesScreen,
  },
  {
    name: 'DessertsScreen',
    component: DessertsScreen,
  },
];

const InAppStack = () => {
  const screens = () => {
    return inAppStackList.map((item, index) => (
      <Stack.Screen
        key={index}
        name={item.name}
        component={item.component}
        options={{ headerShown: false }}
      />
    ));
  };

  return (
    <Stack.Navigator
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS,
      }}
    >
      {screens()}
    </Stack.Navigator>
  );
};

export default App;
