import { StyleSheet, Dimensions } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors } = useTheme();
  return StyleSheet.create({
    welcomeWrapper: {
      marginTop: normalize(30),
      marginHorizontal: normalize(40),
    },
    welcomeTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(24),
      color: '#FFFFFF',
      includeFontPadding: false,
      textAlign: 'center',
    },
    welcomeSubtitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(16),
      color: '#FFFFFF',
      includeFontPadding: false,
      textAlign: 'center',
      marginTop: normalize(8),
    },
    keyboardScrollView: {
      width: Dimensions.get('window').width,
    },
    formEmail: {
      width: '100%',
      marginTop: normalize(50),
    },
    formPassword: {
      width: '100%',
      marginTop: normalize(15),
    },
    loginOpt: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: normalize(10),
    },
    checkboxLabel: {
      fontFamily: 'Nunito-Regular',
      color: '#FFFFFF',
      marginLeft: normalize(10),
    },
    buttonTextOnly: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    submitWrapper: {
      marginTop: normalize(20),
    },
    loginWrapper: {
      alignItems: 'center',
      marginTop: normalize(20),
    },
    loginBtn: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
    },
    loginLabel: {
      color: colors.primary,
      marginLeft: normalize(5),
    },
  });
};
