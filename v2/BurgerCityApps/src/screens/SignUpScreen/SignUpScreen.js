import React, { useContext } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';

import BaseIntroScreen from '~/components/BaseIntroScreen';
import ButtonBlock from '~/components/ButtonBlock';
import InputboxIcon from '~/components/InputboxIcon';

import { LocalizationContext } from '~/utils/i18n';
import styles from './styles';

const SignUpScreen = () => {
  const {
    translations,
    initializeAppLanguage,
  } = useContext(LocalizationContext);
  initializeAppLanguage();
  const navigation = useNavigation();

  const onLogin = () => {
    navigation.navigate('LoginScreen');
  };

  return (
    <BaseIntroScreen>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={styles().keyboardScrollView}
      >
        <View style={styles().welcomeWrapper}>
          <Text style={styles().welcomeTitle}>
            {translations.signup['welcome-title']}
          </Text>
          <Text style={styles().welcomeSubtitle}>
            {translations.signup['welcome-subtitle']}
          </Text>
          <View style={styles().formEmail}>
            <InputboxIcon
              icon={<Fontisto name="email" size={20} color="#92A1B1" />}
              blurOnSubmit
              keyboardType="email-address"
              textContentType="emailAddress"
              placeholder={translations.signup.email}
              placeholderTextColor="#92A1B1"
            />
          </View>
          <View style={styles().formPassword}>
            <InputboxIcon
              icon={<Fontisto name="locked" size={20} color="#92A1B1" />}
              blurOnSubmit
              secureTextEntry
              textContentType="newPassword"
              placeholder={translations.signup.password}
              placeholderTextColor="#92A1B1"
            />
          </View>
          <View style={styles().formPassword}>
            <InputboxIcon
              icon={<Fontisto name="locked" size={20} color="#92A1B1" />}
              blurOnSubmit
              secureTextEntry
              textContentType="newPassword"
              placeholder={translations.signup['password-confirm']}
              placeholderTextColor="#92A1B1"
            />
          </View>
          <View style={styles().submitWrapper}>
            <ButtonBlock
              label={translations.signup.next}
            />
          </View>
          <View style={styles().loginWrapper}>
            <TouchableOpacity onPress={onLogin}>
              <View style={styles().loginBtn}>
                <Text style={styles().buttonTextOnly}>
                  {translations.signup['old-user']}
                </Text>
                <Text style={{...styles().buttonTextOnly, ...styles().loginLabel}}>
                  {translations.signup.login}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </BaseIntroScreen>
  );
};

export default SignUpScreen;
