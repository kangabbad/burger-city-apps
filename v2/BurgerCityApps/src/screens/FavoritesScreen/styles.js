import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors, isDark } = useTheme();

  return StyleSheet.create({
    favoriteStyle: {
      backgroundColor: colors.background,
    },
    favoriteContentContainer: {
      padding: normalize(20),
    },
    favoriteItem: {
      flexDirection: 'row',
      alignItems: 'center',
      borderRadius: normalize(10),
      backgroundColor: isDark ? '#191919' : '#FFFFFF',
      paddingVertical: normalize(20),
      paddingHorizontal: normalize(20),
      marginVertical: normalize(10),
      marginHorizontal: normalize(20),
      elevation: 3,
    },
    favoriteItemInfo: {
      marginLeft: normalize(25),
      marginRight: 'auto',
    },
    favoriteItemTitle: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
    },
    favoriteItemPrice: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.primary,
      includeFontPadding: false,
      marginTop: normalize(5),
    },
  });
};
