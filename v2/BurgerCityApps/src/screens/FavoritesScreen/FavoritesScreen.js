import React, { useState } from 'react';
import { FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Toast from 'react-native-toast-message';

import styles from './styles';
import PrimaryHeader from '~/components/Header/PrimaryHeader';
import { CurrencyFormat } from '~/utils/helpers';

import beefBurgerImg from '~/assets/images/beef-burger.webp';

const FavoritesScreen = () => {
  const [favoriteData, setFavoriteData] = useState([
    {
      imgUrl: beefBurgerImg,
      name: 'Beef Burger',
      price: 50000,
      isFavorite: true,
    },
    {
      imgUrl: beefBurgerImg,
      name: 'Beef Burger',
      price: 50000,
      isFavorite: true,
    },
    {
      imgUrl: beefBurgerImg,
      name: 'Beef Burger',
      price: 50000,
      isFavorite: true,
    },
  ]);

  const onChangeFavorite = (index) => {
    const copyFavorites = [...favoriteData];
    const text1 = copyFavorites[index].isFavorite ? 'Success Remove!' : 'Success Added!';
    const text2 = copyFavorites[index].isFavorite ? `${copyFavorites[index].name} has been removed from favorites.` : `${copyFavorites[index].name} has been added from favorites.`;
    const type = 'info';
    Toast.show({ position: 'bottom', type, text1, text2 });
    copyFavorites[index].isFavorite = !copyFavorites[index].isFavorite;
    setFavoriteData(copyFavorites);
  };

  return (
    <FlatList
      ListHeaderComponent={<PrimaryHeader />}
      data={favoriteData}
      keyExtractor={(item, index) => item + index.toString()}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
      style={styles().favoriteStyle}
      renderItem={({ item, index }) => (
        <FavoriteItem
          {...item}
          onChangeFavorite={() => onChangeFavorite(index)}
        />
      )}
    />
  );
};

const FavoriteItem = (props) => {
  return (
    <View style={styles().favoriteItem}>
      <Image
        source={props.imgUrl}
        resizeMode="cover"
      />
      <View style={styles().favoriteItemInfo}>
        <Text style={styles().favoriteItemTitle}>
          {props.name}
        </Text>
        <CurrencyFormat value={props.price} customStyles={styles().favoriteItemPrice} />
      </View>
      <TouchableOpacity onPress={props.onChangeFavorite}>
        <FontAwesome5
          solid
          name="star"
          size={20}
          color={props.isFavorite ? '#FF9F1C' : '#BFBFBF'}
        />
      </TouchableOpacity>
    </View>
  );
};

export default FavoritesScreen;
