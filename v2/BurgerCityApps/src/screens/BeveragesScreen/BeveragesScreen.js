import React, { useContext } from 'react';

import { LocalizationContext } from '~/utils/i18n';

import SelectedMenuScreen from '../SelectedMenuScreen';

import bannerBeverages from '~/assets/images/banner-beverages.webp';
import pepsiGlass from '~/assets/images/pepsi-glass.webp';

const SidesScreen = () => {
  const { translations } = useContext(LocalizationContext);

  const menuList = [
    {
      imgUrl: pepsiGlass,
      name: 'Pepsi',
      price: 10000,
      isNew: true,
    },
    {
      imgUrl: pepsiGlass,
      name: 'Pepsi',
      price: 10000,
      isNew: true,
    },
    {
      imgUrl: pepsiGlass,
      name: 'Pepsi',
      price: 10000,
      isNew: true,
    },
  ];

  return (
    <SelectedMenuScreen
      bannerImg={bannerBeverages}
      menuTitle={translations['beverages-menu'].title}
      menuTags={translations['beverages-menu'].tags}
      menuList={menuList}
    />
  );
};

export default SidesScreen;
