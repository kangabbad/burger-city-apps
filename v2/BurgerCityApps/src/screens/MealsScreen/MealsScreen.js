import React, { useContext } from 'react';

import { LocalizationContext } from '~/utils/i18n';

import SelectedMenuScreen from '../SelectedMenuScreen';

import bannerBurger from '~/assets/images/banner-meals.webp';
import chickenBigBurger from '~/assets/images/chicken-big-burger.webp';

const MealsScreen = () => {
  const { translations } = useContext(LocalizationContext);

  const menuList = [
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
    {
      imgUrl: chickenBigBurger,
      name: 'Cheesy Burger',
      price: 50000,
      isNew: true,
    },
  ];

  return (
    <SelectedMenuScreen
      bannerImg={bannerBurger}
      menuTitle={translations['meals-menu'].title}
      menuTags={translations['meals-menu'].tags}
      menuList={menuList}
    />
  );
};

export default MealsScreen;
