import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors } = useTheme();

  return StyleSheet.create({
    homeStyle: {
      backgroundColor: colors.background,
    },
    carouselItem: {
      height: 250,
      padding: normalize(30),
    },
    carouselTitle: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(28),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    paginationContainer: {
      position: 'absolute',
      bottom: normalize(20),
      left: normalize(30),
      borderRadius: normalize(30),
      justifyContent: 'flex-start',
      backgroundColor: 'rgba(255, 255, 255, 0.35)',
      paddingVertical: normalize(5),
      paddingHorizontal: normalize(5),
    },
    paginationDotContainer: {
      marginHorizontal: normalize(6),
    },
    pagintaionDot: {
      width: 8,
      height: 8,
      borderRadius: 8,
      backgroundColor: 'rgba(255, 255, 255, 0.92)',
    },
    ticketContainer: {
      paddingVertical: normalize(15),
      paddingHorizontal: normalize(20),
    },
    ticketWrapper: {
      borderRadius: normalize(15),
      backgroundColor: '#303030',
      height: normalize(90),
      marginVertical: normalize(10),
      overflow: 'hidden',
    },
    ticketCircleContainer: {
      position: 'absolute',
      top: 0,
      right: 0,
      bottom: 0,
      left: 0,
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: '100%',
      marginHorizontal: normalize(-20),
    },
    ticketCircle: {
      borderRadius: normalize(100),
      backgroundColor: colors.background,
      height: normalize(48),
      width: normalize(48),
    },
    ticketContent: {
      flexDirection: 'row',
      alignItems: 'center',
      height: '100%',
      paddingLeft: normalize(40),
    },
    ticketIcon: {
      height: normalize(45),
      width: normalize(45),
    },
    ticketDescWrapper: {
      marginLeft: normalize(20),
    },
    ticketTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(20),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    ticketSubtitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    productListContainer: {
      marginBottom: normalize(20),
    },
    productListTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(20),
      color: colors.text,
      includeFontPadding: false,
      marginHorizontal: normalize(20),
    },
    productList: {
      paddingHorizontal: 10,
    },
    productListItem: {
      marginHorizontal: normalize(10),
      marginTop: normalize(20),
    },
    productListItemTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
      marginTop: normalize(10),
    },
    productListItemRating: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: normalize(5),
    },
    productListItemRatingItem: {
      marginRight: normalize(5),
    },
    productListItemPrice: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
      marginTop: normalize(10),
    },
  });
};
