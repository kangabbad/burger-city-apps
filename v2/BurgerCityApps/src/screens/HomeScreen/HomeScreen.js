import React, { useContext, useRef, useState } from 'react';
import { Dimensions, Text, ImageBackground, View, Image, FlatList } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import { CurrencyFormat } from '~/utils/helpers';
import PrimaryHeader from '~/components/Header/PrimaryHeader';
import bannerSlider from '~/assets/images/slider-image.png';
import styles from './styles';

import { LocalizationContext } from '~/utils/i18n';
import burgerCityLogo from '~/assets/icons/burger-city-logo.webp';
import image1 from '~/assets/images/image-1.png';

const sliderWidth = Dimensions.get('window').width;
const itemWidth = Dimensions.get('window').width;

const HomeScreen = () => {
  return (
    <FlatList
      ListHeaderComponent={<PrimaryHeader />}
      data={[0, 1, 2]}
      keyExtractor={(item, index) => item + index.toString()}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
      style={styles().homeStyle}
      renderItem={({ item, index }) => {
        switch (index) {
          case 0:
            return <BannerCarousel />;
          case 1:
            return <Tickets />;
          case 2:
            return <BestOffers />;
          default:
            break;
        }
      }}
    />
  );
};

const BannerCarousel = () => {
  const carouselRef = useRef(null);
  const { translations } = useContext(LocalizationContext);
  const [activeSlide, setActiveSlide] = useState(0);

  const slides = [
    {
      title: translations.home['banner-title'],
      imgUrl: bannerSlider,
    },
    {
      title: translations.home['banner-title'],
      imgUrl: bannerSlider,
    },
    {
      title: translations.home['banner-title'],
      imgUrl: bannerSlider,
    },
  ];

  return (
    <View>
      <Carousel
        ref={carouselRef}
        data={slides}
        loop
        renderItem={({ item, index }) => <CarouselItem key={index} {...item} />}
        sliderWidth={sliderWidth}
        itemWidth={itemWidth}
        inactiveSlideScale={1}
        onSnapToItem={(index) => setActiveSlide(index)}
      />
      <Pagination
        dotsLength={slides.length}
        activeDotIndex={activeSlide}
        containerStyle={styles().paginationContainer}
        dotContainerStyle={styles().paginationDotContainer}
        dotStyle={styles().pagintaionDot}
        inactiveDotOpacity={0.5}
        inactiveDotScale={0.9}
      />
    </View>
  );
};

const CarouselItem = (props) => {
  return (
    <ImageBackground
      source={props.imgUrl}
      resizeMode="cover"
      style={styles().carouselItem}
    >
      <Text style={styles().carouselTitle}>
        {props.title}
      </Text>
    </ImageBackground>
  );
};

const Tickets = () => {
  const { translations } = useContext(LocalizationContext);

  const data = [
    {
      title: translations.home['track-here-title'],
      subtitle: translations.home['track-here-subtitle'],
    },
    {
      title: translations.home['order-here-title'],
      subtitle: translations.home['order-here-subtitle'],
    },
  ];

  return (
    <View style={styles().ticketContainer}>
      <FlatList
        data={data}
        keyExtractor={(item, index) => item + index.toString()}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item }) => (
          <TicketItem {...item} />
        )}
      />
    </View>
  );
};

const TicketItem = (props) => {
  return (
    <View style={styles().ticketWrapper}>
      <View style={styles().ticketCircleContainer}>
        <View style={styles().ticketCircle} />
        <View style={styles().ticketCircle} />
      </View>
      <View style={styles().ticketContent}>
        <Image
          source={burgerCityLogo}
          resizeMode="contain"
          style={styles().ticketIcon}
        />
        <View style={styles().ticketDescWrapper}>
          <Text style={styles().ticketTitle}>
            {props.title}
          </Text>
          <Text style={styles().ticketSubtitle}>
            {props.subtitle}
          </Text>
        </View>
      </View>
    </View>
  );
};

const BestOffers = () => {
  const { translations } = useContext(LocalizationContext);

  const data = [
    {
      imgUrl: image1,
      name: 'Cheesy Burger',
      rating: 4,
      price: 50000,
    },
    {
      imgUrl: image1,
      name: 'Cheesy Burger',
      rating: 4,
      price: 50000,
    },
    {
      imgUrl: image1,
      name: 'Cheesy Burger',
      rating: 4,
      price: 50000,
    },
    {
      imgUrl: image1,
      name: 'Cheesy Burger',
      rating: 4,
      price: 50000,
    },
    {
      imgUrl: image1,
      name: 'Cheesy Burger',
      rating: 4,
      price: 50000,
    },
  ];

  return (
    <View style={styles().productListContainer}>
      <Text style={styles().productListTitle}>
        {translations.home['best-offer']}
      </Text>
      <FlatList
        data={data}
        keyExtractor={(item, index) => item + index.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles().productList}
        renderItem={({ item }) => (
          <OfferItem {...item} />
        )}
      />
    </View>
  );
};

const OfferItem = (props) => {
  return (
    <View style={styles().productListItem}>
      <Image
        source={props.imgUrl}
        resizeMode="contain"
      />
      <Text style={styles().productListItemTitle}>
        {props.name}
      </Text>
      <View style={styles().productListItemRating}>
        {[...Array(props.rating)].map((_, index) => (
          <FontAwesome5 key={index} solid name="star" size={12} color="#FF9F1C" style={styles().productListItemRatingItem} />
        ))}
      </View>
      <CurrencyFormat value={props.price} customStyles={styles().productListItemPrice} />
    </View>
  );
};

export default HomeScreen;
