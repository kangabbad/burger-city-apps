import React, { useContext, useEffect, useRef, useState } from 'react';
import {
  Animated,
  Dimensions,
  Text,
  View,
} from 'react-native';
// import { useSelector } from 'react-redux';
import BootSplash from 'react-native-bootsplash';
import { useNavigation } from '@react-navigation/native';

import { LocalizationContext } from '~/utils/i18n';
import BaseIntroScreen from '~/components/BaseIntroScreen';
import ButtonBlock from '~/components/ButtonBlock';

import bootSplashLogo from '~/assets/icons/app-icon-android/burger_city_round.png';

import styles from './styles';

const SplashScreen = () => {
  return (
    <BaseIntroScreen>
      <Content />
      <BootAnimation />
    </BaseIntroScreen>
  );
};

const fakeApiCallWithoutBadNetwork = (ms) => {
  return new Promise((resolve) => setTimeout(resolve, ms));
};

const BootAnimation = () => {
  const [isSplashVisible, onToggleSplashVisible] = useState(true);
  const [isSplashLogoLoaded, onToggleSplashLogo] = useState(false);
  const opacity = useRef(new Animated.Value(1));
  const translateY = useRef(new Animated.Value(0));

  const init = async () => {
    BootSplash.hide();
    // You can uncomment this line to add a delay on app startup
    await fakeApiCallWithoutBadNetwork(2000);

    const useNativeDriver = true;
    Animated.stagger(250, [
      Animated.spring(translateY.current, { useNativeDriver, toValue: -50 }),
      Animated.spring(translateY.current, {
        useNativeDriver,
        toValue: Dimensions.get('window').height,
      }),
    ]).start();

    Animated.timing(opacity.current, {
      useNativeDriver,
      toValue: 0,
      duration: 150,
      delay: 350,
    }).start(() => {
      onToggleSplashVisible(false);
    });
  };

  useEffect(() => {
    isSplashLogoLoaded && init();
  }, [isSplashLogoLoaded]);

  if (isSplashVisible) {
    return (
      <Animated.View
        style={[
          styles().bootsplash,
          { opacity: opacity.current },
        ]}
      >
        <Animated.Image
          source={bootSplashLogo}
          fadeDuration={0}
          onLoadEnd={() => onToggleSplashLogo(true)}
          style={[
            styles().logo,
            { transform: [{ translateY: translateY.current }] },
          ]}
        />
      </Animated.View>
    );
  }

  return null;
};

const Content = () => {
  // const homeReducer = useSelector(state => state.homeReducer);
  const navigation = useNavigation();
  const { initializeAppLanguage, translations } = useContext(LocalizationContext);
  initializeAppLanguage();

  const onPressStart = () => {
    navigation.navigate('LoginScreen');
  };

  return (
    <>
      <Text style={styles().contentTitle}>
        {translations.splash['intro-title']}
      </Text>
      <View style={styles().buttonWrapper}>
        <ButtonBlock
          label={translations.splash['intro-btn']}
          onPress={onPressStart}
        />
      </View>
    </>
  );
};

export default SplashScreen;
