import { Platform, StatusBar, StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';

export default () => {
  const marginTopLogo = Platform.OS === 'android'
    ? StatusBar.currentHeight + normalize(35)
    : normalize(50);

  return StyleSheet.create({
    bootsplash: {
      ...StyleSheet.absoluteFill,
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#242424',
    },
    logo: {
      height: normalize(125),
      width: normalize(125),
    },
    bgOverlay: {
      backgroundColor: '#242424',
      opacity: 0.35,
    },
    contentContainer: {
      flex: 1,
      alignItems: 'center',
    },
    contentLogo: {
      height: normalize(85),
      width: normalize(75),
      marginTop: marginTopLogo,
    },
    contentTitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(30),
      color: '#FFFFFF',
      marginTop: 'auto',
      marginRight: 'auto',
      marginBottom: normalize(75),
      marginLeft: normalize(35),
    },
    buttonWrapper: {
      width: '85%',
      marginBottom: normalize(30),
    },
  });
};
