import React, { useContext, useEffect, useState } from 'react';
import { FlatList, Image, ImageBackground, Switch, Text, TouchableOpacity, View } from 'react-native';

import styles from './styles';
import ProfileHeader from '~/components/Header/ProfileHeader';
import { CurrencyFormat } from '~/utils/helpers';
import { LocalizationContext } from '~/utils/i18n';
import { useTheme } from '~/utils/ThemeContext';

import ProfilePictureImg from '~/assets/images/profile-picture.webp';
import goPayCardImg from '~/assets/images/bg-gopay.webp';
import goPayLogo from '~/assets/icons/gopay.png';
import linkAjaCardImg from '~/assets/images/bg-linkaja.webp';
import linkAjaLogo from '~/assets/icons/linkaja.webp';
import danaCardImg from '~/assets/images/bg-dana.webp';
import danaLogo from '~/assets/icons/dana.webp';
import ovoCardImg from '~/assets/images/bg-ovo.webp';
import ovoLogo from '~/assets/icons/ovo.webp';

const ProfilesScreen = () => {
  return (
    <FlatList
      ListHeaderComponent={<ProfileHeader />}
      data={[0, 1, 2, 3, 4]}
      keyExtractor={(item, index) => item + index.toString()}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
      style={styles().profileStyle}
      renderItem={({ item, index }) => {
        switch (index) {
          case 0:
            return <ProfileInfo />;
          case 1:
            return <DarkModeBtn />;
          case 2:
            return <Divider />;
          case 3:
            return <DebitCards />;
          case 4:
            return <Transactions />;
          default:
            break;
        }
      }}
    />
  );
};

const ProfileInfo = () => {
  return (
    <View style={styles().profileInfo}>
      <Image
        source={ProfilePictureImg}
        resizeMode="contain"
        style={styles().profileInfoImg}
      />
      <Text style={styles().profileInfoName}>
        Kang Abbad
      </Text>
      <Text style={styles().profileInfoEmail}>
        0877-3999-9776
      </Text>
      <Text style={styles().profileInfoEmail}>
        naufalabbad9@gmail.com
      </Text>
    </View>
  );
};

const DarkModeBtn = () => {
  const { translations } = useContext(LocalizationContext);
  const { setScheme, isDark, colors } = useTheme();
  const [isActive, setActive] = useState(isDark);

  const onToggleTheme = () => {
    isActive ? setScheme('dark') : setScheme('light');
  };

  useEffect(() => {
    setTimeout(() => {
      onToggleTheme();
    }, 1000);
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isActive]);

  return (
    <View style={styles().darkModeContainer}>
      <Text style={styles().darkModeTitle}>
        {translations.profile['dark-mode']}
      </Text>
      <View style={styles().darkModeBtnWrapper}>
        <Switch
          trackColor={{ false: '#BFBFBF', true: colors.primary }}
          thumbColor={isDark ? '#F5DD4B' : '#F4F4F4'}
          ios_backgroundColor="#3e3e3e"
          onValueChange={() => setActive(!isActive)}
          value={isActive}
          style={styles().darkModeBtn}
        />
      </View>
    </View>
  );
};

const Divider = () => {
  return (
    <View style={styles().divider} />
  );
};

const DebitCards = () => {
  const { translations } = useContext(LocalizationContext);

  const dataDummy = [
    {
      id: 0,
      eWallet: {
        bgImgUrl: goPayCardImg,
        logoUrl: goPayLogo,
        name: 'GoPay',
      },
      name: 'Kang Abbad',
      phoneNumber: '0877-3999-9776',
      balance: 125000,
    },
    {
      id: 1,
      eWallet: {
        bgImgUrl: linkAjaCardImg,
        logoUrl: linkAjaLogo,
        name: 'LinkAja',
      },
      name: 'Kang Abbad',
      phoneNumber: '0877-3999-9776',
      balance: 125000,
    },
    {
      id: 2,
      eWallet: {
        bgImgUrl: danaCardImg,
        logoUrl: danaLogo,
        name: 'Dana',
      },
      name: null,
      phoneNumber: null,
      balance: null,
    },
    {
      id: 3,
      eWallet: {
        bgImgUrl: ovoCardImg,
        logoUrl: ovoLogo,
        name: 'OVO',
      },
      name: null,
      phoneNumber: null,
      balance: null,
    },
  ];

  return (
    <>
      <Text style={styles().debitCardTitle}>
        {translations.profile['your-wallet']}
      </Text>
      <FlatList
        data={dataDummy}
        keyExtractor={(item, index) => item + index.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles().debitCardContainer}
        renderItem={({ item }) => (
          <WalletCard {...item} />
        )}
      />
    </>
  );
};

const WalletCard = (props) => {
  const { translations } = useContext(LocalizationContext);

  const balanceInfo = () => {
    if (props.balance) {
      return (
        <View>
          <Text style={styles().debitCardBalanceTitle}>
            {translations.profile.balance}
          </Text>
          <CurrencyFormat value={props.balance} customStyles={styles().debitCardBalanceAmount} />
        </View>
      );
    } else {
      return null;
    }
  };

  const userWalletInfo = () => {
    if (props.phoneNumber || props.name) {
      return (
        <View>
          <Text style={styles().debitCardInfoPhone}>
            {props.phoneNumber}
          </Text>
          <Text style={styles().debitCardInfoName}>
            {props.name}
          </Text>
        </View>
      );
    } else {
      return (
        <TouchableOpacity onPress={() => {}}>
          <Text style={styles().debitCardLoginBtn}>
            {translations.profile['click-here']} {'\n'}
            {translations.profile['to-login']}
          </Text>
        </TouchableOpacity>
      );
    }
  };

  return (
    <ImageBackground
      source={props.eWallet.bgImgUrl}
      style={styles().debitCardWallet}
    >
      <>
        <View style={styles().debitCardWalletTopSection}>
          {balanceInfo()}
          <Text style={styles().debitCardWalletName}>
            {props.eWallet.name}
          </Text>
        </View>
        <View style={styles().debitCardWalletBottomSection}>
          {userWalletInfo()}
          <View style={styles().debitCardLogoWrapper}>
            <Image source={props.eWallet.logoUrl} resizeMode="contain" style={styles().debitCardLogo} />
          </View>
        </View>
      </>
    </ImageBackground>
  );
};

const Transactions = () => {
  const { translations } = useContext(LocalizationContext);

  return (
    <View style={styles().transactions}>
      <Text style={styles().transactionTitle}>
        {translations.profile['recent-transaction']}
      </Text>

      <View style={styles().transactionItem}>
        <View>
          <Text style={styles().transactionItemDate}>
            20/07/2020
          </Text>
          <Text style={styles().transactionItemTitle}>
            Chicken Big Burger
          </Text>
        </View>
        <CurrencyFormat value={50000} customStyles={styles().transactionItemPrice} />
      </View>
      <View style={styles().transactionItem}>
        <View>
          <Text style={styles().transactionItemDate}>
            20/07/2020
          </Text>
          <Text style={styles().transactionItemTitle}>
            Chicken Big Burger
          </Text>
        </View>
        <CurrencyFormat value={50000} customStyles={styles().transactionItemPrice} />
      </View>
    </View>
  );
};

export default ProfilesScreen;
