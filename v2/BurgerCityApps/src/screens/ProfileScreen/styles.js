import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors, isDark } = useTheme();

  return StyleSheet.create({
    profileStyle: {
      backgroundColor: colors.background,
    },
    profileInfo: {
      alignItems: 'center',
      padding: normalize(20),
    },
    profileInfoImg: {
      height: normalize(100),
      width: normalize(100),
    },
    profileInfoName: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(22),
      color: colors.text,
      includeFontPadding: false,
      textAlign: 'center',
      marginTop: normalize(20),
    },
    profileInfoEmail: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#BFBFBF',
      includeFontPadding: false,
      textAlign: 'center',
      letterSpacing: 0.75,
      marginTop: normalize(5),
    },
    darkModeContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      paddingVertical: normalize(10),
    },
    darkModeTitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
      marginLeft: normalize(20),
    },
    darkModeBtnWrapper: {
      borderLeftWidth: 1,
      borderColor: isDark ? '#393939' : '#ECECEC',
      paddingVertical: normalize(8),
      paddingHorizontal: normalize(15),
    },
    divider: {
      backgroundColor: isDark ? '#191919' : '#F0F0F0',
      height: normalize(15),
    },
    debitCardTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
      padding: normalize(20),
    },
    debitCardContainer: {
      paddingHorizontal: normalize(10),
    },
    debitCardWallet: {
      borderRadius: normalize(10),
      overflow: 'hidden',
      height: normalize(150),
      width: normalize(300),
      paddingVertical: normalize(15),
      paddingHorizontal: normalize(20),
      marginHorizontal: normalize(10),
    },
    debitCardWalletTopSection: {
      flexDirection: 'row',
    },
    debitCardBalanceTitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    debitCardBalanceAmount: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(18),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    debitCardWalletName: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(26),
      color: '#FFFFFF',
      includeFontPadding: false,
      marginLeft: 'auto',
    },
    debitCardWalletBottomSection: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginTop: 'auto',
    },
    debitCardInfoPhone: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(16),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    debitCardInfoName: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    debitCardLoginBtn: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(18),
      color: '#FFFFFF',
      includeFontPadding: false,
      lineHeight: normalize(20),
    },
    debitCardLogoWrapper: {
      borderRadius: normalize(25),
      backgroundColor: '#FFFFFF',
      padding: normalize(8),
    },
    debitCardLogo: {
      height: normalize(25),
      width: normalize(25),
    },
    transactions: {
      padding: normalize(20),
    },
    transactionItem: {
      borderRadius: normalize(5),
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: isDark ? '#191919' : '#FFFFFF',
      padding: normalize(15),
      marginTop: normalize(15),
      shadowColor: '#121212',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 3,
    },
    transactionTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
    },
    transactionItemDate: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: isDark ? '#595959' : '#BFBFBF',
      includeFontPadding: false,
    },
    transactionItemTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
      marginTop: normalize(5),
    },
    transactionItemPrice: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(16),
      color: colors.primary,
      includeFontPadding: false,
    },
  });
};
