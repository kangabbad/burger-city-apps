import React, { useContext } from 'react';

import { LocalizationContext } from '~/utils/i18n';

import SelectedMenuScreen from '../SelectedMenuScreen';

import bannerSalads from '~/assets/images/banner-salads.webp';
import whitePurpleSalad from '~/assets/images/white-purple-salad.webp';

const SidesScreen = () => {
  const { translations } = useContext(LocalizationContext);

  const menuList = [
    {
      imgUrl: whitePurpleSalad,
      name: 'White Purple Salads',
      price: 25000,
      isNew: true,
    },
    {
      imgUrl: whitePurpleSalad,
      name: 'White Purple Salads',
      price: 25000,
      isNew: true,
    },
    {
      imgUrl: whitePurpleSalad,
      name: 'White Purple Salads',
      price: 25000,
      isNew: true,
    },
  ];

  return (
    <SelectedMenuScreen
      bannerImg={bannerSalads}
      menuTitle={translations['salads-menu'].title}
      menuTags={translations['salads-menu'].tags}
      menuList={menuList}
    />
  );
};

export default SidesScreen;
