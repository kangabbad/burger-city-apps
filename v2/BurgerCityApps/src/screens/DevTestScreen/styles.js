import { StyleSheet } from 'react-native';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors } = useTheme();

  return StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    },
    title: {
      fontSize: 18,
      color: colors.text,
      marginBottom: 20,
    },
    btnI18n: {
      borderRadius: 5,
      backgroundColor: '#eaeaea',
      paddingVertical: 10,
      paddingHorizontal: 10,
      marginTop: 25,
    },
  });
};
