import React, { useContext, useState } from 'react';
import { Switch, Text, TouchableOpacity } from 'react-native';
import { Screen } from '~/components/Screen';
import { useTheme } from '~/utils/ThemeContext';
import { LocalizationContext } from '~/utils/i18n';

import styles from './styles';

const DevTestScreen = () => {
  const [count, setCount] = useState(0);
  const { setScheme, isDark } = useTheme();

  const {
    translations,
    appLanguage,
    setAppLanguage,
    initializeAppLanguage,
  } = useContext(LocalizationContext);
  initializeAppLanguage();

  const onToggleTheme = () => {
    isDark ? setScheme('light') : setScheme('dark');
  };

  const onChangeLang = () => {
    if (appLanguage === 'en') {
      setAppLanguage('id');
      setCount(count + 1);
    } else if (appLanguage === 'id') {
      setAppLanguage('en');
      setCount(count + 1);
    }
  };

  const title = isDark ? translations.darkModeTitle : translations.lightModeTitle;
  const btnTitle = appLanguage === 'en' ? 'Ganti ke Indonesia' : 'Change to English';

  return (
    <Screen containerStyle={styles().container}>
      <Text style={styles().title}>
        {title}
      </Text>
      <Switch
        trackColor={{ false: '#767577', true: '#81b0ff' }}
        thumbColor={isDark ? '#f5dd4b' : '#f4f3f4'}
        ios_backgroundColor="#3e3e3e"
        onValueChange={onToggleTheme}
        value={isDark}
      />
      <TouchableOpacity
        style={styles().btnI18n}
        onPress={onChangeLang}
      >
        <Text>{btnTitle}</Text>
      </TouchableOpacity>
    </Screen>
  );
};

export default DevTestScreen;
