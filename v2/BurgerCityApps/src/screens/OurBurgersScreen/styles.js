import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors, isDark } = useTheme();

  return StyleSheet.create({
    ourBurgersStyle: {
      backgroundColor: colors.background,
    },
    searchBarBtn: {
      borderRadius: normalize(10),
      borderWidth: 1,
      borderColor: isDark ? '#393939' : '#FFFFFF',
      backgroundColor: colors.background,
      flexDirection: 'row',
      alignItems: 'center',
      paddingVertical: normalize(14),
      paddingHorizontal: normalize(16),
      marginVertical: normalize(20),
      marginHorizontal: normalize(30),
      elevation: 3,
    },
    searchBarBtnText: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
      marginLeft: normalize(20),
    },
    bannerCarouselItem: {
      overflow: 'hidden',
      borderRadius: normalize(10),
      height: normalize(200),
    },
    menus: {
      flexDirection: 'row',
      alignItems: 'center',
      flexWrap: 'wrap',
      paddingVertical: normalize(20),
      paddingHorizontal: normalize(30),
    },
    menuItem: {
      borderRadius: normalize(10),
      overflow: 'hidden',
      flexDirection: 'column',
      alignItems: 'center',
      backgroundColor: isDark ? '#191919' : '#FFFFFF',
      width: 'auto',
      paddingTop: normalize(5),
      paddingBottom: normalize(25),
      margin: normalize(10),
      elevation: 3,
    },
    menuItemIcon: {
      height: normalize(100),
      width: normalize(100),
    },
    menuItemText: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
    },
  });
};
