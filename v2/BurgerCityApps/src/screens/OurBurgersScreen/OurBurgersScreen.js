import React, { useContext } from 'react';
import { Dimensions, FlatList, Image, Text, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Carousel from 'react-native-snap-carousel';
import normalize from 'react-native-normalize';
import { Col, Grid, Row } from 'react-native-easy-grid';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';
import { LocalizationContext } from '~/utils/i18n';
import { useTheme } from '~/utils/ThemeContext';

import PrimaryHeader from '~/components/Header/PrimaryHeader';

import bannerImg from '~/assets/images/banner.png';
import mealsImg from '~/assets/images/meals.webp';
import saladImg from '~/assets/images/white-purple-salad.webp';
import dessertImg from '~/assets/images/dessert.webp';
import baveragesImg from '~/assets/images/baverages.webp';

const OurBurgersScreen = () => {
  return (
    <FlatList
      ListHeaderComponent={<PrimaryHeader />}
      data={[0, 1, 2]}
      keyExtractor={(item, index) => item + index.toString()}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
      style={styles().ourBurgersStyle}
      renderItem={({ item, index }) => {
        switch (index) {
          case 0:
            return <SearchBarBtn />;
          case 1:
            return <BannerCarousel />;
          case 2:
            return <Menus />;
          default:
            break;
        }
      }}
    />
  );
};

const SearchBarBtn = () => {
  const { colors } = useTheme();
  const navigation = useNavigation();
  const { translations } = useContext(LocalizationContext);

  const onPress = () => {
    navigation.navigate('InAppStack', { screen: 'SearchFoodScreen' });
  };

  return (
    <TouchableOpacity activeOpacity={0.95} onPress={onPress}>
      <View style={styles().searchBarBtn}>
        <AntDesign name="search1" color={colors.text} size={18} />
        <Text style={styles().searchBarBtnText}>
          {translations['our-burgers']['search-burger']}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const BannerCarousel = () => {
  const sliderWidth = Dimensions.get('window').width;
  const itemWidth = Dimensions.get('window').width - normalize(60);

  return (
    <Carousel
      data={[0, 1, 2]}
      loop
      sliderWidth={sliderWidth}
      itemWidth={itemWidth}
      sliderHeight={normalize(200)}
      itemHeight={normalize(200)}
      inactiveSlideOpacity={0.5}
      renderItem={({ item, index }) => <BannerItem key={index} {...item} />}
    />
  );
};

const BannerItem = () => {
  return (
    <View style={styles().bannerCarouselItem}>
      <Image source={bannerImg} resizeMode="contain" />
    </View>
  );
};

const Menus = () => {
  const { translations } = useContext(LocalizationContext);
  const navigation = useNavigation();

  const onPress = (screen) => {
    navigation.push('InAppStack', { screen });
  };

  return (
    <View style={styles().menus}>
      <Grid>
        <Row>
          <Col>
            <TouchableOpacity style={styles().menuItem} onPress={() => onPress('MealsScreen')}>
              <Image source={mealsImg} resizeMode="contain" resizeMethod="resize" style={styles().menuItemIcon} />
              <Text style={styles().menuItemText}>
                {translations['our-burgers']['value-meals']}
              </Text>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity style={styles().menuItem} onPress={() => onPress('SidesScreen')}>
              <Image source={saladImg} resizeMode="contain" resizeMethod="resize" style={styles().menuItemIcon} />
              <Text style={styles().menuItemText}>
              {translations['our-burgers']['salads-sides']}
              </Text>
            </TouchableOpacity>
          </Col>
        </Row>
        <Row>
          <Col>
            <TouchableOpacity style={styles().menuItem} onPress={() => onPress('DessertsScreen')}>
              <Image source={dessertImg} resizeMode="contain" resizeMethod="resize" style={styles().menuItemIcon} />
              <Text style={styles().menuItemText}>
                {translations['our-burgers'].desserts}
              </Text>
            </TouchableOpacity>
          </Col>
          <Col>
            <TouchableOpacity style={styles().menuItem} onPress={() => onPress('BeveragesScreen')}>
              <Image source={baveragesImg} resizeMode="contain" resizeMethod="resize" style={styles().menuItemIcon} />
              <Text style={styles().menuItemText}>
                {translations['our-burgers'].baverages}
              </Text>
            </TouchableOpacity>
          </Col>
        </Row>
      </Grid>
    </View>
  );
};

export default OurBurgersScreen;
