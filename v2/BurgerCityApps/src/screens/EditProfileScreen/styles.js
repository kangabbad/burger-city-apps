import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors, isDark } = useTheme();

  return StyleSheet.create({
    editProfileStyle: {
      backgroundColor: colors.background,
    },
    profilePhoto: {
      alignItems: 'center',
      padding: normalize(20),
    },
    profileImg: {
      alignItems: 'flex-end',
      justifyContent: 'flex-end',
      height: normalize(100),
      width: normalize(100),
    },
    profileImgBtn: {
      borderRadius: normalize(32),
      backgroundColor: '#FFFFFF',
      height: normalize(32),
      width: normalize(32),
      alignItems: 'center',
      justifyContent: 'center',
      elevation: 5,
    },
    profileInfo: {
      paddingHorizontal: normalize(15),
    },
    profileInfoTitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
    },
    profileForm: {
      marginTop: normalize(15),
    },
    inputBox: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.text,
      borderBottomWidth: 1,
      borderColor: isDark ? '#292929' : '#ECECEC',
      paddingVertical: normalize(5),
    },
  });
};
