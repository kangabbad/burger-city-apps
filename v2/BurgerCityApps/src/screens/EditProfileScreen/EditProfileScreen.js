import React, { useContext } from 'react';
import { FlatList, ImageBackground, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import normalize from 'react-native-normalize';

import { useTheme } from '~/utils/ThemeContext';
import { LocalizationContext } from '~/utils/i18n';

import styles from './styles';
import EditProfileHeader from '~/components/Header/EditProfileHeader';

import ProfilePictureImg from '~/assets/images/profile-picture.webp';

const ProfilesScreen = () => {
  return (
    <FlatList
      ListHeaderComponent={<EditProfileHeader />}
      data={[0, 1]}
      keyExtractor={(item, index) => item + index.toString()}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
      style={styles().editProfileStyle}
      renderItem={({ item, index }) => {
        switch (index) {
          case 0:
            return <ProfilePhoto />;
          case 1:
            return <ProfileInfo />;
          default:
            break;
        }
      }}
    />
  );
};

const ProfilePhoto = () => {
  const { colors } = useTheme();

  return (
    <View style={styles().profilePhoto}>
      <ImageBackground
        source={ProfilePictureImg}
        resizeMode="contain"
        style={styles().profileImg}
      >
        <TouchableOpacity style={styles().profileImgBtn} onPress={() => {}}>
          <Entypo name="camera" size={normalize(20)} color={colors.primary} />
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

const ProfileInfo = () => {
  const { translations } = useContext(LocalizationContext);

  return (
    <View style={styles().profileInfo}>
      <View style={styles().profileForm}>
        <Text style={styles().profileInfoTitle}>
          {translations.profile['full-name']}
        </Text>
        <TextInput
          style={styles().inputBox}
          defaultValue="Kang Abbad"
        />
      </View>
      <View style={styles().profileForm}>
        <Text style={styles().profileInfoTitle}>
          {translations.profile.email}
        </Text>
        <TextInput
          style={styles().inputBox}
          defaultValue="naufalabbad9@gmail.com"
        />
      </View>
      <View style={styles().profileForm}>
        <Text style={styles().profileInfoTitle}>
          {translations.profile.phone}
        </Text>
        <TextInput
          style={styles().inputBox}
          defaultValue="085156630969"
        />
      </View>
    </View>
  );
};

export default ProfilesScreen;
