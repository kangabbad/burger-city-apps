import React, { useContext } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import normalize from 'react-native-normalize';

import { useTheme } from '~/utils/ThemeContext';
import { LocalizationContext } from '~/utils/i18n';

import HomeScreen from '~/screens/HomeScreen';
import OurBurgersScreen from '~/screens/OurBurgersScreen';
import FavoritesScreen from '~/screens/FavoritesScreen';
import ProfileScreen from '~/screens/ProfileScreen';

const Tab = createBottomTabNavigator();

const WithBottomTabScreen = () => {
  const { colors, isDark } = useTheme();
  const { translations, initializeAppLanguage } = useContext(LocalizationContext);
  initializeAppLanguage();

  const tabBarOptions = {
    activeTintColor: colors.primary,
    inactiveTintColor: '#727C8E',
    keyboardHidesTabBar: true,
    labelStyle: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(12),
      includeFontPadding: false,
      bottom: normalize(7),
    },
    style: {
      borderTopColor: colors.background,
      backgroundColor: isDark ? '#191919' : '#FFFFFF',
      height: normalize(55),
    },
  };

  const tabScreen = [
    {
      name: 'HomeScreen',
      component: HomeScreen,
      options: {
        tabBarLabel: translations['bottom-tab'].home,
        tabBarIcon: ({ color }) => (
          <Ionicons name="home" size={normalize(22)} color={color} />
        ),
      },
    },
    {
      name: 'OurBurgersScreen',
      component: OurBurgersScreen,
      options: {
        tabBarLabel: translations['bottom-tab']['our-burgers'],
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="hamburger" size={normalize(24)} color={color} />
        ),
      },
    },
    {
      name: 'FavoritesScreen',
      component: FavoritesScreen,
      options: {
        tabBarLabel: translations['bottom-tab'].favorites,
        tabBarIcon: ({ color }) => (
          <FontAwesome5 solid name="star" size={normalize(18)} color={color} />
        ),
      },
    },
    {
      name: 'ProfileScreen',
      component: ProfileScreen,
      options: {
        tabBarLabel: translations['bottom-tab'].profile,
        tabBarIcon: ({ color }) => (
          <MaterialCommunityIcons name="account-circle" size={normalize(24)} color={color} />
        ),
      },
    },
  ];

  return (
    <Tab.Navigator lazy tabBarOptions={tabBarOptions}>
      {tabScreen.map((screen, screenIndex) => (
        <Tab.Screen
          key={screenIndex}
          name={screen.name}
          component={screen.component}
          options={screen.options}
        />
      ))}
    </Tab.Navigator>
  );
};

export default WithBottomTabScreen;
