import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { isDark, colors } = useTheme();

  return StyleSheet.create({
    notificationStyle: {
      backgroundColor: colors.background,
    },
    trackOrderContainer: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      backgroundColor: colors.primary,
      paddingVertical: normalize(15),
      paddingHorizontal: normalize(15),
    },
    trackOrderInfo: {
      flexGrow: 2,
      flexShrink: 1,
      paddingRight: normalize(20),
    },
    trackOrderIconWrapper: {
      flexGrow: 1,
      flexShrink: 0,
      flexDirection: 'row',
      justifyContent: 'flex-end',
    },
    trackOrderTitle: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(14),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    trackOrderSubtitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(12),
      color: '#FFFFFF',
      includeFontPadding: false,
      marginTop: normalize(5),
    },
    notifItem: {
      borderBottomWidth: 1,
      borderColor: isDark ? '#121212' : '#ECECEC',
      backgroundColor: isDark ? '#292929' : '#FFFFFF',
      paddingVertical: normalize(15),
      paddingHorizontal: normalize(20),
    },
    notifItemTitle: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
    },
    notifItemSubtitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(12),
      color: colors.text,
      includeFontPadding: false,
      marginTop: normalize(5),
    },
  });
};
