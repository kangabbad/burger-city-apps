import React, { useContext } from 'react';
import { FlatList, Text, View } from 'react-native';

import { LocalizationContext } from '~/utils/i18n';

import styles from './styles';
import PrimaryHeader from '~/components/Header/PrimaryHeader';

import TrackOrderIcon from '~/assets/icons/track-order-white.svg';

const NotificationScreen = () => {
  return (
    <FlatList
      ListHeaderComponent={<PrimaryHeader />}
      data={[0, 1]}
      keyExtractor={(item, index) => item + index.toString()}
      stickyHeaderIndices={[0]}
      showsVerticalScrollIndicator={false}
      style={styles().notificationStyle}
      renderItem={({ item, index }) => {
        switch (index) {
          case 0:
            return <TrackOrder />;
          case 1:
            return <NotifList />;
          default:
            break;
        }
      }}
    />
  );
};

const TrackOrder = () => {
  const { translations } = useContext(LocalizationContext);

  return (
    <View style={styles().trackOrderContainer}>
      <View style={styles().trackOrderInfo}>
        <Text style={styles().trackOrderTitle}>
          {translations.notification['track-order']['order-otw']}
        </Text>
        <Text style={styles().trackOrderSubtitle}>
          13 July 2020 10:00
        </Text>
      </View>
      <View style={styles().trackOrderIconWrapper}>
        <TrackOrderIcon height={32} width={32} />
      </View>
    </View>
  );
};

const NotifList = () => {
  const data = [
    {
      title: 'New Menu On Burger',
      date: '10 July 2020',
    },
    {
      title: 'New Menu On Burger',
      date: '10 July 2020',
    },
    {
      title: 'New Menu On Burger',
      date: '10 July 2020',
    },
  ];
  return (
    <FlatList
      data={data}
      keyExtractor={(item, index) => item + index.toString()}
      renderItem={({ item }) => (
        <NotifItem {...item} />
      )}
    />
  );
};

const NotifItem = ({ title, date }) => {
  return (
    <View style={styles().notifItem}>
      <Text style={styles().notifItemTitle}>
        {title}
      </Text>
      <Text style={styles().notifItemSubtitle}>
        {date}
      </Text>
    </View>
  );
};

export default NotificationScreen;
