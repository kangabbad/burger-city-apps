import React, { useContext } from 'react';
import { Text, View } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';

import BaseIntroScreen from '~/components/BaseIntroScreen';
import ButtonBlock from '~/components/ButtonBlock';

import { LocalizationContext } from '~/utils/i18n';
import styles from './styles';

const VerificationScreen = () => {
  const {
    translations,
    initializeAppLanguage,
  } = useContext(LocalizationContext);
  initializeAppLanguage();

  return (
    <BaseIntroScreen>
      <View style={styles().overlay}>
        <View style={styles().container}>
          <View style={styles().box}>
            <View style={styles().checkContainer}>
              <View style={styles().checkWhiteWrapper}>
                <View style={styles().checkPrimaryWrapper}>
                  <Feather name="check" size={60} color="#FFFFFF" />
                </View>
              </View>
            </View>
            <Text style={styles().boxTitle}>
              {translations.verification.sending}...
            </Text>
            <Text style={styles().boxDesc}>
              {translations.verification['send-desc']}
            </Text>
            <View style={styles().submitWrapper}>
              <ButtonBlock
                label={translations.verification.done}
              />
            </View>
          </View>
        </View>
      </View>
    </BaseIntroScreen>
  );
};

export default VerificationScreen;
