import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors } = useTheme();

  return StyleSheet.create({
    overlay: {
      ...StyleSheet.absoluteFill,
      backgroundColor: 'rgba(0, 0, 0, .5)',
    },
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
    },
    box: {
      borderRadius: normalize(10),
      backgroundColor: '#FFFFFF',
      width: normalize(275),
      paddingHorizontal: normalize(30),
      paddingTop: normalize(50),
      paddingBottom: normalize(25),
    },
    boxTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(20),
      color: '#121212',
      textAlign: 'center',
      textTransform: 'uppercase',
      includeFontPadding: false,
    },
    boxDesc: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(16),
      color: '#121212',
      textAlign: 'center',
      includeFontPadding: false,
      marginTop: normalize(8),
    },
    checkContainer: {
      ...StyleSheet.absoluteFill,
      top: normalize(-50),
      alignItems: 'center',
    },
    checkWhiteWrapper: {
      borderRadius: normalize(50),
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: '#FFFFFF',
      height: normalize(100),
      width: normalize(100),
    },
    checkPrimaryWrapper: {
      borderRadius: normalize(50),
      backgroundColor: colors.primary,
      alignItems: 'center',
      justifyContent: 'center',
      height: normalize(80),
      width: normalize(80),
    },
    submitWrapper: {
      marginTop: normalize(20),
    },
  });
};
