import React, { useContext, useState } from 'react';
import { Text, TouchableOpacity, View } from 'react-native';
import CircleCheckBox, { LABEL_POSITION } from 'react-native-checkbox-circle';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';

import BaseIntroScreen from '~/components/BaseIntroScreen';
import ButtonBlock from '~/components/ButtonBlock';
import InputboxIcon from '~/components/InputboxIcon';

import { LocalizationContext } from '~/utils/i18n';
import styles from './styles';

const LoginScreen = () => {
  const {
    translations,
    initializeAppLanguage,
  } = useContext(LocalizationContext);
  initializeAppLanguage();

  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const navigation = useNavigation();

  const onForgotPassword = () => {
    navigation.navigate('ForgotPasswordScreen');
  };

  const onLogin = () => {
    navigation.navigate('InAppStack', { screen: 'HomeScreen' });
  };

  const onSignUp = () => {
    navigation.navigate('SignUpScreen');
  };

  return (
    <BaseIntroScreen>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={styles().keyboardScrollView}
      >
        <View style={styles().welcomeWrapper}>
          <Text style={styles().welcomeTitle}>
            {translations.login['welcome-title']}
          </Text>
          <Text style={styles().welcomeSubtitle}>
            {translations.login['welcome-subtitle']}
          </Text>
          <View style={styles().formEmail}>
            <InputboxIcon
              icon={<Fontisto name="email" size={20} color="#92A1B1" />}
              blurOnSubmit
              secureTextEntry
              keyboardType="email-address"
              textContentType="emailAddress"
              placeholder={translations.login.email}
              placeholderTextColor="#92A1B1"
            />
          </View>
          <View style={styles().formPassword}>
            <InputboxIcon
              icon={<Fontisto name="locked" size={20} color="#92A1B1" />}
              blurOnSubmit
              secureTextEntry
              textContentType="password"
              placeholder={translations.login.password}
              placeholderTextColor="#92A1B1"
            />
          </View>
          <View style={styles().loginOpt}>
            <CircleCheckBox
              checked={toggleCheckBox}
              onToggle={(checked) => setToggleCheckBox(checked)}
              labelPosition={LABEL_POSITION.RIGHT}
              label={translations.login['remember-me']}
              outerSize={14}
              filterSize={16}
              innerSize={13}
              filterColor="#FFFFFF"
              outerColor="#FFFFFF"
              innerColor="#FF9F1C"
              styleLabel={styles().checkboxLabel}
            />

            <TouchableOpacity onPress={onForgotPassword}>
              <Text style={styles().buttonTextOnly}>
                {translations.login['forgot-password']}
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles().submitWrapper}>
            <ButtonBlock
              label={translations.login.title}
              onPress={onLogin}
            />
          </View>
          <View style={styles().signupWrapper}>
            <TouchableOpacity onPress={onSignUp}>
              <View style={styles().signupBtn}>
                <Text style={styles().buttonTextOnly}>
                  {translations.login['new-user']}
                </Text>
                <Text style={{...styles().buttonTextOnly, ...styles().signupLabel}}>
                  {translations.login.signup}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </BaseIntroScreen>
  );
};

export default LoginScreen;
