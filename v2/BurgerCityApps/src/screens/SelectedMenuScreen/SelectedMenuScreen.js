import React, { useContext, useMemo, useRef, useState } from 'react';
import { FlatList, Image, ImageBackground, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';
import normalize from 'react-native-normalize';
import { useNavigation } from '@react-navigation/core';
import { Portal } from 'react-native-portalize';
import BottomSheet, { BottomSheetBackdrop } from '@gorhom/bottom-sheet';

import styles from './styles';
import { useTheme } from '~/utils/ThemeContext';
import { CurrencyFormat } from '~/utils/helpers';
import { LocalizationContext } from '~/utils/i18n';

import deliveryImg from '~/assets/images/delivery.png';

const SelectedMenuScreen = (props) => {
  return (
    <>
      <StatusBar
        animated
        translucent
        barStyle="light-content"
        backgroundColor="transparent"
      />
      <FlatList
        ListHeaderComponent={<Navbar />}
        data={[0, 1, 2, 3, 4]}
        keyExtractor={(item, index) => item + index.toString()}
        stickyHeaderIndices={[0]}
        showsVerticalScrollIndicator={false}
        style={styles().selectedMenu}
        renderItem={({ item, index }) => {
          switch (index) {
            case 0:
              return <Banner {...props} />;
            case 1:
              return <MenuIntro {...props} />;
            case 2:
              return <Divider {...props} />;
            case 3:
              return <MenuList {...props} />;
            default:
              break;
          }
        }}
      />
    </>
  );
};

const Navbar = () => {
  const navigation = useNavigation();

  return (
    <View style={styles().navbar}>
      <TouchableOpacity style={styles().navbarBackBtn} onPress={() => navigation.goBack()}>
        <AntDesign name="arrowleft" size={normalize(24)} color="#FFFFFF" />
      </TouchableOpacity>
    </View>
  );
};

const Banner = (props) => {
  return (
    <Image
      source={props.bannerImg}
      resizeMode="cover"
      style={styles().bannerImg}
    />
  );
};

const MenuIntro = (props) => {
  const { translations } = useContext(LocalizationContext);

  return (
    <View style={styles().menuIntro}>
      <Text style={styles().menuIntroTitle}>
        {props.menuTitle}
      </Text>
      <Text style={styles().menuTagTitle}>
        {props.menuTags.join(', ')}
      </Text>
      <View style={styles().dividerDash} />
      <View style={styles().menuInfoWrapper}>
        <Text style={styles().menuInfoTitle}>
          {translations['selected-menu']['delivery-cost']}
        </Text>
        <CurrencyFormat value={15000} customStyles={styles().menuInfoSubtitle} />
      </View>
      <View style={styles().menuInfoWrapper}>
        <Text style={styles().menuInfoTitle}>
          {translations['selected-menu']['available-promos']}
        </Text>
        <PromoBtn />
      </View>
    </View>
  );
};

const PromoBtn = () => {
  const { colors } = useTheme();
  const bottomSheetRef = useRef(null);
  const snapPoints = useMemo(() => [0, '50%'], []);

  const { translations } = useContext(LocalizationContext);

  const onOpen = () => {
    bottomSheetRef.current?.expand();
  };

  const dummyData = [
    {
      imgUrl: deliveryImg,
      name: 'Free Delivery Fee',
      valid: '30 Jul 2020',
    },
    {
      imgUrl: deliveryImg,
      name: 'Free Delivery Fee',
      valid: '30 Jul 2020',
    },
    {
      imgUrl: deliveryImg,
      name: 'Free Delivery Fee',
      valid: '30 Jul 2020',
    },
  ];

  return (
    <>
      <TouchableOpacity style={styles().menuInfoPromoBtn} onPress={onOpen}>
        <Text style={styles().menuInfoPromoTitle}>
          Free Delivery
        </Text>
        <Entypo name="colours" size={normalize(16)} color={colors.primary} />
      </TouchableOpacity>
      <Portal>
        <BottomSheet
          ref={bottomSheetRef}
          index={-1}
          animateOnMount
          snapPoints={snapPoints}
          backdropComponent={BottomSheetBackdrop}
          handleComponent={CustomLangModalHandle}
        >
          <View style={styles().promoPopupWrapper}>
            <Text style={styles().promoPopupTitle}>
              {translations['selected-menu'].title}
            </Text>
            <FlatList
              data={dummyData}
              keyExtractor={(item, index) => item + index.toString()}
              showsVerticalScrollIndicator={false}
              style={styles().promoPopupList}
              renderItem={({ item }) => (
                <PromoItem {...item} />
              )}
            />
          </View>
        </BottomSheet>
      </Portal>
    </>
  );
};

const PromoItem = (props) => {
  const { translations } = useContext(LocalizationContext);

  return (
    <View style={styles().promoItem}>
      <Image source={props.imgUrl} />
      <View style={styles().promoItemInfo}>
        <Text style={styles().promoItemTitle}>
          {props.name}
        </Text>
        <View style={styles().promoItemInfoValidDate}>
          <FontAwesome5 name="calendar-check" color="#BFBFBF" />
          <Text style={styles().promoItemSubtitle}>
            {props.valid}
          </Text>
        </View>
        <TouchableOpacity style={styles().promoItemBtn}>
          <Text style={styles().promoItemBtnTitle}>
            {translations['selected-menu'].use}
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const CustomLangModalHandle = ({ style, animatedIndex }) => {
  const { colors, isDark } = useTheme();

  const handleStyles = {
    header: {
      borderTopLeftRadius: normalize(10),
      borderTopRightRadius: normalize(10),
      alignItems: 'center',
      alignContent: 'center',
      justifyContent: 'center',
      backgroundColor: colors.background,
      paddingVertical: normalize(14),
    },
    indicator: {
      borderRadius: normalize(20),
      position: 'absolute',
      width: normalize(40),
      height: normalize(6),
      backgroundColor: isDark ? '#FFFFFF' : '#BFBFBF',
    },
  };

  return (
    <View style={handleStyles.header} renderToHardwareTextureAndroid>
      <View style={handleStyles.indicator} />
    </View>
  );
};

const Divider = () => {
  return (
    <View style={styles().divider} />
  );
};

const MenuList = (props) => {
  const { translations } = useContext(LocalizationContext);

  return (
    <View style={styles().menuItemList}>
      <Text style={styles().menuItemListTitle}>
        {translations['selected-menu'].menus}
      </Text>
      <FlatList
        data={props.menuList}
        keyExtractor={(item, index) => item + index.toString()}
        renderItem={({ item }) => (
          <MenuItem {...item} />
        )}
      />
    </View>
  );
};

const MenuItem = (props) => {
  const { colors } = useTheme();
  const [isFavorite, setFavorite] = useState(false);

  return (
    <View style={styles().menuItem}>
      <ImageBackground source={props.imgUrl} resizeMode="contain" style={styles().menuItemImg}>
        {props.isNew && (
          <View style={styles().menuItemBadgeWrapper}>
            <Text style={styles().menuItemBadgeTitle}>
              New
            </Text>
          </View>
        )}
      </ImageBackground>
      <View style={styles().menuItemInfoWrapper}>
        <Text style={styles().menuItemName}>
          {props.name}
        </Text>
        <CurrencyFormat value={props.price} customStyles={styles().menuItemPrice} />
      </View>
      <View style={styles().menuItemCtaWrapper}>
        <TouchableOpacity onPress={() => setFavorite(!isFavorite)}>
          <FontAwesome name="star" size={normalize(20)} color={isFavorite ? colors.primary : '#F0F0F0'} />
        </TouchableOpacity>
        <TouchableOpacity style={styles().menuItemAddBtn} onPress={() => {}}>
          <AntDesign name="plus" size={normalize(16)} color="#FFFFFF" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SelectedMenuScreen;
