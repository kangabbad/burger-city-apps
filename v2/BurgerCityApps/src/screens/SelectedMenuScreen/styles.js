import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { isDark, colors } = useTheme();

  return StyleSheet.create({
    selectedMenu: {
      backgroundColor: colors.background,
    },
    navbar: {
      backgroundColor: 'transparent',
      height: normalize(75),
      zIndex: 5,
    },
    navbarBackBtn: {
      borderRadius: normalize(35),
      alignItems: 'center',
      justifyContent: 'center',
      height: normalize(35),
      width: normalize(35),
      backgroundColor: 'rgba(255, 255, 255, 0.5)',
      position: 'absolute',
      top: normalize(30),
      right: normalize(20),
      left: normalize(20),
      zIndex: 5,
    },
    bannerImg: {
      width: '100%',
      height: normalize(200),
      marginTop: normalize(-70),
    },
    menuIntro: {
      backgroundColor: colors.background,
      padding: normalize(20),
    },
    menuIntroTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(20),
      color: colors.text,
      includeFontPadding: false,
    },
    menuTagTitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#ECECEC',
      includeFontPadding: false,
      marginTop: normalize(5),
    },
    dividerDash: {
      borderWidth: 1,
      borderRadius: 1,
      borderColor: '#BFBFBF',
      borderStyle: 'dashed',
      marginVertical: normalize(15),
    },
    menuInfoWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      marginVertical: normalize(5),
    },
    menuInfoTitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
    },
    menuInfoSubtitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.primary,
      includeFontPadding: false,
      marginRight: normalize(10),
    },
    menuInfoPromoTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.primary,
      includeFontPadding: false,
      marginRight: normalize(10),
    },
    menuInfoPromoBtn: {
      borderBottomWidth: 1,
      borderBottomColor: colors.primary,
      flexDirection: 'row',
      alignItems: 'center',
    },
    promoPopupWrapper: {
      backgroundColor: colors.background,
      height: '100%',
      padding: normalize(20),
      paddingBottom: 0,
    },
    promoPopupTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
    },
    promoPopupList: {
      marginTop: normalize(20),
    },
    promoItem: {
      flexDirection: 'row',
      alignItems: 'center',
      borderRadius: normalize(5),
      backgroundColor: isDark ? '#191919' : '#FFFFFF',
      padding: normalize(10),
      marginBottom: normalize(10),
    },
    promoItemInfo: {
      flexGrow: 2,
      flexShrink: 1,
      marginLeft: normalize(20),
    },
    promoItemTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
    },
    promoItemInfoValidDate: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: normalize(5),
    },
    promoItemSubtitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#BFBFBF',
      includeFontPadding: false,
      marginLeft: normalize(10),
    },
    promoItemBtn: {
      borderRadius: normalize(5),
      backgroundColor: colors.primary,
      paddingVertical: normalize(5),
      paddingHorizontal: normalize(10),
      marginTop: normalize(10),
      marginRight: normalize(5),
      marginLeft: 'auto',
    },
    promoItemBtnTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    divider: {
      backgroundColor: isDark ? '#191919' : '#F0F0F0',
      height: normalize(15),
    },
    menuItemList: {
      backgroundColor: colors.background,
      padding: normalize(20),
    },
    menuItemListTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
      marginBottom: normalize(15),
    },
    menuItem: {
      flexDirection: 'row',
      alignItems: 'center',
      marginVertical: normalize(10),
    },
    menuItemImg: {
      height: normalize(60),
      width: normalize(60),
      flexDirection: 'row',
      justifyContent: 'flex-end',
    },
    menuItemBadgeWrapper: {
      borderRadius: normalize(25),
      alignItems: 'center',
      justifyContent: 'center',
      height: normalize(25),
      width: normalize(25),
      backgroundColor: colors.error,
      marginTop: normalize(-5),
      marginRight: normalize(-5),
    },
    menuItemBadgeTitle: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(8),
      color: '#FFFFFF',
      includeFontPadding: false,
      textTransform: 'uppercase',
    },
    menuItemInfoWrapper: {
      marginLeft: normalize(20),
    },
    menuItemName: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
    },
    menuItemPrice: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(14),
      color: colors.primary,
      includeFontPadding: false,
      marginTop: normalize(5),
    },
    menuItemCtaWrapper: {
      flexDirection: 'row',
      alignItems: 'center',
      marginLeft: 'auto',
    },
    menuItemAddBtn: {
      borderRadius: normalize(5),
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: colors.primary,
      padding: normalize(5),
      marginLeft: normalize(15),
    },
  });
};
