import React, { useContext } from 'react';

import { LocalizationContext } from '~/utils/i18n';

import SelectedMenuScreen from '../SelectedMenuScreen';

import bannerDesserts from '~/assets/images/banner-desserts.webp';
import sundaeChocolate from '~/assets/images/sundae-chocolate.webp';

const SidesScreen = () => {
  const { translations } = useContext(LocalizationContext);

  const menuList = [
    {
      imgUrl: sundaeChocolate,
      name: 'Sundae Chocolate',
      price: 10000,
      isNew: true,
    },
    {
      imgUrl: sundaeChocolate,
      name: 'Sundae Chocolate',
      price: 10000,
      isNew: true,
    },
    {
      imgUrl: sundaeChocolate,
      name: 'Sundae Chocolate',
      price: 10000,
      isNew: false,
    },
  ];

  return (
    <SelectedMenuScreen
      bannerImg={bannerDesserts}
      menuTitle={translations['desserts-menu'].title}
      menuTags={translations['desserts-menu'].tags}
      menuList={menuList}
    />
  );
};

export default SidesScreen;
