import { StyleSheet, Dimensions } from 'react-native';
import normalize from 'react-native-normalize';

export default () => {
  return StyleSheet.create({
    welcomeWrapper: {
      marginTop: normalize(30),
      marginHorizontal: normalize(40),
    },
    welcomeTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(24),
      color: '#FFFFFF',
      includeFontPadding: false,
      textAlign: 'center',
    },
    welcomeSubtitle: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(16),
      color: '#FFFFFF',
      includeFontPadding: false,
      textAlign: 'center',
      marginTop: normalize(8),
    },
    keyboardScrollView: {
      width: Dimensions.get('window').width,
    },
    formEmail: {
      width: '100%',
      marginTop: normalize(50),
    },
    buttonTextOnly: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: '#FFFFFF',
      includeFontPadding: false,
    },
    submitWrapper: {
      marginTop: normalize(20),
    },
  });
};
