import React, { useContext } from 'react';
import { Text, View } from 'react-native';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { useNavigation } from '@react-navigation/native';

import BaseIntroScreen from '~/components/BaseIntroScreen';
import ButtonBlock from '~/components/ButtonBlock';
import InputboxIcon from '~/components/InputboxIcon';

import { LocalizationContext } from '~/utils/i18n';
import styles from './styles';

const ForgotPasswordScreen = () => {
  const {
    translations,
    initializeAppLanguage,
  } = useContext(LocalizationContext);
  initializeAppLanguage();

  const navigation = useNavigation();

  const onSubmit = () => {
    navigation.push('VerificationScreen');
  };

  return (
    <BaseIntroScreen>
      <KeyboardAwareScrollView
        showsVerticalScrollIndicator={false}
        style={styles().keyboardScrollView}
      >
        <View style={styles().welcomeWrapper}>
          <Text style={styles().welcomeTitle}>
            {translations['forgot-password']['welcome-title']}
          </Text>
          <Text style={styles().welcomeSubtitle}>
            {translations['forgot-password']['welcome-subtitle']}
          </Text>
          <View style={styles().formEmail}>
            <InputboxIcon
              icon={<Fontisto name="email" size={20} color="#92A1B1" />}
              blurOnSubmit
              secureTextEntry
              keyboardType="email-address"
              textContentType="emailAddress"
              placeholder={translations['forgot-password'].email}
              placeholderTextColor="#92A1B1"
            />
          </View>
          <View style={styles().submitWrapper}>
            <ButtonBlock
              label={translations['forgot-password'].proceed}
              onPress={onSubmit}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
    </BaseIntroScreen>
  );
};

export default ForgotPasswordScreen;
