import React, { useContext } from 'react';
import { FlatList, Image, Text, TextInput, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import normalize from 'react-native-normalize';
import { useNavigation } from '@react-navigation/core';

import styles from './styles';
import { useTheme } from '~/utils/ThemeContext';
import { CurrencyFormat } from '~/utils/helpers';
import { LocalizationContext } from '~/utils/i18n';

import beefBurgerImg from '~/assets/images/beef-burger.webp';

const SearchFoodScreen = () => {
  const dummyData = [
    {
      imgUrl: beefBurgerImg,
      name: 'Cheesy Burger',
      rating: 5,
      price: 50000,
    },
    {
      imgUrl: beefBurgerImg,
      name: 'Cheesy Burger',
      rating: 5,
      price: 50000,
    },
    {
      imgUrl: beefBurgerImg,
      name: 'Cheesy Burger',
      rating: 5,
      price: 50000,
    },
  ];

  return (
    <FlatList
      ListHeaderComponent={<Header />}
      data={dummyData}
      keyExtractor={(item, index) => item + index.toString()}
      stickyHeaderIndices={[0]}
      style={styles().searchFood}
      renderItem={({ item }) => (
        <SearchItem {...item} />
      )}
    />
  );
};

const Header = () => {
  const { colors } = useTheme();
  const navigation = useNavigation();
  const { translations } = useContext(LocalizationContext);

  const onGoBack = () => navigation.goBack();

  return (
    <View style={styles().header}>
      <TouchableOpacity style={styles().backBtn} onPress={onGoBack}>
        <AntDesign name="arrowleft" color={colors.text} size={normalize(26)} />
      </TouchableOpacity>
      <TextInput
        placeholder={translations['our-burgers']['search-burger']}
        placeholderTextColor={colors.text}
        style={styles().searchBar}
      />
    </View>
  );
};

const SearchItem = (props) => {
  return (
    <View style={styles().searchItem}>
      <Image
        source={props.imgUrl}
        resizeMode="contain"
        style={styles().searchItemImg}
      />
      <View style={styles().searchItemInfo}>
        <Text style={styles().searchItemTitle}>
          {props.name}
        </Text>
        <View style={styles().searchItemRating}>
          {[...Array(props.rating)].map((_, ratingIndex) => (
            <FontAwesome5
              key={ratingIndex}
              solid
              name="star"
              size={12}
              color="#FF9F1C"
              style={styles().searchItemRatingItem}
            />
          ))}
        </View>
        <CurrencyFormat value={props.price} customStyles={styles().searchItemPrice} />
      </View>
    </View>
  );
};

export default SearchFoodScreen;
