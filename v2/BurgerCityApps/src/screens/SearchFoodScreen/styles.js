import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors } = useTheme();

  return StyleSheet.create({
    searchFood: {
      backgroundColor: colors.background,
    },
    header: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingVertical: normalize(20),
      paddingHorizontal: normalize(10),
    },
    backBtn: {
      borderRadius: normalize(10),
      flexShrink: 1,
      padding: normalize(10),
    },
    searchBar: {
      borderRadius: normalize(10),
      borderWidth: 1,
      borderColor: '#E0E0E0',
      flexGrow: 1,
      flexShrink: 0,
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.text,
      paddingVertical: normalize(5),
      paddingHorizontal: normalize(20),
      marginHorizontal: normalize(10),
    },
    searchItem: {
      flexDirection: 'row',
      alignItems: 'center',
      paddingVertical: normalize(10),
      paddingHorizontal: normalize(20),
    },
    searchItemInfo: {
      marginLeft: normalize(20),
    },
    searchItemImg: {
      height: normalize(75),
      width: normalize(75),
    },
    searchItemTitle: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
    },
    searchItemRating: {
      flexDirection: 'row',
      alignItems: 'center',
      marginTop: normalize(5),
    },
    searchItemRatingItem: {
      marginRight: normalize(5),
    },
    searchItemPrice: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
      marginTop: normalize(15),
    },
  });
};
