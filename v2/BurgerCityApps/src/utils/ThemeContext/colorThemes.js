// Light theme colors
export const baseColors = {
  primary: '#FF9F1C',
  secondary: '',
  accent: '',
  error: '',
  info: '',
  success: '',
  warning: '',
};

export const lightColors = {
  background: '#FFFFFF',
  primary: baseColors.primary,
  text: '#121212',
  error: '#D32F2F',
};

// Dark theme colors
export const darkColors = {
  background: '#121212',
  primary: baseColors.primary,
  text: '#FFFFFF',
  error: '#D32F2F',
};
