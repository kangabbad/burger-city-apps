import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import NumberFormat from 'react-number-format';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { useTheme } from '../ThemeContext';

const currencyStyles = StyleSheet.create(() => {
  const { colors } = useTheme();

  return {
    currency: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(14),
      color: colors.text,
      includeFontPadding: false,
    },
  };
});

export const CurrencyFormat = (props) => {
  return (
    <NumberFormat
      value={props.value}
      displayType="text"
      thousandSeparator="."
      decimalSeparator=","
      prefix="Rp "
      renderText={(value) => (
        <Text style={{ ...currencyStyles.currency, ...props.customStyles }}>
          {value}
        </Text>
      )}
    />
  );
};

CurrencyFormat.propTypes = {
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  customStyles: Text.propTypes.style,
};

export const storeData = async (key, value) => {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value));
  } catch (e) {
    // saving error
  }
};

export const getData = async (key) => {
  try {
    const value = await AsyncStorage.getItem(key);
    if (value !== null) {
      return value;
    }
  } catch (e) {
    // error reading value
  }
};

export const hexToRgba = (color, opacity = 1) => {
  color = color.replace('#', '');
  const r = parseInt(color.substring(0, 2), 16);
  const g = parseInt(color.substring(2, 4), 16);
  const b = parseInt(color.substring(4, 6), 16);
  return `rgba(${r}, ${g}, ${b}, ${opacity})`;
};
