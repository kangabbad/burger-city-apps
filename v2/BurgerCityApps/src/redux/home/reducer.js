import { HOME_ACTION } from './types';

const INITIAL_STATE = {
  title: 'THIS IS HOME',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case HOME_ACTION:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
