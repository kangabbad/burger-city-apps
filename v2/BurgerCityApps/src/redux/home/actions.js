import { HOME_ACTION } from './types';

export const homeAction = () => {
  return {
    type: HOME_ACTION,
    payload: {
      title: 'THIS IS HOME (UPDATED)',
    },
  };
};
