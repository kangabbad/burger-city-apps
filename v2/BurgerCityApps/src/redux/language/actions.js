import { CHANGE_LANGUAGE } from './types';
import { storeData } from '~/utils/helpers';

export const onChangeLanguage = (languageActive) => {
  storeData('languageActive', languageActive);
  return {
    type: CHANGE_LANGUAGE,
    payload: {
      languageActive,
    },
  };
};
