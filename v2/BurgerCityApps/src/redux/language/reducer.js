import { CHANGE_LANGUAGE } from './types';

const INITIAL_STATE = {
  languageActive: 'en',
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CHANGE_LANGUAGE:
      return {
        ...state,
        ...action.payload,
      };
    default:
      return state;
  }
};
