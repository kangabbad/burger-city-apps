import { combineReducers } from 'redux';

import homeReducer from './home/reducer';
import languageReducer from './language/reducer';

const rootReducer = combineReducers({
  homeReducer,
  languageReducer,
});

export default rootReducer;
