import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';

export default () => {
  return StyleSheet.create({
    inputWrapper: {
      borderRadius: normalize(10),
      backgroundColor: '#FFFFFF',
      overflow: 'hidden',
      flexDirection: 'row',
      alignItems: 'center',
      width: '100%',
      // paddingVertical: normalize(8),
      paddingHorizontal: normalize(20),
    },
    inputBox: {
      flex: 1,
      fontFamily: 'Nunito-Regular',
      color: '#242424',
      height: normalize(50),
      marginLeft: normalize(15),
    },
  });
};
