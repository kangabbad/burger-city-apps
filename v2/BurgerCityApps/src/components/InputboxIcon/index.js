import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import { TextInput, View } from 'react-native';
import { useTheme } from '~/utils/ThemeContext';
import styles from './styles';

const InputboxIcon = (props) => {
  const { colors } = useTheme();
  const {
    icon,
    blurOnSubmit,
    placeholder,
    placeholderTextColor,
    keyboardType,
    textContentType,
    secureTextEntry,
  } = props;
  const inputboxRef = useRef(null);
  useEffect(() => {
    if (inputboxRef) {
      inputboxRef.current.setNativeProps({
        style: { fontFamily: 'Nunito-Regular' },
      });
    }
  }, []);
  return (
    <View style={styles().inputWrapper}>
      {icon}
      <TextInput
        ref={inputboxRef}
        blurOnSubmit={blurOnSubmit}
        keyboardType={keyboardType}
        textContentType={textContentType}
        selectionColor={colors.primary}
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        style={styles().inputBox}
      />
    </View>
  );
};

InputboxIcon.propTypes = {
  icon: PropTypes.object,
  blurOnSubmit: PropTypes.bool,
  placeholder: PropTypes.string,
  placeholderTextColor: PropTypes.string,
  keyboardType: PropTypes.string,
  textContentType: PropTypes.string,
  secureTextEntry: PropTypes.bool,
};

export default InputboxIcon;
