import React from 'react';
import PropTypes from 'prop-types';
import { Text, TouchableOpacity, ViewPropTypes } from 'react-native';

import styles from './styles';

const ButtonBlock = (props) => {
  const { onPress, label, style } = props;
  return (
    <TouchableOpacity
      style={[styles().contentBtn, style]}
      onPress={onPress}
    >
      <Text style={styles().contentBtnText}>
        {label}
      </Text>
    </TouchableOpacity>
  );
};

ButtonBlock.propTypes = {
  onPress: PropTypes.func,
  label: PropTypes.string,
  style: ViewPropTypes.style,
};

export default ButtonBlock;
