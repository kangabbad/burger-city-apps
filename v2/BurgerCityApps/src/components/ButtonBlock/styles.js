import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors } = useTheme();

  return StyleSheet.create({
    contentBtn: {
      borderRadius: normalize(10),
      backgroundColor: colors.primary,
      width: '100%',
      padding: normalize(15),
    },
    contentBtnText: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(18),
      color: '#FFFFFF',
      includeFontPadding: false,
      textAlign: 'center',
    },
  });
};
