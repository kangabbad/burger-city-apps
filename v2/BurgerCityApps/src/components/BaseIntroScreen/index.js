import React from 'react';
import { Image, ImageBackground, SafeAreaView, StatusBar, View } from 'react-native';

import bootSplashBg from '~/assets/images/bg-splash.webp';
import burgerCityLogo from '~/assets/icons/burger-city-logo.webp';

import styles from './styles';

const BaseIntroScreen = (props) => {
  return (
    <>
      <StatusBar
        barStyle="light-content"
        backgroundColor="transparent"
        translucent
      />
      <ImageBackground source={bootSplashBg} style={styles().container}>
        <RenderOverlay />
        <Content {...props} />
      </ImageBackground>
    </>
  );
};

const RenderOverlay = () => {
  return (
    <View style={styles().bgOverlay} />
  );
};

const Content = ({ children }) => {
  return (
    <SafeAreaView style={styles().containerIOS}>
      <View style={styles().contentContainer}>
        <Image source={burgerCityLogo} resizeMode="contain" style={styles().contentLogo} />
        {children}
      </View>
    </SafeAreaView>
  );
};

export default BaseIntroScreen;
