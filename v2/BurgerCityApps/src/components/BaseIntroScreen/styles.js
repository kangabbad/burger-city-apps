import { Platform, StatusBar, StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';

export default () => {
  const marginTopLogo = Platform.OS === 'android'
    ? StatusBar.currentHeight + normalize(35)
    : normalize(50);

  return StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#242424',
    },
    containerIOS: {
      flex: 1,
    },
    bgOverlay: {
      ...StyleSheet.absoluteFill,
      backgroundColor: '#242424',
      opacity: 0.35,
    },
    contentContainer: {
      flex: 1,
      alignItems: 'center',
      width: '100%',
    },
    contentLogo: {
      height: normalize(85),
      width: normalize(75),
      marginTop: marginTopLogo,
    },
  });
};
