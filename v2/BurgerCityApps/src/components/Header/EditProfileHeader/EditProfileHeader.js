import React, { useContext, useState } from 'react';
import { Modal, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import { useNavigation } from '@react-navigation/native';
import normalize from 'react-native-normalize';

import { useTheme } from '~/utils/ThemeContext';
import { LocalizationContext } from '~/utils/i18n';
import styles from './styles';

const EditProfileHeader = () => {
  const { isDark, colors } = useTheme();
  const barStyle = isDark ? 'light-content' : 'dark-content';

  return (
    <>
      <StatusBar
        animated
        translucent={false}
        barStyle={barStyle}
        backgroundColor={colors.background}
      />
      <View style={styles().container}>
        <BackBtn />
        <SaveBtn />
      </View>
    </>
  );
};

const BackBtn = () => {
  const { colors } = useTheme();
  const navigation = useNavigation();
  const onPress = () => {
    navigation.goBack();
  };

  return (
    <TouchableOpacity style={styles().backBtn} onPress={onPress}>
      <AntDesign name="arrowleft" color={colors.text} size={normalize(26)} />
    </TouchableOpacity>
  );
};

const SaveBtn = () => {
  const { isDark, colors } = useTheme();
  const { translations } = useContext(LocalizationContext);
  const [isModalVisible, setModalVisible] = useState(false);

  const onOpen = () => {
    setModalVisible(!isModalVisible);
    StatusBar.setBarStyle('light-content', true);
  };
  const onHide = () => {
    setModalVisible(!isModalVisible);
    if (!isDark) {
      StatusBar.setBarStyle('dark-content', true);
    } else {
      StatusBar.setBarStyle('light-content', true);
    }
  };

  return (
    <>
      <TouchableOpacity style={styles().saveBtn} onPress={onOpen}>
        <SimpleLineIcons name="check" size={normalize(24)} color={colors.primary} />
      </TouchableOpacity>
      <Modal
        transparent
        hardwareAccelerated
        statusBarTranslucent
        animationType="fade"
        visible={isModalVisible}
        onRequestClose={onHide}
      >
        <View style={styles().logoutModalContainer}>
          <View style={styles().logoutModalContent}>
            <View style={styles().logoutModalIconBg}>
              <View style={styles().logoutModalIconWrapper}>
                <AntDesign name="question" color="#FFFFFF" size={normalize(50)} />
              </View>
            </View>
            <Text style={styles().logoutModalTitle}>
              {translations.profile['save-profile-title']}
            </Text>
            <Text style={styles().logoutModalDesc}>
              {translations.profile['save-profile-desc']}
            </Text>
            <View style={styles().logoutModalActionWrapper}>
              <TouchableOpacity
                style={[styles().logoutModalActionBtn, styles().logoutModalActionReject]}
                activeOpacity={0.8}
                onPress={onHide}
              >
                <Text style={[styles().logoutModalActionText, styles().logoutModalRejectText]}>
                  {translations.profile['save-profile-cta-reject']}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles().logoutModalActionBtn, styles().logoutModalActionAccept]}
                activeOpacity={0.5}
                onPress={onHide}
              >
                <Text style={[styles().logoutModalActionText, styles().logoutModalAcceptText]}>
                  {translations.profile['save-profile-cta-accept']}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

export default EditProfileHeader;
