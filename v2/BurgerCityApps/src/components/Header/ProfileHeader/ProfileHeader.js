import React, { useContext, useState } from 'react';
import { Image, Modal, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import Feather from 'react-native-vector-icons/Feather';
import AntDesign from 'react-native-vector-icons/AntDesign';
import { useNavigation } from '@react-navigation/native';
import normalize from 'react-native-normalize';

import { useTheme } from '~/utils/ThemeContext';
import { LocalizationContext } from '~/utils/i18n';
import styles from './styles';

import menuLogoBlack from '~/assets/icons/menu-logo-black.webp';
import menuLogoWhite from '~/assets/icons/menu-logo-white.webp';

const ProfileHeader = () => {
  const { isDark, colors } = useTheme();
  const barStyle = isDark ? 'light-content' : 'dark-content';

  return (
    <>
      <StatusBar
        animated
        translucent={false}
        barStyle={barStyle}
        backgroundColor={colors.background}
      />
      <View style={styles().container}>
        <LogoutBtn />
        <Logo />
        <EditBtn />
      </View>
    </>
  );
};

const LogoutBtn = () => {
  const { isDark, colors } = useTheme();
  const { translations } = useContext(LocalizationContext);
  const [isModalVisible, setModalVisible] = useState(false);

  const onOpen = () => {
    setModalVisible(!isModalVisible);
    StatusBar.setBarStyle('light-content', true);
  };
  const onHide = () => {
    setModalVisible(!isModalVisible);
    if (!isDark) {
      StatusBar.setBarStyle('dark-content', true);
    } else {
      StatusBar.setBarStyle('light-content', true);
    }
  };

  return (
    <>
      <TouchableOpacity style={styles().logoutBtn} onPress={onOpen}>
        <Feather
          name="log-out"
          color={colors.error}
          size={normalize(20)}
          style={{ transform: [{ rotateY: '180deg' }] }}
        />
      </TouchableOpacity>
      <Modal
        transparent
        hardwareAccelerated
        statusBarTranslucent
        animationType="fade"
        visible={isModalVisible}
        onRequestClose={onHide}
      >
        <View style={styles().logoutModalContainer}>
          <View style={styles().logoutModalContent}>
            <View style={styles().logoutModalIconBg}>
              <View style={styles().logoutModalIconWrapper}>
                <AntDesign name="question" color="#FFFFFF" size={normalize(50)} />
              </View>
            </View>
            <Text style={styles().logoutModalTitle}>
              {translations.profile['logout-title']}
            </Text>
            <Text style={styles().logoutModalDesc}>
              {translations.profile['logout-desc']}
            </Text>
            <View style={styles().logoutModalActionWrapper}>
              <TouchableOpacity
                style={[styles().logoutModalActionBtn, styles().logoutModalActionReject]}
                activeOpacity={0.5}
                onPress={onHide}
              >
                <Text style={[styles().logoutModalActionText, styles().logoutModalRejectText]}>
                  {translations.profile['logout-cta-reject']}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[styles().logoutModalActionBtn, styles().logoutModalActionAccept]}
                activeOpacity={0.8}
                onPress={onHide}
              >
                <Text style={[styles().logoutModalActionText, styles().logoutModalAcceptText]}>
                  {translations.profile['logout-cta-accept']}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </>
  );
};

const Logo = () => {
  const { isDark } = useTheme();
  const source = isDark ? menuLogoWhite : menuLogoBlack;

  return (
    <View style={styles().logoContainer}>
      <Image
        source={source}
        resizeMode="contain"
        style={styles().logoImg}
      />
    </View>
  );
};

const EditBtn = () => {
  const { colors } = useTheme();
  const navigation = useNavigation();
  const onPress = () => {
    navigation.navigate('InAppStack', { screen: 'EditProfileScreen' });
  };
  return (
    <TouchableOpacity style={styles().editBtn} onPress={onPress}>
      <AntDesign name="edit" size={20} color={colors.primary} />
    </TouchableOpacity>
  );
};

export default ProfileHeader;
