import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { isDark, colors } = useTheme();

  return StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: normalize(44),
      backgroundColor: colors.background,
    },
    logoutBtn: {
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: normalize(15),
    },
    logoutBtnText: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.primary,
      includeFontPadding: false,
      marginRight: normalize(5),
    },
    logoutModalContainer: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'rgba(0, 0, 0, 0.5)',
    },
    logoutModalContent: {
      borderRadius: normalize(10),
      backgroundColor: isDark ? '#292929' : '#FFFFFF',
      alignItems: 'center',
      width: '70%',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 4,
      elevation: 5,
    },
    logoutModalIconBg: {
      borderRadius: normalize(100),
      backgroundColor: isDark ? '#292929' : '#FFFFFF',
      padding: normalize(10),
      marginTop: normalize(-40),
    },
    logoutModalIconWrapper: {
      borderRadius: normalize(100),
      backgroundColor: colors.error,
      padding: normalize(10),
    },
    logoutModalTitle: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
      textAlign: 'center',
      marginTop: normalize(10),
    },
    logoutModalDesc: {
      fontFamily: 'Nunito-Regular',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
      textAlign: 'center',
      paddingHorizontal: normalize(20),
      marginTop: normalize(7),
    },
    logoutModalActionWrapper: {
      borderBottomLeftRadius: normalize(10),
      borderBottomRightRadius: normalize(10),
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'center',
      height: 'auto',
      width: '100%',
      overflow: 'hidden',
      marginTop: normalize(20),
    },
    logoutModalActionBtn: {
      flex: 1,
      paddingVertical: normalize(15),
      alignItems: 'center',
    },
    logoutModalActionReject: {
      backgroundColor: isDark ? '#292929' : '#F0F0F0',
    },
    logoutModalActionAccept: {
      backgroundColor: colors.error,
    },
    logoutModalActionText: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(16),
      includeFontPadding: false,
    },
    logoutModalRejectText: {
      color: colors.text,
    },
    logoutModalAcceptText: {
      color: '#FFFFFF',
    },
    logoContainer: {
      ...StyleSheet.absoluteFill,
      alignItems: 'center',
      justifyContent: 'center',
    },
    logoImg: {
      height: normalize(20),
    },
    editBtn: {
      paddingHorizontal: normalize(16),
    },
  });
};
