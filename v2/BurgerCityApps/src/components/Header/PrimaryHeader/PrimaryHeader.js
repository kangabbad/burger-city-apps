import React, { useRef, useMemo, useCallback, useContext } from 'react';
import { Image, Pressable, StatusBar, Text, TouchableOpacity, View } from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather';
import { useNavigation } from '@react-navigation/native';
import BottomSheet, { BottomSheetBackdrop } from '@gorhom/bottom-sheet';
import Animated from 'react-native-reanimated';
import { Portal } from 'react-native-portalize';
import normalize from 'react-native-normalize';

import { useTheme } from '~/utils/ThemeContext';
import { LocalizationContext } from '~/utils/i18n';
import styles from './styles';

import menuLogoBlack from '~/assets/icons/menu-logo-black.webp';
import menuLogoWhite from '~/assets/icons/menu-logo-white.webp';

const PrimaryHeader = () => {
  const { isDark, colors } = useTheme();
  const barStyle = isDark ? 'light-content' : 'dark-content';

  return (
    <>
      <StatusBar
        animated
        translucent={false}
        barStyle={barStyle}
        backgroundColor={colors.background}
      />
      <View style={styles().container}>
        <LangPicker />
        <Logo />
        <NotificationBtn />
      </View>
    </>
  );
};

const LangPicker = () => {
  const { colors, isDark } = useTheme();
  const bottomSheetRef = useRef(null);
  const snapPoints = useMemo(() => [0, '35%'], []);

  const {
    translations,
    appLanguage,
    setAppLanguage,
    initializeAppLanguage,
  } = useContext(LocalizationContext);
  initializeAppLanguage();

  const onChangeLanguage = (lang) => {
    setAppLanguage(lang);
    bottomSheetRef.current?.collapse();
  };

  const langIdBtnStyles = appLanguage === 'id' ? [styles().langSelectBtn, styles().langSelectBtnActive] : styles().langSelectBtn;
  const langIdBtnTextStyles = appLanguage === 'id' ? [styles().langSelectBtnText, styles().langSelectBtnTextActive] : styles().langSelectBtnText;
  const langIdBtnCheck = appLanguage === 'id' && <FontAwesome5 name="check" color="#FFFFFF" size={20} />;

  const langEnBtnStyles = appLanguage === 'en' ? [styles().langSelectBtn, styles().langSelectBtnActive] : styles().langSelectBtn;
  const langEnBtnTextStyles = appLanguage === 'en' ? [styles().langSelectBtnText, styles().langSelectBtnTextActive] : styles().langSelectBtnText;
  const langEnBtnCheck = appLanguage === 'en' && <FontAwesome5 name="check" color="#FFFFFF" size={20} />;

  const onAnimateChanges = useCallback((_fromIndex, toIndex) => {
    if (toIndex < 1) {
      if (isDark) {
        StatusBar.setBarStyle('light-content', true);
      } else {
        StatusBar.setBarStyle('dark-content', true);
      }
      StatusBar.setBackgroundColor(colors.background, true);
    }
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  const onOpen = () => {
    bottomSheetRef.current?.expand();
    StatusBar.setBarStyle('light-content', true);
    if (isDark) {
      StatusBar.setBackgroundColor('rgba(0, 0, 0, 0.97)', true);
    } else {
      StatusBar.setBackgroundColor('rgba(0, 0, 0, 0.5)', true);
    }
  };

  return (
    <>
      <Pressable style={styles().langModalBtn} onPress={onOpen}>
        <Text style={styles().langModalBtnText}>
          {appLanguage}
        </Text>
        <Feather name="chevron-down" color={colors.primary} size={22} />
      </Pressable>
      <Portal>
        <BottomSheet
          ref={bottomSheetRef}
          index={-1}
          animateOnMount
          snapPoints={snapPoints}
          backdropComponent={BottomSheetBackdrop}
          handleComponent={CustomLangModalHandle}
          onAnimate={onAnimateChanges}
        >
          <View style={styles().langModalContent}>
            <Text style={styles().langModalContentHeading}>
              {translations.home['language-setting']}
            </Text>
            <TouchableOpacity
              style={langIdBtnStyles}
              onPress={() => onChangeLanguage('id')}
            >
              <View style={styles().langSelectBtnContent}>
                <Text style={langIdBtnTextStyles}>
                  {translations.indonesia}
                </Text>
                {langIdBtnCheck}
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              style={langEnBtnStyles}
              onPress={() => onChangeLanguage('en')}
            >
              <View style={styles().langSelectBtnContent}>
                <Text style={langEnBtnTextStyles}>
                  {translations.english}
                </Text>
                {langEnBtnCheck}
              </View>
            </TouchableOpacity>
          </View>
        </BottomSheet>
      </Portal>
    </>
  );
};

const CustomLangModalHandle = ({ style, animatedIndex }) => {
  const { colors, isDark } = useTheme();

  const handleStyles = {
    header: {
      borderTopLeftRadius: normalize(10),
      borderTopRightRadius: normalize(10),
      alignItems: 'center',
      alignContent: 'center',
      justifyContent: 'center',
      backgroundColor: colors.background,
      paddingVertical: normalize(14),
    },
    indicator: {
      borderRadius: normalize(20),
      position: 'absolute',
      width: normalize(40),
      height: normalize(6),
      backgroundColor: isDark ? '#FFFFFF' : '#BFBFBF',
    },
  };

  return (
    <Animated.View style={handleStyles.header} renderToHardwareTextureAndroid>
      <Animated.View style={handleStyles.indicator} />
    </Animated.View>
  );
};

const Logo = () => {
  const { isDark } = useTheme();
  const source = isDark ? menuLogoWhite : menuLogoBlack;

  return (
    <View style={styles().logoContainer}>
      <Image
        source={source}
        resizeMode="contain"
        style={styles().logoImg}
      />
    </View>
  );
};

const NotificationBtn = () => {
  const { isDark } = useTheme();
  const navigation = useNavigation();

  const color = isDark ? '#FFFFFF' : '#CCCCCC';

  const onPress = () => {
    navigation.navigate('InAppStack', { screen: 'NotificationScreen' });
  };

  return (
    <TouchableOpacity style={styles().notificationBtn} onPress={onPress}>
      <MaterialCommunityIcons
        name="bell"
        size={normalize(20)}
        color={color}
      />
    </TouchableOpacity>
  );
};

export default PrimaryHeader;
