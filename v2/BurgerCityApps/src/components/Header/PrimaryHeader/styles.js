import { StyleSheet } from 'react-native';
import normalize from 'react-native-normalize';
import { useTheme } from '~/utils/ThemeContext';

export default () => {
  const { colors, isDark } = useTheme();

  return StyleSheet.create({
    container: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
      height: normalize(44),
      backgroundColor: colors.background,
    },
    langModalBtn: {
      flexDirection: 'row',
      alignItems: 'center',
      marginHorizontal: normalize(15),
    },
    langModalBtnText: {
      fontFamily: 'Nunito-Bold',
      fontSize: normalize(14),
      color: colors.primary,
      includeFontPadding: false,
      textTransform: 'uppercase',
      marginRight: normalize(5),
    },
    langModalContainer: {
      backgroundColor: colors.background,
    },
    langModalContent: {
      backgroundColor: colors.background,
      height: '100%',
      padding: normalize(20),
      paddingTop: normalize(15),
    },
    langModalContentHeading: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(22),
      fontWeight: '500',
      color: colors.text,
      includeFontPadding: false,
      marginBottom: normalize(10),
    },
    langSelectBtn: {
      borderRadius: normalize(10),
      borderWidth: 1,
      borderColor: isDark ? '#292929' : '#E0E0E0',
      backgroundColor: isDark ? '#292929' : '#FFFFFF',
      paddingVertical: normalize(16),
      paddingHorizontal: normalize(20),
      marginTop: normalize(15),
    },
    langSelectBtnActive: {
      borderWidth: 0,
      backgroundColor: colors.primary,
    },
    langSelectBtnContent: {
      flexDirection: 'row',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    langSelectBtnText: {
      fontFamily: 'Nunito-SemiBold',
      fontSize: normalize(16),
      color: colors.text,
      includeFontPadding: false,
    },
    langSelectBtnTextActive: {
      fontFamily: 'Nunito-Bold',
      color: '#FFFFFF',
    },
    logoContainer: {
      ...StyleSheet.absoluteFill,
      alignItems: 'center',
      justifyContent: 'center',
    },
    logoImg: {
      height: normalize(20),
    },
    notificationBtn: {
      paddingHorizontal: normalize(16),
    },
  });
};
