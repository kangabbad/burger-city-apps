import { StyleSheet } from 'react-native';
import { useTheme } from '~/utils/ThemeContext';

export default function () {
  const { colors } = useTheme();

  return StyleSheet.create({
    containerIOS: {
      flex: 1,
    },
    container: {
      flex: 1,
      backgroundColor: colors.background,
    },
  });
}

