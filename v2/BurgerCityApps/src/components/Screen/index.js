import React from 'react';
import { SafeAreaView, StatusBar, StyleSheet, View } from 'react-native';

import { useTheme } from '~/utils/ThemeContext';
import styles from './styles';

const Content = (props) => {
  const {
    children,
    statusBarTranslucent,
    statusBarBgColor,
    containerStyle,
  } = props;

  const { isDark } = useTheme();

  const barStyle = isDark ? 'light-content' : 'dark-content';
  const baseContainerStyles = StyleSheet.flatten({
    ...styles().container,
    ...containerStyle,
  });

  return (
    <>
      <StatusBar
        animated
        barStyle={barStyle}
        backgroundColor={statusBarBgColor}
        translucent={statusBarTranslucent}
      />
      <View style={baseContainerStyles}>
        {children}
      </View>
    </>
  );
};

export const Screen = (props) => {
  const { safeArea = false } = props;

  if (safeArea) {
    return (
      <SafeAreaView style={styles().containerIOS}>
        <Content {...props} />
      </SafeAreaView>
    );
  } else {
    return <Content {...props} />;
  }
};
