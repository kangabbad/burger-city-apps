module.exports = {
  root: true,
  parser: 'babel-eslint',
  extends: '@react-native-community/eslint-config',
  rules: {
    'prettier/prettier': 0,
    'no-var': 'error',
    'semi': 'error',
    'no-multi-spaces': 'error',
    'space-in-parens': 'error',
    'no-multiple-empty-lines': 'error',
    'prefer-const': 'error',
  },
  env: {
    es6: true,
    node: true,
  },
};