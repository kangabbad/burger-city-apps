/**
 * @format
 */

import React from 'react';
import { AppRegistry } from 'react-native';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import App from './App';

import store from '~/redux/store';
import { name as appName } from './app.json';

const rootApp = () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};

AppRegistry.registerComponent(appName, () => rootApp);
