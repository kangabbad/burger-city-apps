import { useState } from 'react'

export const useForm = initialValue => {
  const [values, setValues] = useState(initialValue)
  return [
    values,
    (formType, formValue) => {
      if (formType === 'reset') {
        return setValues(initialValue)
      }
      return setValues(prevState => {
        console.log('useForm')
        console.log({ ...prevState, [formType]: formValue })
        return { ...prevState, [formType]: formValue }
      })
    }
  ]
}
