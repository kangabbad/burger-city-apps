import firebase from 'firebase'

firebase.initializeApp({
  apiKey: 'AIzaSyB3rR9ur5P3Kqer5tZ2gzSjkrreCwcYwDk',
  authDomain: 'burger-city-ef05f.firebaseapp.com',
  databaseURL: 'https://burger-city-ef05f.firebaseio.com',
  projectId: 'burger-city-ef05f',
  storageBucket: 'burger-city-ef05f.appspot.com',
  messagingSenderId: '636646997376',
  appId: '1:636646997376:web:cb3deea9f5cd6580f25565'
})

const Fire = firebase

export default Fire
