import React from 'react'
import PropTypes from 'prop-types'
import { Image, Text, FlatList } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { storeData } from '../../util/LocalStorage'

import imgMenu from '../assets/images/ourBurgerMenu.webp'

const MenuCategory = (props) => {
  const menuCategory = [
    {
      name: '',
      thumbnail: imgMenu
    },
    {
      name: '',
      thumbnail: imgMenu
    },
    {
      name: '',
      thumbnail: imgMenu
    },
    {
      name: '',
      thumbnail: imgMenu
    }
  ]
  props.offers.map((item, index) => {
    if (index === 0) menuCategory.splice(0, 4)
    menuCategory.push({
      menu: item.menu,
      name: item.name,
      thumbnail: { uri: item.thumbnail }
    })
  })
  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={menuCategory}
      renderItem={({ item, index }) => (
        <TouchableOpacity
          style={{ ...styles.imageContainer, overflow: 'hidden' }}
          onPress={() => {
            storeData('isLoadApi', true)
            props.navigation.navigate('MenuPacket', {
              menu: item.menu,
              name: item.name
            })
          }}
        >
          <Image
            source={item.thumbnail}
            style={item.name !== '' ? styles.image : styles.imagePlaceHolder}
          />
          <Text
            style={styles.text}
          >
            {item.name}
          </Text>
        </TouchableOpacity>
      )}
      numColumns={2}
      keyExtractor={(item, index) => item.name + index.toString()}
    />
  )
}

export default MenuCategory
MenuCategory.propTypes = {
  navigation: PropTypes.object,
  offers: PropTypes.array
}
const styles = EStyleSheet.create({
  imageContainer: {
    alignItems: 'center',
    marginTop: '5rem',
    marginVertical: '10rem',
    padding: '10rem',
    backgroundColor: 'white',
    marginHorizontal: '10rem',
    width: '130rem',
    height: '115rem',
    borderRadius: '8rem',
    position: 'relative'
  },
  imagePlaceHolder: {
    flex: 1,
    width: '140rem',
    height: '115rem',
    position: 'absolute',
    borderRadius: '8rem'
  },
  image: {
    width: '60rem',
    height: '60rem',
    resizeMode: 'contain'
  },
  text: {
    fontFamily: 'Nunito-Bold',
    fontSize: '12rem'
  }
})
