import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, Image } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import ActionButton from './ActionButton'

const IncludesMenu = (props) => {
  const { thumbnail, title, options, onPressChange } = props
  const ChangeButton = () => {
    if (options.length > 1) {
      return (
        <ActionButton
          styleButton={styles.styleButton}
          titleButton='Change'
          titleButtonStyle={styles.titleButtonStyle}
          onHandlePress={onPressChange}
        />
      )
    }
    return null
  }
  return (
    <View style={styles.container}>
      <Image source={{ uri: thumbnail }} style={styles.image} />
      <Text style={styles.text}>
        {title}
      </Text>
      <View style={styles.buttonPosition}>
        <ChangeButton />
      </View>
    </View>
  )
}

export default IncludesMenu

IncludesMenu.propTypes = {
  thumbnail: PropTypes.string,
  title: PropTypes.string,
  options: PropTypes.array,
  onPressChange: PropTypes.func
}

const styles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    marginBottom: '8rem',
    paddingVertical: '10rem',
    paddingHorizontal: '5rem',
    borderRadius: 8
  },
  image: {
    height: '60rem',
    width: '60rem',
    resizeMode: 'contain',
    marginRight: '5rem'
  },
  text: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem'
  },
  buttonPosition: {
    position: 'absolute',
    right: 0,
    paddingRight: '15rem'
  },
  styleButton: {
    marginRight: '3rem',
    paddingHorizontal: '14rem',
    paddingVertical: '6rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '4rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '10rem',
    color: 'white'
  }
})
