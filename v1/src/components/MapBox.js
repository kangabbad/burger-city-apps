import React from 'react'
import { View, Text, TextInput } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import { Appbar } from 'react-native-paper'
import PropTypes from 'prop-types'

const MapBox = (props) => {
  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE}
        style={styles.map}
        scrollEnabled={false}
        region={{
          latitude: -7.9311342,
          longitude: 110.4314679,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121
        }}
      >
        <Marker
          coordinate={{
            latitude: -7.9311342,
            longitude: 110.4314679
          }}
        />
      </MapView>

      <View style={styles.noteWrapper}>
        <View style={styles.addressWrapper}>
          <Text
            style={styles.textAddress}
            numberOfLines={3}
          >
            Jalan Mangunan, Sukorame, Mangunan
          </Text>
          <Appbar.Action
            icon='pencil'
            size={EStyleSheet.value('16rem')}
            color='#FF9F1C'
            style={{
              marginTop: EStyleSheet.value('-3rem'),
              marginRight: EStyleSheet.value('-3rem')
            }}
            onPress={() => props.navigation.navigate('ChangeAddress')}
          />
        </View>

        <TextInput
          placeholder='Add a note to driver'
          style={styles.titleTextInput}
        />
      </View>
    </View>
  )
}

export default MapBox

MapBox.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    paddingRight: '30rem',
    paddingBottom: '3rem',
    paddingLeft: '25rem',
    marginTop: '16rem'
  },
  map: {
    height: '75rem',
    width: '75rem',
    marginTop: '2rem'
  },
  noteWrapper: {
    flex: 1,
    marginLeft: '20rem'
  },
  addressWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: '16rem'
  },
  textAddress: {
    flex: 1,
    fontSize: '12rem',
    fontFamily: 'Nunito-Regular',
    color: '#121212',
    paddingRight: '20rem'
  },
  titleTextInput: {
    fontSize: '12rem',
    color: '#121212',
    fontFamily: 'Nunito-Regular',
    marginTop: '5rem',
    paddingLeft: 0
  }
})
