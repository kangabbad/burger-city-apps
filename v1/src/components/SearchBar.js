import React from 'react'
import { View } from 'react-native'
import { Searchbar } from 'react-native-paper'
import EStyleSheet from 'react-native-extended-stylesheet'

const SearchBar = () => {
  const [searchQuery, setSearchQuery] = React.useState('')

  const onChangeSearch = query => setSearchQuery(query)
  return (
    <View style={styles.container}>
      <Searchbar
        placeholder='Search for a food'
        onChangeText={onChangeSearch}
        value={searchQuery}
        inputStyle={styles.textInput}
        style={{ elevation: 0 }}
      />
    </View>
  )
}

export default SearchBar
const styles = EStyleSheet.create({
  container: {
    marginTop: '25rem',
    paddingHorizontal: '20rem'
  },
  search: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: '10rem',
    borderRadius: 8
  },
  textInput: {
    fontSize: '12rem',
    fontFamily: 'Nunito-Regular',
    color: '#727C8E',
    elevation: 0
  }
})
