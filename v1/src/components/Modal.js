import React from 'react'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Modal from 'react-native-modal'

const Modals = () => {
  return (
    <Modal
      isVisible={false}
      useNativeDriver
      onBackdropPress={() => {}}
    >
      <View style={styles.bonus__modal}>
        <View style={styles.bonus__image__wrapper}>
          <Image
            source={null}
            style={styles.bonus__image}
          />
        </View>
        <Text
          style={{ fontFamily: 'Nunito-Bold' }}
        >
          Congratulations!
        </Text>
        <Text
          style={{
            paddingHorizontal: 20,
            marginTop: 15
          }}
        >
          Thanks for your payment! You have won a FREE Coca-cola
        </Text>
        <TouchableOpacity style={{ marginTop: 20, width: 150, alignSelf: 'center' }}>
          <Text>Ok</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  )
}

export default Modals

const styles = EStyleSheet.create({
  container: {
    flex: 1
  },
  'detail--confirmation': {
    backgroundColor: '#1D2126',
    paddingTop: '16rem',
    paddingBottom: '10rem',
    paddingHorizontal: '16rem'
  },
  address: {
    backgroundColor: '#1D2126',
    paddingTop: '16rem',
    paddingBottom: '10rem',
    paddingHorizontal: '16rem'
  },
  wrapper__button: {
    flexDirection: 'row',
    padding: '16rem',
    marginTop: 'auto'
  },
  bonus__modal: {
    borderRadius: '8rem',
    backgroundColor: '#FFFFFF',
    marginHorizontal: '16rem',
    padding: '16rem'
  },
  bonus__image__wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '104rem',
    height: '104rem',
    width: '104rem',
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    marginTop: '-40rem',
    marginBottom: '8rem'
  },
  bonus__image: {
    height: '80rem',
    width: '48rem',
    marginBottom: '-21rem'
  }
})
