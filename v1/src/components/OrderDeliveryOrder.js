import React from 'react'
import { Text, View } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Dash from 'react-native-dash'

const OrderDeliveryOrder = () => {
  return (
    <View style={styles.container}>
      <View style={styles.wrapperItemBills}>
        <View style={styles.flexDir}>
          <Text style={styles.textDesc}>
            Subtotal
          </Text>
          <Text style={styles.textCost}>
            Rp 60000
          </Text>
        </View>

        <View style={styles.flexDir}>
          <Text style={styles.textDesc}>
            Delivery Fee
          </Text>
          <Text style={styles.textCost}>
            Rp 12000
          </Text>
        </View>
      </View>

      <Dash
        dashColor='#DEDEDE'
        dashStyle={{ height: 1 }}
        dashGap={EStyleSheet.value('3rem')}
      />

      <View style={styles.wrapperTotal}>
        <Text style={styles.textDesc}>
          Total Payment
        </Text>

        <View style={{ alignItems: 'flex-end' }}>
          <Text style={styles.textCost}>
            Rp 42000
          </Text>
          <Text style={styles.textDiscount}>
            Rp 30000
          </Text>
        </View>
      </View>
    </View>
  )
}

export default OrderDeliveryOrder

const styles = EStyleSheet.create({
  container: {
    paddingHorizontal: '20rem'
  },
  wrapperItemBills: {
    paddingBottom: '10rem',
    marginTop: '40rem'
  },
  flexDir: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: '10rem'
  },
  textDesc: {
    fontFamily: 'Nunito-Regular',
    fontSize: '14rem',
    color: '#545454'
  },
  textCost: {
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem',
    color: '#3f3f3f'
  },
  wrapperTotal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '20rem'
  },
  textDiscount: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    color: '#989898',
    marginTop: '4rem',
    textDecorationLine: 'line-through'
  }
})
