import React from 'react'
import { Text, View, ActivityIndicator, StatusBar } from 'react-native'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'

const Loading = ({ type }) => {
  if (type === 'splash') {
    return (
      <>
        <View style={styles.container}>
          <StatusBar translucent backgroundColor='transparent' />
          <View style={styles.bgOverlay} />
          <ActivityIndicator size='large' color='#FF9F1C' />
          <Text style={styles.title}>Loading ...</Text>
        </View>
      </>
    )
  }
  return (
    <View style={styles.container}>
      <ActivityIndicator size='large' color='#FF9F1C' />
      <Text style={styles.title}>Loading ...</Text>
    </View>
  )
}

Loading.propTypes = {
  type: PropTypes.string
}

export default Loading

const styles = EStyleSheet.create({
  imgBg: {
    width: '100%',
    height: '100%',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  bgOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.5)'
  },
  container: {
    flex: 1,
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%'
  },
  title: {
    fontSize: '16rem',
    fontFamily: 'Nunito-Regular',
    color: '#FF9F1C',
    marginTop: '10rem'
  }
})
