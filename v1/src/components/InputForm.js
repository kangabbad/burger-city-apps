import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, TextInput } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import EStyleSheet from 'react-native-extended-stylesheet'

const InputForm = (props) => {
  const ItemLeft = props.leftItem === 'countryCode'
    ? (
      <Text style={styles.countryCode}>
        (+62)
      </Text>
    )
    : (
      <Icon
        name={props.iconName}
        size={props.iconSize}
        color='gray'
        style={styles.icon}
      />
    )
  return (
    <View style={styles.container}>
      {ItemLeft}

      <TextInput
        style={styles.input}
        placeholder={props.placeholder}
        onChangeText={props.onChangeText}
        value={props.value}
        secureTextEntry={props.secureTextEntry}
      />
    </View>
  )
}

InputForm.propTypes = {
  iconName: PropTypes.string,
  iconSize: PropTypes.number,
  leftItem: PropTypes.string,
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func,
  value: PropTypes.string,
  secureTextEntry: PropTypes.bool
}

export default InputForm

const styles = EStyleSheet.create({
  container: {
    paddingHorizontal: '10rem',
    backgroundColor: 'white',
    marginTop: '20rem',
    borderRadius: '8rem',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  input: {
    flex: 1,
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: '#727C8E'
  },
  icon: {
    marginHorizontal: '5rem',
    opacity: '0.5rem'
  },
  countryCode: {
    marginHorizontal: '5rem',
    opacity: '0.6rem',
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#8F8F8F'
  }
})
