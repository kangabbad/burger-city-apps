import React from 'react'
import { View, TouchableOpacity, Text, Image } from 'react-native'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'

import FontAwesome from 'react-native-vector-icons/FontAwesome'
import Feather from 'react-native-vector-icons/Feather'

import Vector from '../assets/icons/Vector.svg'
import Logout from '../assets/icons/Logout.svg'
import logo from '../../src/assets/icons/logo-black.webp'

const Header = (props) => {
  return (
    <View style={styles.header}>
      <LeftButton
        withBack={props.withBack}
        logout={props.logout}
        onPressLeftButton={props.onPressLeftButton}
      />

      <ImageHeader
        titleHeader={props.titleHeader}
      />

      <RightButton
        vector={props.vector}
        disabledIConRight={props.disabledIConRight}
        onPressRightButton={props.onPressRightButton}
      />
    </View>
  )
}

const LeftButton = ({ withBack, logout, onPressLeftButton }) => {
  let button = (
    <View style={styles.lang_btn}>
      <Text style={styles.text}>
        EN
      </Text>
      <Feather
        name='chevron-down'
        color='#ff9f1c'
        size={20}
        style={{ marginLeft: 2 }}
      />
    </View>
  )

  if (withBack) {
    button = (
      <Feather
        name='arrow-left'
        color='#ff9f1c'
        size={28}
        style={{ marginLeft: -5, marginVertical: -10 }}
      />
    )
  }

  if (logout) {
    button = (
      <Logout />
    )
  }

  return (
    <TouchableOpacity
      onPress={(onPressLeftButton)}
      style={{ ...styles.btn, padding: 15 }}
    >
      {button}
    </TouchableOpacity>
  )
}

const ImageHeader = ({ titleHeader }) => {
  if (titleHeader) {
    return (
      <Text style={styles.titleHeader}>
        Notification
      </Text>
    )
  }

  return (
    <View style={styles.logo}>
      <Image
        source={logo}
        resizeMode='contain'
      />
    </View>
  )
}

const RightButton = ({ disabledIConRight, vector, onPressRightButton }) => {
  let iconRight = (
    <FontAwesome
      name='bell'
      color='#CCCCCC'
      size={19}
    />
  )

  if (vector) {
    iconRight = (
      <Vector />
    )
  }

  if (disabledIConRight) {
    return null
  }

  return (
    <TouchableOpacity
      onPress={onPressRightButton}
      style={{ ...styles.btn }}
    >
      {iconRight}
    </TouchableOpacity>
  )
}

Header.propTypes = {
  withBack: PropTypes.bool,
  logout: PropTypes.bool,
  vector: PropTypes.bool,
  onPressLeftButton: PropTypes.func,
  onPressRightButton: PropTypes.func,
  disabledIConRight: PropTypes.bool,
  titleHeader: PropTypes.bool
}

LeftButton.propTypes = {
  withBack: PropTypes.bool,
  logout: PropTypes.bool,
  onPressLeftButton: PropTypes.func
}

ImageHeader.propTypes = {
  titleHeader: PropTypes.bool
}

RightButton.propTypes = {
  onPressRightButton: PropTypes.func,
  vector: PropTypes.bool,
  disabledIConRight: PropTypes.bool
}

export default Header

const styles = EStyleSheet.create({
  header: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    marginTop: '18rem',
    paddingVertical: '10rem'
  },
  btn: {
    paddingHorizontal: '20rem'
  },
  lang_btn: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  text: {
    fontFamily: 'Nunito-Bold',
    color: '#ff9f1c'
  },
  logo: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem',
    color: '#323232',
    position: 'absolute',
    top: 15,
    right: 0,
    left: 0
  }
})
