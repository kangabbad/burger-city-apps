import React from 'react'
import logoImg from '../assets/icons/logo.webp'
import { View, Image } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

export default function Logo () {
  return (
    <View style={{ flex: 1 }}>
      <Image source={logoImg} style={styles.logo} />
    </View>
  )
}

const styles = EStyleSheet.create({
  logo: {
    height: '64rem',
    resizeMode: 'contain'
  }
})
