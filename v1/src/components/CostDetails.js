import React from 'react'
import { Text, View, TouchableOpacity, FlatList } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Dash from 'react-native-dash'
import { useNavigation } from '@react-navigation/native'
import { Appbar } from 'react-native-paper'
import { useSelector } from 'react-redux'

const CostDetails = () => {
  const navigation = useNavigation()
  const cart = useSelector(state => state.cart)
  const { meals, salads, desserts, beverages, burgers } = cart
  const dataCart = [...meals, ...salads, ...desserts, ...beverages, ...burgers]
  let subtotalCart = 0
  dataCart.map((item) => { subtotalCart += item.count * item.price })
  const deliveryFee = 12000
  const totalPayment = subtotalCart + deliveryFee

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <FlatList
          listKey='cart-item'
          data={dataCart}
          keyExtractor={(item, index) => item + index.toString()}
          renderItem={({ item }) => {
            let name = ''
            if (item.category === 'meals') name = 'Value Meals'
            if (item.category === 'salads') name = 'Salads'
            if (item.category === 'desserts') name = 'Desserts'
            if (item.category === 'beverages') name = 'Beverages'
            if (item.category === 'burgers') name = 'Burgers'
            const totalPrice = item.count * item.price

            return (
              <View style={styles.item}>
                <View style={styles.name}>
                  <View style={styles.wrapperTotal}>
                    <Text style={styles.total}>
                      {item.count}x
                    </Text>
                  </View>

                  <View style={styles.details}>
                    <Text style={styles.itemName}>
                      {item.name}
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        navigation.navigate('MenuPacket', {
                          menu: item.category, name
                        })
                      }}
                    >
                      <Text style={styles.edit}>
                        Edit
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>

                <Text style={styles.textCost}>
                  Rp {totalPrice}
                </Text>
              </View>
            )
          }}
        />

        <Dash
          dashColor='#CACACA'
          dashStyle={{ height: 1 }}
          dashGap={EStyleSheet.value('3rem')}
        />

        <View style={styles.cartPrice}>
          <View style={styles.cartPriceWrapper}>
            <Text style={styles.textCost}>
              Subtotal
            </Text>
            <Text style={styles.textCost}>
              Rp {subtotalCart}
            </Text>
          </View>

          <View style={styles.cartPriceWrapper}>
            <Text style={styles.textCost}>
              Delivery Fee
            </Text>
            <Text style={styles.textCost}>
              Rp {deliveryFee}
            </Text>
          </View>
        </View>

        <Dash
          dashColor='#CACACA'
          dashStyle={{ height: 1 }}
          dashGap={EStyleSheet.value('3rem')}
        />

        <View style={styles.cartPrice}>
          <TouchableOpacity onPress={() => {}}>
            <View style={styles.cartPriceWrapper}>
              <Text style={styles.textPromo}>
                Add Your Promo Code
              </Text>
              <Appbar.Action
                icon='chevron-right'
                color='gray'
                size={EStyleSheet.value('14rem')}
                style={styles.chevron}
              />
            </View>
          </TouchableOpacity>
        </View>
      </View>

      <View style={styles.cartPriceTotal}>
        <Text style={styles.cartPriceTotalLabel}>
          Total Payment
        </Text>
        <Text style={styles.cartPriceTotalTitle}>
          Rp {totalPayment}
        </Text>
      </View>
    </View>
  )
}

export default CostDetails

const styles = EStyleSheet.create({
  container: {
    paddingVertical: '15rem'
  },
  wrapper: {
    backgroundColor: '#FFFFFF'
  },
  item: {
    borderTopWidth: 1,
    borderTopColor: '#FEFEFE',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingHorizontal: '25rem',
    paddingVertical: '15rem',
    paddingTop: '10rem'
  },
  name: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingRight: '30rem'
  },
  textCost: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem'
  },
  wrapperTotal: {
    width: '43rem',
    height: '43rem',
    textAlign: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: '43rem',
    backgroundColor: '#FAFAFA'
  },
  total: {
    fontSize: '13rem',
    fontFamily: 'Nunito-Bold',
    color: '#FF9F1C'
  },
  details: {
    marginLeft: '25rem'
  },
  itemName: {
    fontSize: '12rem',
    fontFamily: 'Nunito-bold',
    paddingRight: '30rem',
    marginBottom: '5rem'
  },
  edit: {
    fontSize: '12rem',
    color: '#FF9F1C',
    fontFamily: 'Nunito-Black'
  },
  cartPrice: {
    backgroundColor: '#FFFFFF',
    paddingVertical: '10rem'
  },
  cartPriceWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: '5rem',
    paddingHorizontal: '25rem'
  },
  textPromo: {
    fontFamily: 'Nunito-Bold',
    color: '#FF9F1C',
    fontSize: '14rem'
  },
  chevron: {
    backgroundColor: '#DEDEDE',
    paddingBottom: '2rem'
  },
  cartPriceTotal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    marginVertical: '10rem',
    paddingVertical: '15rem',
    paddingHorizontal: '25rem'
  },
  cartPriceTotalLabel: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem'
  },
  cartPriceTotalTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    color: '#FF9F1C'
  }
})
