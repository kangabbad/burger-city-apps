import React from 'react'
import { View, Image, StatusBar, TouchableOpacity, Text } from 'react-native'
import { Appbar } from 'react-native-paper'
import PropTypes from 'prop-types'
import menuLogo from '../assets/icons/menu-logo.webp'
import EStyleSheet from 'react-native-extended-stylesheet'
import Logout from '../assets/icons/Logout.svg'
import Feather from 'react-native-vector-icons/Feather'
import Vector from '../assets/icons/Vector.svg'

const MenuHeader = (props) => {
  const { navigation, icon, onRightPress, rightPress, right, leftPress, left, notifScreen, onBackCallback } = props

  const LeftButton = left === 'en'
    ? (
      <TouchableOpacity
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'flex-start',
          paddingHorizontal: '3%'
        }}
      >
        <Text style={{ fontFamily: 'Nunito-Black', color: '#ff9f1c' }}>EN</Text>
        <Feather
          name='chevron-down'
          color='#ff9f1c'
          size={20}
          style={{ marginLeft: 2 }}
        />
      </TouchableOpacity>
    )
    : left === 'logout'
      ? <Logout onPress={leftPress} />
      : (
        <Appbar.BackAction
          color='#FF9F1C'
          onPress={() => {
            navigation.goBack()
            onBackCallback()
          }}
        />
      )

  const notif = props.icon !== 'bell'
    ? null
    : props.notif > 0 && <View style={styles.notif} />

  const RightButton = right === 'profile'
    ? <Vector onPress={onRightPress} />
    : <Appbar.Action icon={icon} onPress={() => navigation.navigate(rightPress)} color={props.icon === 'star' ? '#FF9F1C' : 'gray'} />
  const Logo = notifScreen
    ? (
      <Text style={styles.titleHeader}>
        Notification
      </Text>
    )
    : <Image source={menuLogo} />
  return (
    <View style={right === 'profile' ? styles.profileHeader : styles.header}>
      <StatusBar barStyle='dark-content' translucent backgroundColor='transparent' />
      {LeftButton}
      <View style={styles.logo}>
        {Logo}
      </View>
      <View>
        {RightButton}
        {notif}
      </View>
    </View>
  )
}

export default MenuHeader

MenuHeader.defaultProps = {
  onBackCallback: () => {}
}

MenuHeader.propTypes = {
  navigation: PropTypes.object,
  icon: PropTypes.string,
  onRightPress: PropTypes.func,
  rightPress: PropTypes.string,
  right: PropTypes.string,
  leftPress: PropTypes.func,
  left: PropTypes.string,
  notifScreen: PropTypes.bool,
  notif: PropTypes.number,
  onBackCallback: PropTypes.func
}

const styles = EStyleSheet.create({
  header: {
    backgroundColor: 'white',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: '80rem',
    paddingTop: '30rem',
    paddingHorizontal: '10rem'
  },
  profileHeader: {
    backgroundColor: 'white',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    height: '80rem',
    paddingTop: '25rem',
    paddingHorizontal: '25rem'
  },
  notif: {
    width: '8rem',
    height: '8rem',
    borderRadius: 8,
    backgroundColor: '#FF9F1C',
    position: 'absolute',
    right: '10rem',
    top: '10rem'
  },
  logo: {
    position: 'absolute',
    left: '8rem',
    right: 0,
    top: '35rem',
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleHeader: {
    textAlign: 'center',
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem',
    color: '#323232',
    position: 'relative',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0
  }
})
