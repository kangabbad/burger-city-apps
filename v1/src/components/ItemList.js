import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Text, View, FlatList, Image } from 'react-native'
import { Appbar } from 'react-native-paper'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { showMessage } from 'react-native-flash-message'
import { setCountItemsMenu } from '../redux/actions/menu'
import { storeData } from '../../util/LocalStorage'
import { addToFavorite } from '../redux/actions/favorite'

import ActionButton from './ActionButton'
import imgPlaceholder from '../assets/images/itemMenuPacket.webp'

const ItemList = ({ data, menuName, itemTypeIcon }) => {
  const navigation = useNavigation()
  const dispatch = useDispatch()
  const remakeItemsMenu = useSelector(state => state.menu.remakeItemsMenu)
  const [imgLoaded, setLoad] = useState(false)

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={remakeItemsMenu}
      keyExtractor={(_item, index) => index.name + index.toString()}
      renderItem={({ item, index }) => {
        const imgSrc = item.img_url !== undefined && imgLoaded ? { uri: item.img_url } : imgPlaceholder
        return (
          <View style={styles.container}>
            <View style={styles.itemLeft}>
              <Image
                source={imgSrc}
                resizeMode='contain'
                style={
                  item.price !== undefined
                    ? styles.image
                    : styles.imagePlaceholder
                }
                onLoad={() => setLoad(true)}
                onLoadEnd={() => setLoad(true)}
              />
            </View>

            <View style={styles.itemMiddle}>
              <Text
                style={styles.textTitle}
                numberOfLines={3}
              >
                {item.name}
              </Text>
              <Text style={styles.textSubTitle}>
                {item.price !== undefined && `Rp. ${item.price}`}
              </Text>
            </View>

            <View style={styles.itemRight}>
              {item.price !== undefined && (
                <>
                  <Appbar.Action
                    icon={itemTypeIcon}
                    onPress={() => {
                      if (menuName === 'meals') {
                        storeData('isLoadApi', false)
                        navigation.navigate('ChangePacket', {
                          index,
                          name: item.name,
                          image: item.img_url,
                          menuName: menuName
                        })
                      } else {
                        dispatch(addToFavorite(menuName, { ...item, category: menuName }))
                        showMessage({
                          animationDuration: 150,
                          message: 'Yeay!',
                          description: `${item.name} was added to your Favorite`,
                          type: 'none',
                          position: 'bottom',
                          backgroundColor: '#FF9F1C',
                          color: '#FFFFFF',
                          titleStyle: {
                            fontSize: EStyleSheet.value('16rem')
                          }
                        })
                      }
                    }}
                    size={EStyleSheet.value(itemTypeIcon !== 'star' ? '16rem' : '20rem')}
                    color={itemTypeIcon !== 'star' ? '#7e7e7e' : '#dedede'}
                    style={itemTypeIcon !== 'star' && styles.icon}
                  />

                  <View style={{ width: EStyleSheet.value('75rem') }}>
                    {item.count === 0 ? (
                      <ActionButton
                        styleButton={styles.styleButton}
                        titleButton='Add +'
                        titleButtonStyle={styles.titleButtonStyle}
                        onHandlePress={() => {
                          dispatch(setCountItemsMenu(index, '+'))
                        }}
                      />
                    ) : (
                      <View style={styles.plusMinusWrap}>
                        <Appbar.Action
                          icon='minus'
                          size={14}
                          style={styles.plusMinusButton}
                          onPress={() => {
                            dispatch(setCountItemsMenu(index, '-'))
                          }}
                        />
                        <Text style={styles.counterTitle}>
                          {item.count}
                        </Text>
                        <Appbar.Action
                          icon='plus'
                          size={14}
                          style={styles.plusMinusButton}
                          onPress={() => {
                            dispatch(setCountItemsMenu(index, '+'))
                          }}
                        />
                      </View>
                    )}

                  </View>
                </>
              )}
            </View>
          </View>
        )
      }}
    />
  )
}

export default ItemList

ItemList.propTypes = {
  data: PropTypes.array,
  itemTypeIcon: PropTypes.string,
  menuName: PropTypes.string
}

const styles = EStyleSheet.create({
  container: {
    // borderWidth: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: '10rem'
  },
  itemLeft: {
    alignItems: 'center'
  },
  image: {
    height: '60rem',
    width: '60rem'
  },
  imagePlaceholder: {
    width: '60rem',
    height: '60rem',
    marginVertical: '5rem',
    borderRadius: '5rem'
  },
  textTitle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    paddingRight: '5rem'
  },
  textSubTitle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#E59413',
    marginTop: '3rem'
  },
  itemMiddle: {
    justifyContent: 'center',
    paddingRight: '5rem',
    width: '120rem'
  },
  itemRight: {
    alignItems: 'center'
  },
  icon: {
    backgroundColor: '#dedede',
    fontWeight: 'bold'
  },
  styleButton: {
    paddingHorizontal: '14rem',
    height: '27rem',
    backgroundColor: '#ff9f1c',
    borderRadius: '4rem',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#ffffff'
  },
  counterTitle: {
    fontSize: '12rem',
    paddingVertical: '6rem'
  },
  plusMinusWrap: {
    height: '27rem',
    borderRadius: 8,
    flexDirection: 'row',
    backgroundColor: '#ffffff',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  plusMinusButton: {
    backgroundColor: '#eaeaea',
    margin: 0,
    marginHorizontal: '5rem'
  }
})
