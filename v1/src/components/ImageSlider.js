import React from 'react'
import { View, Dimensions, Image } from 'react-native'
import Carousel from 'react-native-snap-carousel'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'
import imgPlaceHolder from '../assets/images/ourBurgerSlider.webp'

const { width, height } = Dimensions.get('window')
const _renderItem = ({ item, index }) => {
  return (
    <View style={styles.container}>
      <Image source={item.img_url} style={{ width: width * 80 / 100, height: height / 4, borderRadius: 8 }} key={index} />
    </View>
  )
}

const ImageSlider = (props) => {
  const imgBanner = [
    {
      img_url: imgPlaceHolder
    },
    {
      img_url: imgPlaceHolder
    },
    {
      img_url: imgPlaceHolder
    }
  ]
  props.data.map((item, index) => {
    if (index === 0) imgBanner.splice(0, 3)
    imgBanner.push({ img_url: { uri: item.img_url } })
  })

  return (
    <Carousel
      data={imgBanner}
      renderItem={_renderItem}
      loop
      enableSnap
      autoplay
      sliderWidth={width}
      itemWidth={width * 85 / 100}
      itemHeight={height}
      sliderHeight={height}
      inactiveSlideScale={1}
      autoplayInterval={10000}
    />
  )
}

export default ImageSlider

_renderItem.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number
}
ImageSlider.propTypes = {
  data: PropTypes.array
}

ImageSlider.defaultProps = {
  data: {
    img_url: require('../assets/images/banner.png')
  }
}
const styles = EStyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: '25rem'
  }
})
