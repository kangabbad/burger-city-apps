import React from 'react'
import PropTypes from 'prop-types'
import { Text, View } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

export default function TitleScreen (props) {
  return (
    <View style={styles.title}>
      <Text style={styles.fontTitle}> {props.mainTitle} </Text>
      {props.secondTitle !== null
        ? <Text style={styles.secondTitle}>{props.secondTitle} </Text>
        : null}
    </View>
  )
}

TitleScreen.propTypes = {
  mainTitle: PropTypes.string,
  secondTitle: PropTypes.string
}

const styles = EStyleSheet.create({
  title: {
    alignItems: 'center'
  },
  fontTitle: {
    color: 'white',
    fontFamily: 'Nunito-Black',
    fontSize: '18rem'
  },
  secondTitle: {
    fontSize: '13rem',
    color: '#FFFFFF',
    fontFamily: 'Nunito-Bold',
    textAlign: 'center',
    marginTop: '8rem'
  }
})
