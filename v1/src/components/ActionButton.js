import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, TouchableOpacity } from 'react-native'

const ActionButton = (props) => {
  return (
    <TouchableOpacity
      onPress={props.onHandlePress}
      style={props.styleButton}
    >
      <LeftItem
        wrapperBadge={props.wrapperBadge}
        itemLeft={props.itemLeft}
        itemLeftStyle={props.itemLeftStyle}
      />

      <MiddleItem
        titleButton={props.titleButton}
        titleButtonStyle={props.titleButtonStyle}
      />

      <RightItem
        itemRight={props.itemRight}
        itemRightStyle={props.itemRightStyle}
      />
    </TouchableOpacity>
  )
}

const LeftItem = (props) => {
  return (
    <View style={props.wrapperBadge}>
      <Text style={props.itemLeftStyle}>
        {props.itemLeft}
      </Text>
    </View>
  )
}

const MiddleItem = (props) => {
  return (
    <Text style={props.titleButtonStyle}>
      {props.titleButton}
    </Text>
  )
}

const RightItem = (props) => {
  return (
    <View style={props.itemRightStyle}>
      {props.itemRight}
    </View>
  )
}

ActionButton.propTypes = {
  onHandlePress: PropTypes.func,
  styleButton: PropTypes.object,
  wrapperBadge: PropTypes.object,
  itemLeftStyle: PropTypes.object,
  itemLeft: PropTypes.string,
  titleButtonStyle: PropTypes.object,
  titleButton: PropTypes.string,
  itemRightStyle: PropTypes.object,
  itemRight: PropTypes.object
}

LeftItem.propTypes = {
  wrapperBadge: PropTypes.object,
  itemLeftStyle: PropTypes.object,
  itemLeft: PropTypes.string
}

MiddleItem.propTypes = {
  titleButtonStyle: PropTypes.object,
  titleButton: PropTypes.string
}

RightItem.propTypes = {
  itemRightStyle: PropTypes.object,
  itemRight: PropTypes.object
}

export default ActionButton
