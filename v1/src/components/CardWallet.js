import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Text, View, ImageBackground, Image, FlatList, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useNavigation } from '@react-navigation/native'
import Ionicons from 'react-native-vector-icons/Ionicons'

import danaIcon from '../assets/icons/dana.webp'
import gopayIcon from '../assets/icons/go-pay-logo.png'
import ovoIcon from '../assets/icons/ovo.webp'
import linkajaIcon from '../assets/icons/link-aja.webp'
import codIcon from '../assets/icons/DeliveryOrder.webp'

import bgDana from '../assets/images/bg-dana.webp'
import bgGopay from '../assets/images/bg-gopay.webp'
import bgOvo from '../assets/images/bg-ovo.webp'
import bgLinkaja from '../assets/images/bg-linkaja.webp'
import bgCOD from '../assets/images/bg-cod.webp'

const CardWallet = ({ leftItemWallet }) => {
  const [dataWallet, setDataWallet] = useState([
    {
      imgBg: bgCOD,
      iconWallet: codIcon,
      nameCardWallet: 'COD',
      titleLogin: 'Cash On Delivery',
      balance: '',
      nominalBalance: '0',
      nameUserWallet: '',
      phoneNumber: '',
      pin: '',
      selected: true
    },
    {
      imgBg: bgDana,
      iconWallet: danaIcon,
      nameCardWallet: 'Dana',
      titleLogin: 'Click Here\nto Log-in',
      balance: 'Balance',
      nominalBalance: '100000000',
      nameUserWallet: 'Darrell Steward',
      phoneNumber: '',
      pin: '',
      selected: false
    },
    {
      imgBg: bgGopay,
      iconWallet: gopayIcon,
      nameCardWallet: 'GoPay',
      titleLogin: 'Click Here\nto Log-in',
      balance: 'Balance',
      nominalBalance: '100000000',
      nameUserWallet: 'Darrell Steward',
      phoneNumber: '',
      pin: '',
      selected: false
    },
    {
      imgBg: bgOvo,
      iconWallet: ovoIcon,
      nameCardWallet: 'OVO',
      titleLogin: 'Click Here\nto Log-in',
      balance: 'Balance',
      nominalBalance: '100000000',
      nameUserWallet: 'Darrell Steward',
      phoneNumber: '',
      pin: '',
      selected: false
    },
    {
      imgBg: bgLinkaja,
      iconWallet: linkajaIcon,
      nameCardWallet: 'LinkAja',
      titleLogin: 'Click Here\nto Log-in',
      balance: 'Balance',
      nominalBalance: '100000000',
      nameUserWallet: 'Darrell Steward',
      phoneNumber: '',
      pin: '',
      selected: false
    }
  ])

  return (
    <FlatList
      data={dataWallet}
      keyExtractor={(item, index) => item + index.toString()}
      horizontal
      showsHorizontalScrollIndicator={false}
      contentContainerStyle={{
        paddingHorizontal: EStyleSheet.value('16rem'),
        paddingTop: EStyleSheet.value('16rem')
      }}
      renderItem={({ item, index }) => {
        return (
          <RenderWalletItem
            item={item}
            index={index}
            setDataWallet={setDataWallet}
            leftItemWallet={leftItemWallet}
          />
        )
      }}
    />
  )
}

const RenderWalletItem = ({ item, index, setDataWallet, leftItemWallet }) => {
  const navigation = useNavigation()

  const heightIcon = index === 1 ? 38 : 20 && index === 3 ? 8 : 20
  const widhtIcon = index === 1 ? 38 : 20 && index === 3 ? 24 : 20

  const marginLogin = index === 0 ? 94 : 84

  const ribuan = item.nominalBalance.match(/\d{1,3}/g)
  const nominal = ribuan.join('.').split('').join('')

  const ItemLeftWallet = () => {
    if (leftItemWallet === 'paymentMethod' && index !== 0) {
      return (
        <View style={styles.wrapperDataWallet}>
          <Text style={styles.balance}>
            {item.balance}
          </Text>
          <Text style={styles.nominalBalance}>
            Rp {nominal}
          </Text>
          <Text style={styles.phoneNumber}>
            +62 87632653627
          </Text>
          <Text style={styles.nameUserWallet}>
            {item.nameUserWallet}
          </Text>
        </View>
      )
    } else if (leftItemWallet === 'profileScreen' && item.pin !== '') {
      return (
        <View style={styles.wrapperDataWallet}>
          <Text style={styles.balance}>
            {item.balance}
          </Text>
          <Text style={styles.nominalBalance}>
            Rp {nominal}
          </Text>
          <Text style={styles.phoneNumber}>
            {item.phoneNumber}
          </Text>
          <Text style={styles.nameUserWallet}>
            {item.nameUserWallet}
          </Text>
        </View>
      )
    } else {
      return (
        <TouchableOpacity
          onPress={() => navigation.navigate('InputPhoneNumber', {
            index: index,
            setDataWallet: setDataWallet
          })}
          style={{ marginTop: marginLogin }}
        >
          <Text style={styles.titleLogin}>
            {item.titleLogin}
          </Text>
        </TouchableOpacity>
      )
    }
  }

  return (
    <ImageBackground
      source={item.imgBg}
      resizeMode='stretch'
      style={styles.imgBgWallet}
    >
      <View style={styles.wrapperCardWallet}>
        <SelectedMark isShown={item.selected} />
        <View style={styles.wrapperItemLeft}>
          <ItemLeftWallet />
        </View>

        <View style={styles.wrapperItemRight}>
          <Text style={styles.nameCardWallet}>
            {item.nameCardWallet}
          </Text>

          <View style={styles.wrapperIconWallet}>
            <Image
              source={item.iconWallet}
              style={{
                height: heightIcon,
                width: widhtIcon
              }}
            />
          </View>
        </View>
      </View>
    </ImageBackground>
  )
}

const SelectedMark = ({ isShown }) => {
  if (isShown) {
    return (
      <Ionicons
        name='ios-checkmark-done-circle'
        color='#04e762'
        size={EStyleSheet.value('28rem')}
        style={{
          position: 'absolute',
          top: EStyleSheet.value('-10rem'),
          left: EStyleSheet.value('-10rem'),
          backgroundColor: '#FFFFFF',
          borderRadius: EStyleSheet.value('26rem')
        }}
      />
    )
  }
  return null
}

export default CardWallet

CardWallet.propTypes = {
  leftItemWallet: PropTypes.string
}

RenderWalletItem.propTypes = {
  item: PropTypes.object,
  index: PropTypes.number,
  setDataWallet: PropTypes.func,
  leftItemWallet: PropTypes.string
}

SelectedMark.propTypes = {
  isShown: PropTypes.bool
}

const styles = EStyleSheet.create({
  imgBgWallet: {
    width: '220rem',
    height: '125rem',
    marginHorizontal: '6rem'
  },
  wrapperCardWallet: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  wrapperItemLeft: {
    marginLeft: '20rem'
  },
  titleLogin: {
    fontFamily: 'Nunito-Bold',
    fontSize: '13rem',
    color: '#ffffff'
  },
  wrapperDataWallet: {
    marginTop: '20rem'
  },
  balance: {
    fontFamily: 'Nunito-Regular',
    fontSize: '10rem',
    color: '#ffffff'
  },
  nominalBalance: {
    fontFamily: 'Nunito-Regular',
    fontSize: '15rem',
    color: '#ffffff'
  },
  phoneNumber: {
    fontFamily: 'Nunito-Bold',
    fontSize: '12rem',
    color: '#ffffff',
    marginTop: '22rem'
  },
  nameUserWallet: {
    fontFamily: 'Nunito-Regular',
    fontSize: '10rem',
    color: '#ffffff'
  },
  wrapperItemRight: {
    alignItems: 'center',
    marginTop: '10rem',
    marginRight: '15rem'
  },
  nameCardWallet: {
    fontFamily: 'Nunito-Bold',
    fontSize: '18rem',
    color: '#ffffff'
  },
  wrapperIconWallet: {
    borderRadius: '32rem',
    height: '32rem',
    width: '32rem',
    top: '42rem',
    right: '0rem',
    bottom: '0rem',
    left: '0rem',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff'
  },
  iconWallet: {
    width: '18rem',
    height: '18rem'
  }
})
