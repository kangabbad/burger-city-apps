import React, { useState } from 'react'
import { Text, View, FlatList, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { Appbar } from 'react-native-paper'
import deliver from '../assets/icons/delivery.png'
import store from '../assets/icons/store.png'
import car from '../assets/icons/car.png'

const OrderMethod = () => {
  const [isActive, setActive] = useState({
    store: true,
    delivery: false,
    drive: false
  })

  const data = [
    {
      key: '0',
      icon: store,
      method: 'In - Store',
      onPress: () => {
        setActive(prevState => ({
          drive: false,
          delivery: false,
          store: true
        }))
      },
      active: isActive.store
    },
    {
      key: '1',
      icon: deliver,
      method: 'Delivery',
      onPress: () => {
        setActive(prevState => ({
          store: false,
          drive: false,
          delivery: true
        }))
      },
      active: isActive.delivery
    },
    {
      key: '2',
      icon: car,
      method: 'Drive Thru',
      onPress: () => {
        setActive(prevState => ({
          store: false,
          delivery: false,
          drive: true
        }))
      },
      active: isActive.drive
    }
  ]

  return (
    <FlatList
      showsVerticalScrollIndicator={false}
      data={data}
      keyExtractor={index => index.key}
      renderItem={({ item }) => (
        <TouchableOpacity onPress={() => item.onPress()}>
          <View style={styles.wrap}>
            <View style={styles.method}>
              <Appbar.Action
                icon={item.icon}
                style={styles.iconMethod}
                size={EStyleSheet.value('20rem')}
                color='#92A1B1'
              />
              <Text style={styles.text}>
                {item.method}
              </Text>
            </View>
            <View style={styles.wrapperCheck}>
              <Appbar.Action
                icon='check'
                size={EStyleSheet.value('12rem')}
                color={item.active ? '#FFFFFF' : '#000000'}
                style={item.active ? { backgroundColor: '#FF9F1C' } : { backgroundColor: 'rgba(168, 168, 168, .5)' }}
              />
            </View>
          </View>
        </TouchableOpacity>
      )}
    />
  )
}

export default OrderMethod

const styles = EStyleSheet.create({
  wrap: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  method: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  iconMethod: {
    marginRight: '20rem'
  },
  text: {
    fontSize: '13rem',
    fontFamily: 'Nunito-Regular',
    color: '#121212'
  },
  wrapperCheck: {
  },
  active: {
    backgroundColor: '#FF9F1C',
    borderRadius: '100rem',
    padding: '3rem'
  },
  inActive: {
    backgroundColor: '#E3E5E8',
    borderRadius: '100rem',
    padding: '3rem'
  }
})
