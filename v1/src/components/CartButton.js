import React from 'react'
import { Text, TouchableOpacity } from 'react-native'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'

const CartButton = (props) => {
  return (
    <TouchableOpacity
      onPress={() => props.press.navigate('CartScreen')}
      style={styles.button}
    >
      <Text style={styles.totalItem}>1</Text>
      <Text style={styles.title}>{props.titleButton}</Text>
      <Text style={styles.harga}>Rp 50.000</Text>
    </TouchableOpacity>
  )
}

export default CartButton

CartButton.propTypes = {
  // onHandlePress: PropTypes.func,
  titleButton: PropTypes.string,
  press: PropTypes.object
}

CartButton.defaultProps = {
  styleButton: null,
  onHandlePress: () => {}
}
const styles = EStyleSheet.create({
  button: {
    backgroundColor: '#FF9F1C',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: '12rem',
    marginTop: '10rem',
    borderRadius: '5rem',
    paddingHorizontal: '10rem'
  },
  totalItem: {
    color: '#FF9F1C',
    fontFamily: 'Nunito-Bold',
    backgroundColor: 'white',
    paddingVertical: '2rem',
    paddingHorizontal: '8rem',
    borderRadius: '2rem'
  },
  title: {
    color: 'white',
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    position: 'absolute',
    alignSelf: 'center',
    flex: 1,
    left: 0,
    right: 0,
    textAlign: 'center',
    fontWeight: 'bold'
  },
  harga: {
    color: 'white',
    fontFamily: 'Nunito-Bold'
  }
})
