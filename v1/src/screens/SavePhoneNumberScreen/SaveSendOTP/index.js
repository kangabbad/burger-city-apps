import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Text, View, Image, StatusBar } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Feather from 'react-native-vector-icons/Feather'

import ActionButton from '../../../components/ActionButton'
import InputForm from '../../../components/InputForm'
import Loading from '../../../components/Loading'

import telephone from '../../../assets/icons/telephone.webp'

const PhoneNumber = ({ navigation }) => {
  const [isLoading, setLoading] = useState(false)

  const onSubmit = () => {
    setLoading(true)
    setTimeout(() => {
      setLoading(false)
      navigation.navigate('SettingProfile')
    }, 3000)
  }

  return (
    <>
      <View style={styles.container}>
        <StatusBar
          translucent
          barStyle='dark-content'
          backgroundColor='transparent'
        />

        <View style={styles.badge}>
          <Feather
            onPress={() => navigation.navigate('SavePhoneNumber')}
            name='arrow-left'
            color='#3C3833'
            size={24}
          />
        </View>

        <View style={styles.wrapperLogo}>
          <Image
            source={telephone}
            style={styles.logo}
          />

          <Text style={styles.styleTitleLogo}>
            Telephone
          </Text>
        </View>

        <Text style={styles.textTitle}>
          Masukan Kode Verifikasi yang telah dikirimkan ke nomor anda
        </Text>

        <View style={styles.inputForm}>
          <InputForm
            placeholder='OTP Code'
          />
        </View>

        <ActionButton
          styleButton={styles.styleButton}
          titleButton='SUBMIT'
          titleButtonStyle={styles.titleButtonStyle}
          onHandlePress={onSubmit}
        />
      </View>
      {isLoading && <Loading />}
    </>
  )
}

export default PhoneNumber

PhoneNumber.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '40rem'
  },
  badge: {
    position: 'absolute',
    top: '32rem',
    left: '22rem'
  },
  wrapperLogo: {
    alignItems: 'center',
    marginTop: '65rem'
  },
  logo: {
    width: '70rem',
    height: '60rem'
  },
  styleTitleLogo: {
    fontFamily: 'Nunito-Bold',
    fontSize: '24rem',
    color: '#25D366',
    marginTop: '2rem'
  },
  textTitle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '14rem',
    color: '#8F8F8F',
    textAlign: 'center',
    marginTop: '30rem',
    paddingHorizontal: '8rem'
  },
  inputForm: {
    marginTop: '40rem'
  },
  styleButton: {
    marginTop: '40rem',
    paddingVertical: '14rem',
    backgroundColor: '#25D366',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: 'white'
  }
})
