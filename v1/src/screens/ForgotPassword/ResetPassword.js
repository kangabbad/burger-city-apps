import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  ImageBackground,
  Text,
  StatusBar
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Bg from '../../assets/images/background-image.webp'
import Logo from '../../components/Logo'
import TitleScreen from '../../components/TitleScreen'
import InputForm from '../../components/InputForm'
import ActionButton from '../../components/ActionButton'

const { height, width } = Dimensions.get('window')
const ResetPassword = ({ navigation }) => {
  return (
    <ImageBackground source={Bg} style={{ ...styles.imgBg, height: height }}>
      <StatusBar translucent backgroundColor='transparent' />
      <View style={styles.bgOverlay} />
      <View style={{ ...styles.contentWrapper, height: height }}>
        <Logo />
        <View style={styles.wrapper}>
          <TitleScreen mainTitle='Forget Password' />
          <Text style={styles.textHeader}>
            Please enter a new password and confirm the password
          </Text>
          <View style={styles.formWrapper}>
            <InputForm
              iconName='lock'
              iconSize={18}
              placeholder='New Password'
            />
            <InputForm
              iconName='lock'
              iconSize={18}
              placeholder='Confirm Password'
            />
            <ActionButton
              styleButton={styles.styleButton}
              titleButton='Submit'
              titleButtonStyle={styles.titleButtonStyle}
              onHandlePress={() => navigation.navigate('reset password')}
            />
          </View>
        </View>
      </View>
    </ImageBackground>
  )
}

export default ResetPassword

ResetPassword.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  imgBg: {
    flex: 1,
    width: null,
    height: null
  },
  bgOverlay: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5
  },
  contentWrapper: {
    top: 0,
    left: 0,
    alignItems: 'center',
    position: 'absolute',
    marginTop: '15%'
  },
  wrapper: {
    height: height / 1.15,
    width: width,
    paddingHorizontal: '10rem',
    paddingTop: '20rem'
  },
  textHeader: {
    color: 'white',
    fontFamily: 'Nunito-Regular',
    textAlign: 'center',
    marginHorizontal: '30rem',
    fontSize: '14rem'
  },
  formWrapper: {
    paddingHorizontal: '20rem',
    marginTop: '5%'
  },
  styleButton: {
    marginTop: '20rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  }
})
