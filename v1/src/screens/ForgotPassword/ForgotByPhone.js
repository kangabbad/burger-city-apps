import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  ImageBackground,
  Text,
  StatusBar
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Bg from '../../assets/images/background-image.webp'
import Logo from '../../components/Logo'
import TitleScreen from '../../components/TitleScreen'
import InputForm from '../../components/InputForm'
import ActionButton from '../../components/ActionButton'

const { height, width } = Dimensions.get('window')
const ForgotByPhone = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor='transparent' />
      <ImageBackground source={Bg} style={{ ...styles.imgBg, height: height }}>
        <View style={styles.bgOverlay} />
        <View style={{ ...styles.contentWrapper, height: height }}>
          <Logo />
          <View style={styles.wrapper}>
            <TitleScreen mainTitle='Forget Password' />
            <Text style={styles.textHeader}>
              A text message with a 4-digit verification code{' '}
              <Text style={styles.fontBold}>was just send to *** *** **78</Text>
            </Text>
            <View style={styles.formWrapper}>
              <InputForm
                iconName='phone'
                iconSize={18}
                placeholder='Mobile Number'
              />
              <ActionButton
                styleButton={styles.styleButton}
                titleButton='Proceed'
                titleButtonStyle={styles.titleButtonStyle}
                onHandlePress={() => navigation.navigate('send otp')}
              />
              <Text style={styles.textPhone}>
                With{' '}
                <Text
                  style={styles.textPhoneChild}
                  onPress={() => navigation.navigate('forgot by email')}
                >
                  Email
                </Text>
              </Text>
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  )
}

export default ForgotByPhone

ForgotByPhone.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    ...EStyleSheet.absoluteFill
  },
  imgBg: {
    flex: 1,
    width: null,
    height: null
  },
  bgOverlay: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5
  },
  contentWrapper: {
    top: 0,
    left: 0,
    alignItems: 'center',
    position: 'absolute',
    marginTop: '15%'
  },
  wrapper: {
    height: height / 1.15,
    width: width,
    paddingHorizontal: '10rem',
    paddingTop: '20rem'
  },
  textHeader: {
    fontFamily: 'Nunito-Regular',
    color: 'white',
    textAlign: 'center',
    fontSize: '14rem'
  },
  fontBold: {
    fontFamily: 'Nunito-Bold'
  },
  formWrapper: {
    paddingHorizontal: '20rem',
    marginTop: '5%'
  },
  styleButton: {
    marginTop: '20rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  },
  textPhone: {
    textAlign: 'center',
    marginTop: '10rem',
    color: '#FF9F1C',
    fontFamily: 'Nunito-Bold'
  },
  textPhoneChild: {
    color: 'white',
    textDecorationLine: 'underline'
  }
})
