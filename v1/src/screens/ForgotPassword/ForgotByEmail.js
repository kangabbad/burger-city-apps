
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  ImageBackground,
  Text,
  StatusBar
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Bg from '../../assets/images/background-image.webp'
import Logo from '../../components/Logo'
import TitleScreen from '../../components/TitleScreen'
import InputForm from '../../components/InputForm'
import ActionButton from '../../components/ActionButton'
import { useForm } from '../../../util/UseForm'
import Modal from 'react-native-modal'
import Fire from '../../../util/Fire'
import { showMessage } from 'react-native-flash-message'
import Feather from 'react-native-vector-icons/Feather'
import Loading from '../../components/Loading'

const { height, width } = Dimensions.get('window')
const ForgotByEmail = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <RenderStatusBar />
      <RenderInput navigation={navigation} />
    </View>
  )
}

const RenderStatusBar = () => {
  return (
    <StatusBar translucent backgroundColor='transparent' />
  )
}

const RenderInput = ({ navigation }) => {
  const [form, setForm] = useForm({ email: '' })
  const [loading, setLoading] = useState(false)
  const [isModalVisible, setModalVisible] = useState(false)

  const isGetEmail = () => {
    setLoading(true)
    Fire.auth().sendPasswordResetEmail(form.email)
      .then(() => {
        setLoading(false)
        setModalVisible(true)
        setForm('reset')
      }).catch(err => {
        setLoading(false)
        showMessage({
          message: err.message,
          type: 'default',
          position: 'top',
          backgroundColor: '#c0392b',
          color: '#FFFFFF',
          style: {
            marginTop: 20,
            alignItems: 'center'
          }
        })
      })
  }

  const toggleModal = () => {
    setModalVisible(false)
    setTimeout(() => {
      navigation.navigate('LoginScreen')
    }, 500)
  }

  useEffect((prevState) => {
    if (isModalVisible !== prevState) {
      if (isModalVisible) {
        setTimeout(() => {
          StatusBar.setBackgroundColor('rgba(0, 0, 0, 0.7)', true)
        }, 10)
      } else {
        setTimeout(() => {
          StatusBar.setBackgroundColor('transparent', true)
        }, 10)
      }
    }
  })

  return (
    <>
      <ImageBackground source={Bg} style={{ ...styles.imgBg, height: height }}>
        <View style={styles.bgOverlay} />
        <View style={{ ...styles.contentWrapper, height: height }}>
          <Logo />
          <View style={styles.wrapper}>
            <TitleScreen mainTitle='Forget Password' />
            <Text style={styles.textHeader}>
              A text message with a 4-digit verification code
              <Text style={styles.fontBold}>
                was just send to *****@gmail.com
              </Text>
            </Text>
            <View style={styles.formWrapper}>
              <InputForm
                iconName='envelope'
                iconSize={18}
                placeholder='Email Address'
                value={form.email}
                onChangeText={value => setForm('email', value)}
              />
              <ActionButton
                styleButton={styles.styleButton}
                titleButton='Proceed'
                titleButtonStyle={styles.titleButtonStyle}
                onHandlePress={isGetEmail}
              />
              <Modal useNativeDriver isVisible={isModalVisible} onBackButtonPress={toggleModal}>
                <View style={styles.modal}>
                  <View style={styles.topImageWrapper}>
                    <View style={styles.imageModal}>
                      <Feather name='check' color='#FFFFFF' size={55} />
                    </View>
                  </View>
                  <Text style={styles.titleModal}>
                    SENDING ...
                  </Text>
                  <Text style={styles.descModal}>
                    We're sending the link for verify your account. Please check your mailbox
                  </Text>
                  <ActionButton
                    styleButton={styles.styleButtonModal}
                    titleButton='DONE'
                    titleButtonStyle={styles.titleButtonStyle}
                    onHandlePress={toggleModal}
                  />
                </View>
              </Modal>
            </View>
          </View>
        </View>
      </ImageBackground>
      {loading && <Loading />}
    </>
  )
}
export default ForgotByEmail

ForgotByEmail.propTypes = {
  navigation: PropTypes.object
}

RenderInput.propTypes = {
  navigation: PropTypes.object
}
const styles = EStyleSheet.create({
  container: {
    ...EStyleSheet.absoluteFill
  },
  imgBg: {
    flex: 1,
    width: null,
    height: null
  },
  bgOverlay: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5
  },
  contentWrapper: {
    top: 0,
    left: 0,
    alignItems: 'center',
    position: 'absolute',
    marginTop: '15%'
  },
  wrapper: {
    height: height / 1.15,
    width: width,
    paddingHorizontal: '10rem',
    paddingTop: '20rem'
  },
  textHeader: {
    fontFamily: 'Nunito-Regular',
    color: 'white',
    textAlign: 'center',
    fontSize: '14rem'
  },
  fontBold: {
    fontFamily: 'Nunito-Bold'
  },
  formWrapper: {
    paddingHorizontal: '20rem',
    marginTop: '5%'
  },
  styleButton: {
    marginTop: '20rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  },
  modal: {
    borderRadius: '10rem',
    backgroundColor: '#FFFFFF',
    marginHorizontal: '20rem',
    paddingHorizontal: '20rem'
  },
  topImageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    width: 100,
    borderRadius: 100 / 2,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    marginTop: '-44rem'
  },
  imageModal: {
    height: 80,
    width: 80,
    borderRadius: 80 / 2,
    backgroundColor: 'orange',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleModal: {
    fontSize: '16rem',
    fontFamily: 'Nunito-Bold',
    textAlign: 'center'
  },
  descModal: {
    fontFamily: 'Nunito-Regular',
    fontSize: '14rem',
    textAlign: 'center',
    marginTop: '10rem'
  },
  styleButtonModal: {
    marginVertical: '20rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  }
})
