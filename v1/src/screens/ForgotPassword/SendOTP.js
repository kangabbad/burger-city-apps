import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  ImageBackground,
  StatusBar
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Bg from '../../assets/images/background-image.webp'
import Logo from '../../components/Logo'
import TitleScreen from '../../components/TitleScreen'
import InputForm from '../../components/InputForm'
import ActionButton from '../../components/ActionButton'

const { height, width } = Dimensions.get('window')
const SendOTP = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor='transparent' />
      <ImageBackground source={Bg} style={{ ...styles.imgBg, height: height }}>
        <View style={styles.bgOverlay} />
        <View style={{ ...styles.contentWrapper, height: height }}>
          <Logo />
          <View style={styles.wrapper}>
            <TitleScreen
              mainTitle='Forget Password'
              secondTitle='For your security, a one time password  has been sent to
              your email address. Please enter it below to continue.'
            />
            <View style={styles.formWrapper}>
              <InputForm
                iconName='clipboard'
                iconSize={18}
                placeholder='Code OTP'
              />
              <ActionButton
                styleButton={styles.styleButton}
                titleButton='Proceed'
                titleButtonStyle={styles.titleButtonStyle}
                onHandlePress={() => navigation.navigate('reset password')}
              />
            </View>
          </View>
        </View>
      </ImageBackground>
    </View>
  )
}

export default SendOTP

SendOTP.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    ...EStyleSheet.absoluteFill
  },
  imgBg: {
    flex: 1,
    width: null,
    height: null
  },
  bgOverlay: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5
  },
  contentWrapper: {
    top: 0,
    left: 0,
    alignItems: 'center',
    position: 'absolute',
    marginTop: '15%'
  },
  wrapper: {
    height: height / 1.15,
    width: width,
    paddingHorizontal: '10rem',
    paddingTop: '20rem'
  },
  formWrapper: {
    paddingHorizontal: '20rem',
    marginTop: '5%'
  },
  styleButton: {
    marginTop: '20rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  }
})
