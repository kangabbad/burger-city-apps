import React from 'react'
import { View, Text, StatusBar, FlatList } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import PropTypes from 'prop-types'

import TrackIcon from '../../assets/icons/track-icon-white.svg'
import MenuHeader from '../../components/MenuHeader'

const NotificationScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <RenderStatusBar />
      <MenuHeader
        notifScreen
        navigation={navigation}
      />
      <RenderNotifTrackOrder />
      <RenderHistoryNotification />
    </View>
  )
}

const RenderStatusBar = () => {
  return (
    <StatusBar
      translucent
      barStyle='dark-content'
      backgroundColor='#FFFFFF'
    />
  )
}

const RenderNotifTrackOrder = () => {
  return (
    <View style={styles.notifTrackOrder}>
      <View>
        <Text style={styles.titleTrackOrder}>
          Your Order Is On The Way
        </Text>
        <View style={{ flexDirection: 'row' }}>
          <Text style={styles.dateTrackOrder}>
            13 July 2020
          </Text>
          <Text style={styles.timeTrackOrder}>
            10 : 00
          </Text>
        </View>
      </View>

      <View style={styles.wrapperLogo}>
        <TrackIcon
          height={30}
          width={30}
        />

        <Text style={styles.titleLogo}>
          Track Orders
        </Text>
      </View>
    </View>
  )
}

const RenderHistoryNotification = () => {
  const historyNotif = [
    {
      titleNotif: 'New Menu On Burger',
      dateNotif: '10 July 2020'
    },
    {
      titleNotif: 'Buy 1 Get 1',
      dateNotif: '5 July 2020'
    },
    {
      titleNotif: 'Get Free Drinks',
      dateNotif: '1 July 2020'
    }
  ]

  return (
    <FlatList
      data={historyNotif}
      keyExtractor={(item, index) => item + index.toString()}
      renderItem={({ item }) => {
        return (
          <View style={styles.itemHistoryNotif}>
            <Text style={styles.titleNotif}>
              {item.titleNotif}
            </Text>
            <Text style={styles.dateNotif}>
              {item.dateNotif}
            </Text>
          </View>
        )
      }}

    />
  )
}

export default NotificationScreen
NotificationScreen.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEEFF3'
  },
  notifTrackOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#ff9f1c',
    paddingVertical: '20rem',
    paddingHorizontal: '20rem'
  },
  titleTrackOrder: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    color: '#ffffff'
  },
  dateTrackOrder: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: 'rgba(232, 232, 232, 0.75)'
  },
  timeTrackOrder: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: 'rgba(232, 232, 232, 0.75)',
    marginLeft: '10rem'
  },
  wrapperLogo: {
    alignItems: 'center'
  },
  titleLogo: {
    fontFamily: 'Nunito-SemiBold',
    fontSize: '8rem',
    color: '#ffffff',
    marginTop: '0rem'
  },
  itemHistoryNotif: {
    backgroundColor: '#ffffff',
    paddingVertical: '20rem',
    paddingHorizontal: '20rem',
    marginBottom: '5rem'
  },
  titleNotif: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    color: '#141414'
  },
  dateNotif: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: 'rgba(20, 20, 20, 0.75)'
  }
})
