import React, { useState } from 'react'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'
import { showMessage } from 'react-native-flash-message'
import {
  View,
  Dimensions,
  ImageBackground,
  StatusBar,
  Text,
  TouchableOpacity,
  ScrollView
} from 'react-native'

import Bg from '../../assets/images/background-image.webp'

import InputForm from '../../components/InputForm'
import ActionButton from '../../components/ActionButton'
import Logo from '../../components/Logo'
import TitleScreen from '../../components/TitleScreen'
import Loading from '../../components/Loading'

import { useForm } from '../../../util/UseForm'
import Fire from '../../../util/Fire'

const { height, width } = Dimensions.get('window')
const RegisterNumberScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  const [form, setForm] = useForm({
    name: '',
    email: '',
    phoneNumber: '',
    password: ''
  })

  const onContinue = () => {
    setLoading(true)
    Fire.auth()
      .createUserWithEmailAndPassword(form.email, form.password)
      .then(success => {
        setLoading(false)
        setForm('reset')
        const data = {
          name: form.name,
          email: form.email,
          phoneNumber: '',
          uid: success.user.uid
        }
        Fire.database()
          .ref('users/' + success.user.uid + '/')
          .set(data)
        navigation.replace('')
      })
      .catch(error => {
        const errorMessage = error.message
        setLoading(false)
        showMessage({
          message: errorMessage,
          type: 'default',
          backgroundColor: '#c0392b',
          color: '#FFFFFF',
          style: {
            alignItems: 'center'
          }
        })
      })
  }
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <ImageBackground source={Bg} style={{ ...styles.imgBg, height: height }}>
        <StatusBar translucent backgroundColor='transparent' />
        <View style={styles.bgOverlay} />
        <View style={{ ...styles.contentWrapper }}>
          <Logo />
          <View style={styles.wrapper}>
            <TitleScreen
              mainTitle='Sign Up'
              secondTitle='Create Your Burger City Account'
            />
            <View style={styles.form}>
              <InputForm
                iconName='user-circle'
                placeholder='Username'
                iconSize={18}
                value={form.name}
                onChangeText={value => setForm('name', value)}
              />
              <InputForm
                iconName='phone'
                placeholder='Mobile Number'
                iconSize={18}
                value={form.email}
                onChangeText={value => setForm('email', value)}
              />
              <InputForm
                secureTextEntry
                iconName='lock'
                placeholder='Password'
                iconSize={25}
                value={form.password}
                onChangeText={value => setForm('password', value)}
              />
              <InputForm
                secureTextEntry
                iconName='lock'
                placeholder='Confirm Password'
                iconSize={25}
                value={form.password}
                onChangeText={value => setForm('password', value)}
              />
              <View style={styles.link}>
                <Text style={styles.titleLink}>With</Text>
                <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
                  <Text style={styles.titleLinkWhite}> Email</Text>
                </TouchableOpacity>
              </View>
              <ActionButton
                styleButton={styles.styleButton}
                titleButton='NEXT'
                titleButtonStyle={styles.titleButtonStyle}
                onHandlePress={onContinue}
              />
            </View>
          </View>
        </View>
      </ImageBackground>
      {loading && <Loading />}
    </ScrollView>
  )
}

export default RegisterNumberScreen

RegisterNumberScreen.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  imgBg: {
    flex: 1,
    width: '100%',
    resizeMode: 'contain'
  },
  bgOverlay: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5
  },
  contentWrapper: {
    alignItems: 'center',
    position: 'absolute',
    paddingVertical: '30rem'
  },
  wrapper: {
    height: height / 1.15,
    width: width,
    marginTop: '10rem'
  },
  form: {
    paddingHorizontal: '20rem',
    paddingVertical: '20rem'
  },
  wrapperResetPass: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 0
  },
  wrapperRadio: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  link: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15rem'
  },
  titleLink: {
    fontSize: '12rem',
    color: '#FF9F1C',
    fontFamily: 'Nunito-Bold'
  },
  titleLinkWhite: {
    fontSize: '12rem',
    color: '#FFFFFF',
    fontFamily: 'Nunito-Bold',
    textDecorationLine: 'underline'
  },
  styleButton: {
    marginTop: '25rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  }
})
