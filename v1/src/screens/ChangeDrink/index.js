import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, FlatList } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useSelector, useDispatch } from 'react-redux'
import { changePackageOption } from '../../redux/actions/menu'

import MenuHeader from '../../components/MenuHeader'
import IncludesMenu from '../../components/IncludesMenu'

const ChangeDrink = ({ navigation, route }) => {
  const dispatch = useDispatch()
  const { index, category, options } = route.params
  const remakeItemsMenu = useSelector(state => state.menu.remakeItemsMenu)

  const onChangeOption = (item) => {
    const newItems = {
      ...remakeItemsMenu[index],
      package_item: {
        ...remakeItemsMenu[index].package_item,
        [category]: {
          ...remakeItemsMenu[index].package_item[category],
          chosen: item
        }
      }
    }

    const filterItemsMenu = remakeItemsMenu.filter(({ id }) => id !== newItems.id)
    const concatItems = [...filterItemsMenu, newItems]
    const sortItems = concatItems.sort((a, b) => a.id - b.id)

    dispatch(changePackageOption(sortItems))
  }

  return (
    <View>
      <MenuHeader
        icon='bell'
        navigation={navigation}
        rightPress='NotificationScreen'
      />
      <View style={styles.drinkWrapp}>
        <Text style={styles.textInclude}>
          Change Drink's
        </Text>
        <FlatList
          data={options}
          renderItem={({ item, index }) => {
            return (
              <IncludesMenu
                thumbnail={item.img_url}
                title={item.name}
                options={[1, 2]}
                onPressChange={() => {
                  onChangeOption(item)
                  navigation.goBack()
                }}
              />
            )
          }}
          keyExtractor={(item, index) => item.name + index}
        />
      </View>
    </View>
  )
}

ChangeDrink.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object
}

export default ChangeDrink

const styles = EStyleSheet.create({
  drinkWrapp: {
    paddingHorizontal: '24rem'
  },
  textInclude: {
    fontFamily: 'Nunito-Bold',
    fontSize: '18rem',
    marginVertical: '25rem'
  }
})
