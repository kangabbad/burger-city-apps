/* eslint-disable camelcase */
import React from 'react'
import PropTypes from 'prop-types'
import { View, Text, StatusBar, FlatList, Image, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigation } from '@react-navigation/native'
import { Appbar } from 'react-native-paper'
import { removeFavorite } from '../../redux/actions/favorite'

import MenuHeader from '../../components/MenuHeader'
import imgPlaceholder from '../../assets/images/ourBurgerMenu.webp'
import emptyIcon from '../../assets/icons/empty-favorite.webp'

const FavoriteScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <RenderStatusBar />
      <RenderHeader navigation={navigation} />
      <RenderContentFavorite />
    </View>
  )
}

const RenderStatusBar = () => {
  return (
    <StatusBar
      translucent
      barStyle='dark-content'
      backgroundColor='#FFFFFF'
    />
  )
}

const RenderHeader = (props) => {
  const navigation = useNavigation()
  return (
    <MenuHeader
      navigation={navigation}
      icon='bell'
      rightPress='NotificationScreen'
      left='en'
    />
  )
}

const RenderContentFavorite = () => {
  const favorites = useSelector(state => state.favorite)

  const { meals, salads, desserts, beverages, burgers } = favorites
  const data = [...meals, ...salads, ...desserts, ...beverages, ...burgers]

  if (data.length) {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={[0]}
          keyExtractor={(item, index) => item + index.toString()}
          ListHeaderComponent={() => {
            return (
              <Text style={styles.title__text}>
                Favorites
              </Text>
            )
          }}
          renderItem={() => {
            return (
              <View>
                <RenderSubcontentFavorite
                  title='Burgers'
                  data={burgers}
                />
                <RenderSubcontentFavorite
                  title='Meals'
                  data={meals}
                />
                <RenderSubcontentFavorite
                  title='Salads'
                  data={salads}
                />
                <RenderSubcontentFavorite
                  title='Desserts'
                  data={desserts}
                />
                <RenderSubcontentFavorite
                  title='Beverages'
                  data={beverages}
                />
              </View>
            )
          }}
          contentContainerStyle={[
            styles.content__favorite,
            { justifyContent: !data.length ? 'center' : 'flex-start' }
          ]}
        />
      </View>
    )
  } else {
    return (
      <View style={styles.emptyWrapper}>
        <Image
          source={emptyIcon}
          style={styles.emptyIcon}
        />
        <Text style={styles.emptyTitle}>
          Hmm.. You have no favorite. Let's get some eats!
        </Text>
      </View>
    )
  }
}

const RenderSubcontentFavorite = ({ data, title }) => {
  return (
    <FlatList
      listKey={title}
      showsVerticalScrollIndicator={false}
      data={data}
      keyExtractor={(item, index) => title + item + index.toString()}
      ListHeaderComponent={() => {
        if (data.length) {
          return (
            <Text style={styles.subtitle__text}>
              {title}
            </Text>
          )
        }
        return null
      }}
      renderItem={({ item, index }) => {
        return (
          <RenderBurgerItem
            {...item}
            index={index}
            price={`Rp ${item.price},-`}
          />
        )
      }}
    />
  )
}

const RenderBurgerItem = (props) => {
  return (
    <View>
      <RenderBurgerButton index={props.index} {...props} />
      <RenderBurgerBadge status={props.status} />
    </View>
  )
}

const RenderBurgerBadge = (props) => {
  if (props.status.includes('new')) {
    return (
      <View style={styles.badge}>
        <Text style={styles.text__burger__badge}>
          NEW
        </Text>
      </View>
    )
  }
  return null
}

const RenderBurgerButton = ({ index, category, img_url, name, price }) => {
  const dispatch = useDispatch()
  const imgSrc = img_url === '' ? imgPlaceholder : { uri: img_url }
  return (
    <TouchableOpacity
      onPress={() => {}}
    >
      <View style={styles.button}>
        <View style={styles['section--left']}>
          <Image
            source={imgSrc}
            resizeMode='contain'
            style={styles['burger__button--thumbnail']}
          />
        </View>

        <View style={styles['section--middle']}>
          <Text style={styles['burger__button--title']} numberOfLines={3}>
            {name}
          </Text>
          <Text style={styles['burger__button--subTitle']}>
            {price}
          </Text>
        </View>

        <View style={styles['section--right']}>
          <Appbar.Action
            icon='delete'
            onPress={() => {
              dispatch(removeFavorite(index, category))
            }}
            size={EStyleSheet.value('20rem')}
            color='#ff3e3e'
          />
        </View>
      </View>
    </TouchableOpacity>
  )
}

FavoriteScreen.propTypes = {
  navigation: PropTypes.object
}

RenderSubcontentFavorite.propTypes = {
  data: PropTypes.array,
  title: PropTypes.string
}

RenderBurgerItem.propTypes = {
  index: PropTypes.number,
  img_url: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  name: PropTypes.string,
  price: PropTypes.string,
  status: PropTypes.string
}

RenderBurgerButton.propTypes = {
  index: PropTypes.number,
  img_url: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  name: PropTypes.string,
  price: PropTypes.string,
  category: PropTypes.string
}

RenderBurgerBadge.propTypes = {
  status: PropTypes.string
}

export default FavoriteScreen

const styles = EStyleSheet.create({
  container: {
    flex: 1
  },
  title__text: {
    fontFamily: 'Nunito-Bold',
    fontSize: '21rem',
    paddingVertical: '25rem',
    paddingBottom: '10rem'
  },
  subtitle__text: {
    fontFamily: 'Nunito-Regular',
    fontSize: '18rem',
    paddingVertical: '10rem'
  },
  content__favorite: {
    paddingHorizontal: '20rem'
  },
  button: {
    overflow: 'hidden',
    borderRadius: '10rem',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: '#ffffff',
    paddingVertical: '5rem',
    paddingHorizontal: '15rem',
    paddingRight: '5rem',
    marginBottom: '15rem'
  },
  'section--left': {
    overflow: 'hidden'
  },
  'section--middle': {
    flex: 1,
    overflow: 'hidden',
    paddingHorizontal: '10rem',
    paddingLeft: '15rem'
  },
  'section--right': {
    paddingVertical: '5rem',
    paddingHorizontal: '2rem'
  },
  'burger__button--thumbnail': {
    height: '60rem',
    width: '45rem'
  },
  'burger__button--title': {
    fontFamily: 'Nunito-Regular',
    fontSize: '14rem'
  },
  'burger__button--subTitle': {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#6c6c6c',
    marginTop: '5rem'
  },
  badge: {
    borderRadius: '20rem',
    position: 'absolute',
    top: '15rem',
    left: '40rem',
    justifyContent: 'center',
    alignItems: 'center',
    height: '20rem',
    width: '20rem',
    backgroundColor: '#C40304'
  },
  text__burger__badge: {
    fontSize: '6rem',
    color: '#ffffff'
  },
  emptyWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  emptyIcon: {
    height: '150rem',
    width: '180rem'
  },
  emptyTitle: {
    fontSize: '16rem',
    textAlign: 'center',
    width: '200rem',
    marginTop: '30rem'
  }
})
