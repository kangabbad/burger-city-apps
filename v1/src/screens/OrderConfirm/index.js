import React from 'react'
import PropTypes from 'prop-types'
import { Text, View } from 'react-native'
import Dash from 'react-native-dash'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useNavigation } from '@react-navigation/native'
import MenuHeader from '../../components/MenuHeader'
import ActionButton from '../../components/ActionButton'

const OrderConfirm = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <RenderMenuHeader navigation={navigation} />
      <RenderContent />
    </View>
  )
}

const RenderMenuHeader = () => {
  const navigation = useNavigation()
  return (
    <MenuHeader
      navigation={navigation}
      icon='bell'
      rightPress='NotificationScreen'
    />
  )
}

const RenderContent = () => {
  return (
    <View style={styles.content}>
      <RenderOrder />
      <RenderOrderList />
      <RenderButton />
    </View>
  )
}

const RenderOrder = () => {
  return (
    <>
      <Text style={styles.textOrder}>Order Confirmed!</Text>
      <View style={styles.separator}>
        <View style={{ ...styles.circle, left: '-10%', zIndex: 2 }} />
        <Dash
          dashColor='#DEDEDE'
          style={{ flex: 1, height: 1 }}
          dashGap={EStyleSheet.value('3rem')}
        />
        <View style={{ ...styles.circle, right: '-10%' }} />
      </View>
      <View style={styles.customer}>
        <View>
          <Text style={styles.title}>Customer Name</Text>
          <Text style={styles.subTitle}>Darrel Steward</Text>
        </View>
        <View>
          <Text style={{ ...styles.title, textAlign: 'right' }}>Subtotal</Text>
          <Text style={styles.subTitle}>Rp. 52.000</Text>
        </View>
      </View>
    </>
  )
}

const RenderOrderList = () => {
  return (
    <View style={styles.orderList}>
      <Text style={styles.title}>Order List</Text>
      <View style={styles.listItem}>
        <Text style={styles.textNo}>1.</Text>
        <Text style={styles.textName}>Chicken Big Burger</Text>
      </View>
    </View>
  )
}

const RenderButton = () => {
  const navigation = useNavigation()
  return (
    <View style={styles.buttonWrapper}>
      <ActionButton
        styleButton={styles.styleButtonTrack}
        titleButton='TRACK YOUR ORDER'
        titleButtonStyle={styles.titleButtonStyleTrack}
        onHandlePress={() => navigation.navigate('FinishOrderScreen')}
      />
      <ActionButton
        styleButton={styles.styleButtonClose}
        titleButton='CLOSE'
        titleButtonStyle={styles.titleButtonStyleClose}
        onHandlePress={() => navigation.navigate('HomeScreen')}
      />
    </View>
  )
}

export default OrderConfirm

OrderConfirm.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f3f2f6'
  },
  content: {
    backgroundColor: 'white',
    marginTop: '20rem',
    marginHorizontal: '20rem',
    paddingTop: '15rem',
    borderRadius: '8rem',
    flex: 1
  },
  textOrder: {
    textAlign: 'center',
    color: '#FF9F1C',
    fontFamily: 'Nunito-Black',
    fontSize: '18rem',
    marginVertical: '10rem'
  },
  separator: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: '20rem',
    marginBottom: '35rem'
  },
  circle: {
    padding: '25rem',
    backgroundColor: '#f3f2f6',
    borderRadius: '40rem',
    position: 'absolute'
  },
  customer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: '15rem',
    paddingTop: '12rem'
  },
  title: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#3F3F3F'
  },
  subTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    marginTop: '7rem'
  },
  orderList: {
    paddingHorizontal: '15rem',
    marginTop: '30rem',
    width: '100%'
  },
  listItem: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: '10rem',
    marginTop: '5rem'
  },
  textNo: {
    color: '#989898',
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    marginRight: '5rem'
  },
  textName: {
    fontFamily: 'Nunito-Bold',
    color: '#1D2126',
    fontSize: '14rem'
  },
  titleOrder: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    marginVertical: '5rem',
    marginTop: '10rem'
  },
  buttonWrapper: {
    paddingHorizontal: '15rem',
    position: 'absolute',
    bottom: '15rem',
    width: '100%'
  },
  styleButtonTrack: {
    paddingVertical: '15rem',
    backgroundColor: '#ff9f1c',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyleTrack: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: 'white'
  },
  styleButtonClose: {
    marginTop: '10rem',
    paddingHorizontal: '85rem',
    paddingVertical: '15rem',
    backgroundColor: '#ffffff',
    borderRadius: '8rem',
    borderWidth: '1rem',
    borderColor: '#EFEFEF',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyleClose: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ff9f1c'
  }
})
