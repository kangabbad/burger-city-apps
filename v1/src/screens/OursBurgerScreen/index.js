import React, { useEffect, useState } from 'react'
import { View, StatusBar, Dimensions, FlatList, Text } from 'react-native'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useSelector } from 'react-redux'
import { useNavigation } from '@react-navigation/native'

import Fire from '../../../util/Fire'
import { storeData } from '../../../util/LocalStorage'
import SearchBar from '../../components/SearchBar'
import ImageSlider from '../../components/ImageSlider'
import MenuCategory from '../../components/MenuCategory'
import MenuHeader from '../../components/MenuHeader'
import Loading from '../../components/Loading'
import ActionButton from '../../components/ActionButton'

const OurBurgerScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  const [banner, setBanner] = useState([])
  const [offers, setOffers] = useState([])

  useEffect(() => {
    const FetchApi = navigation.addListener('focus', () => {
      setLoading(true)
      Fire.database()
        .ref('menu/')
        .once('value')
        .then((response) => {
          const data = response.val()
          const banner = data.banners
          setBanner(banner.filter(data => data.status === 'billboard'))

          let newDataOffers = {}
          let menu = ''
          const newOffers = []

          data.offers.map((item, index) => {
            if (index === 0) {
              menu = 'meals'
            } else if (index === 1) {
              menu = 'salads'
            } else if (index === 2) {
              menu = 'desserts'
            } else if (index === 3) {
              menu = 'beverages'
            }

            newDataOffers = {
              ...item,
              menu
            }

            newOffers.push(newDataOffers)
          })

          setOffers(newOffers)
          setLoading(false)
        })
        .catch((error) => error)
    })
    return FetchApi
  }, [navigation])

  return (
    <>
      <View style={styles.container}>
        <StatusBar translucent backgroundColor='transparent' />
        <MenuHeader
          navigation={navigation}
          icon='bell'
          left='en'
          rightPress='NotificationScreen'
        />
        <FlatList
          contentContainerStyle={{ paddingBottom: EStyleSheet.value('20rem') }}
          showsVerticalScrollIndicator={false}
          data={[0, 1, 2, 3]}
          renderItem={({ item, index }) => {
            switch (index) {
              case 0:
                return (
                  <SearchBar />
                )
              case 1:
                return (
                  <ImageSlider data={banner} />
                )
              case 2:
                return (
                  <View style={{ alignItems: 'center' }}>
                    <MenuCategory
                      navigation={navigation}
                      offers={offers}
                    />
                  </View>
                )
              case 3:
                return (
                  <ActionButton
                    styleButton={styles.styleButton}
                    titleButton='Burger Menu'
                    titleButtonStyle={styles.titleButtonStyle}
                    onHandlePress={() => {
                      storeData('isLoadApi', true)
                      navigation.navigate('MenuPacket', {
                        menu: 'burgers',
                        name: 'Burger Menu'
                      })
                    }}
                  />
                )
              default:
                break
            }
          }}
          keyExtractor={index => index.toString()}
        />

        <ViewCartButton />
      </View>
      {loading && <Loading type='splash' />}
    </>
  )
}

const ViewCartButton = () => {
  const navigation = useNavigation()
  const { meals, salads, desserts, beverages, burgers } = useSelector(state => state.cart)
  let totalItem = 0
  let totalPrice = 0

  if (meals.length) {
    meals.map((item) => {
      const singlePrice = item.count * item.price
      totalItem += item.count
      totalPrice += singlePrice
    })
  }
  if (salads.length) {
    salads.map((item) => {
      const singlePrice = item.count * item.price
      totalItem += item.count
      totalPrice += singlePrice
    })
  }
  if (desserts.length) {
    desserts.map((item) => {
      const singlePrice = item.count * item.price
      totalItem += item.count
      totalPrice += singlePrice
    })
  }
  if (beverages.length) {
    beverages.map((item) => {
      const singlePrice = item.count * item.price
      totalItem += item.count
      totalPrice += singlePrice
    })
  }
  if (burgers.length) {
    burgers.map((item) => {
      const singlePrice = item.count * item.price
      totalItem += item.count
      totalPrice += singlePrice
    })
  }

  if (totalItem > 0) {
    return (
      <View style={styles.wrapperCardBottom}>
        <ActionButton
          styleButton={styles.styleButtonBottom}
          wrapperBadge={styles.wrapperBadge}
          itemLeft={totalItem.toString()}
          itemLeftStyle={styles.itemLeftStyle}
          titleButton='View your cart'
          titleButtonStyle={styles.titleButtonStyleBottom}
          itemRight={<Text style={styles.textRight}>Rp {totalPrice}</Text>}
          itemRightStyle={styles.itemRightStyle}
          onHandlePress={() => navigation.navigate('CartScreen')}
        />
      </View>
    )
  }

  return null
}

export default OurBurgerScreen
const { height } = Dimensions.get('window')

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F7'
  },
  header: {
    backgroundColor: 'white',
    paddingHorizontal: '10rem',
    height: height * 10 / 100,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: '6rem'
    },
    shadowOpacity: 0.1,
    elevation: 15,
    marginTop: '25rem'
  },
  styleButton: {
    marginTop: '15rem',
    marginHorizontal: '20rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '16rem',
    color: '#ffffff'
  },
  wrapperCardBottom: {
    // position: 'absolute',
    // bottom: 0,
    // right: 0,
    // left: 0,
    backgroundColor: 'white',
    height: '80rem'
  },
  styleButtonBottom: {
    marginHorizontal: '14rem',
    marginTop: '10rem',
    paddingVertical: '16rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  wrapperBadge: {
    borderRadius: '4rem',
    justifyContent: 'center',
    alignItems: 'center',
    height: '20rem',
    width: '20rem',
    backgroundColor: '#ffffff',
    marginLeft: '-10rem'
  },
  itemLeftStyle: {
    fontFamily: 'Nunito-Regular',
    color: '#ff9f1c'
  },
  titleButtonStyleBottom: {
    fontFamily: 'Nunito-Regular',
    fontSize: '14rem',
    color: '#ffffff'
  },
  textRight: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  },
  itemRightStyle: {
    marginRight: '-10rem'
  }
})

OurBurgerScreen.propTypes = {
  navigation: PropTypes.object
}
