import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'
import { Text, View, TouchableOpacity, Image, ImageBackground, FlatList, ScrollView } from 'react-native'
import StarRating from 'react-native-star-rating'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { useSelector } from 'react-redux'

import Icon from '../../assets/icons/logo.webp'
import banner from '../../assets/images/banner.png'

import Fire from '../../../util/Fire'
import MenuHeader from '../../components/MenuHeader'

const Homescreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <MenuHeader
        navigation={navigation}
        icon='bell'
        rightPress='NotificationScreen'
        left='en'
      />
      <ScrollView showsVerticalScrollIndicator={false}>
        <RenderBanner />
        <RenderOrder />
        <RenderBestOffer navigation={navigation} />
      </ScrollView>
      <RenderFloat navigation={navigation} />
    </View>
  )
}

const RenderBanner = () => {
  return (
    <ImageBackground
      source={banner}
      resizeMode='contain'
      style={styles.banner}
    >
      <View style={styles.overlayBanner} />
      <View style={styles.wrapperTitleBanner}>
        <Text style={styles.titleBanner}>
          World Greatest
        </Text>
        <Text style={styles.titleBannerBottom}>
          Burgers.
        </Text>
      </View>
    </ImageBackground>
  )
}

const RenderOrder = () => {
  return (
    <View style={styles.order}>
      <TouchableOpacity style={styles.orderTicket}>
        <View style={styles.circleTicketLeft} />
        <View style={[styles.contentTicket, { marginRight: 15 }]}>
          <Image source={Icon} style={styles.iconTicket} />
          <View style={styles.wrapperTitleTicket}>
            <Text style={styles.titleTicket}>
              Track Here
            </Text>
            <Text style={styles.descTicket}>
              Order to Track Your Food
            </Text>
          </View>
        </View>
        <View style={styles.circleTicketRight} />
      </TouchableOpacity>
      <TouchableOpacity style={styles.orderTicket}>
        <View style={styles.circleTicketLeft} />
        <View style={styles.contentTicket}>
          <Image source={Icon} style={styles.iconTicket} />
          <View style={styles.wrapperTitleTicket}>
            <Text style={styles.titleTicket}>Order Here</Text>
            <Text style={styles.descTicket}>Choice Your Delicious Burger</Text>
          </View>
        </View>
        <View style={styles.circleTicketRight} />
      </TouchableOpacity>
    </View>
  )
}

const RenderBestOffer = ({ navigation }) => {
  const [bestOffers, setBestOffers] = useState([])

  const getBestOffers = () => {
    Fire.database()
      .ref('menu/category/burgers/items')
      .once('value')
      .then(res => {
        if (res.val()) {
          const data = res.val()
          const filterData = data.filter(el => el !== null)
          setBestOffers(filterData)
        }
      })
      .catch(err => {
        console.log(err)
      })
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      getBestOffers()
    })
    return unsubscribe
  }, [navigation])

  return (
    <View style={styles.bestOffer}>
      <Text style={styles.bestOfferTitle}>
        Best Offers
      </Text>

      <FlatList
        data={bestOffers}
        keyExtractor={(item, index) => item + index.toString()}
        horizontal
        showsHorizontalScrollIndicator={false}
        renderItem={(item => {
          const { thumbnail, name, rating, price, status } = item.item

          if (status === 'bestoffer') {
            return (
              <TouchableOpacity
                style={styles.bestOfferItem}
                onPress={() => navigation.navigate('MenuPacket', { menu: 'burgers', name: 'Burger Menu' })}
              >
                <View style={styles.wrapperBestOfferImage}>
                  <Image
                    source={{ uri: thumbnail }}
                    resizeMode='cover'
                    style={styles.bestOfferImage}
                  />
                </View>

                <View style={styles.bestOfferInfo}>
                  <Text style={styles.bestOfferName}>
                    {name}
                  </Text>
                </View>
                <StarRating
                  disabled
                  maxStars={5}
                  rating={rating}
                  starSize={14}
                  fullStarColor='#ff9f1c'
                  emptyStar='star'
                  emptyStarColor='#727C8E'
                  containerStyle={styles.bestOfferRate}
                />
                <View style={styles.wrapperBestOfferPrice}>
                  <Text style={styles.bestOfferPrice}>
                    {`Rp. ${price} ,-`}
                  </Text>
                </View>
              </TouchableOpacity>
            )
          }
        }
        )}
        contentContainerStyle={styles.bestOfferList}
      />
    </View>
  )
}

const RenderFloat = ({ navigation }) => {
  const cart = useSelector(state => state.cart)
  const { meals, salads, desserts, beverages, burgers } = cart
  const data = [...meals, ...salads, ...desserts, ...beverages, ...burgers]
  if (data.length) {
    return (
      <TouchableOpacity
        style={styles.floatAction}
        onPress={() => navigation.navigate('CartScreen')}
      >
        <Ionicons
          name='cart-outline'
          size={EStyleSheet.value('25rem')}
          color='#FF9F1C'
        />
        <View style={styles.notif}>
          <Text style={styles.titleNotif}>
            {data.length}
          </Text>
        </View>
      </TouchableOpacity>
    )
  }

  return null
}

export default Homescreen

Homescreen.propTypes = {
  navigation: PropTypes.object
}

RenderFloat.propTypes = {
  navigation: PropTypes.object
}

RenderBestOffer.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f3f2f6'
  },
  header: {
    marginTop: '-3rem'
  },
  banner: {
    width: '100%',
    height: '205rem'
  },
  overlayBanner: {
    top: 0,
    position: 'absolute',
    right: 0,
    bottom: 0,
    left: 0,
    backgroundColor: 'rgba(0, 0, 0, 0.2)'
  },
  wrapperTitleBanner: {
    paddingVertical: '30rem',
    paddingHorizontal: '17rem'
  },
  titleBanner: {
    fontSize: '18rem',
    fontFamily: 'Nunito-Regular',
    color: '#FFFFFF'
  },
  titleBannerBottom: {
    fontSize: '18rem',
    fontFamily: 'Nunito-Black',
    color: '#FFFFFF'
  },
  order: {
    paddingHorizontal: '20rem'
  },
  circleTicketLeft: {
    right: '17rem',
    height: '40rem',
    width: '40rem',
    borderRadius: '40rem',
    backgroundColor: '#f3f2f6'
  },
  circleTicketRight: {
    left: '17rem',
    height: '40rem',
    width: '40rem',
    borderRadius: '40rem',
    backgroundColor: '#f3f2f6'
  },
  orderTicket: {
    paddingVertical: '17rem',
    marginTop: '17rem',
    backgroundColor: '#1D2126',
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    borderRadius: '5rem'
  },
  contentTicket: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    flexDirection: 'row'
  },
  iconTicket: {
    height: '35rem',
    resizeMode: 'contain',
    width: '35rem',
    marginRight: '20rem'
  },
  titleTicket: {
    fontSize: '14rem',
    color: '#FFFFFF',
    fontFamily: 'Nunito-Bold'
  },
  descTicket: {
    fontSize: '10rem',
    color: '#FFFFFF',
    fontFamily: 'Nunito-Regular',
    marginTop: '5rem'
  },
  bestOffer: {
    marginTop: '17rem',
    marginBottom: '21rem'
  },
  bestOfferItem: {
    // backgroundColor: '#FFFFFF',
    marginHorizontal: '6rem'
    // paddingHorizontal: '10rem',
    // paddingVertical: '20rem',
  },
  bestOfferTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem',
    color: '#1D2126',
    includeFontPadding: false,
    paddingHorizontal: '16rem'
  },
  bestOfferList: {
    paddingHorizontal: '10rem',
    marginTop: '12rem'
  },
  bestOfferImage: {
    width: '130rem',
    height: '180rem',
    borderRadius: '8rem'
  },
  bestOfferInfo: {
    marginTop: '15rem',
    marginHorizontal: '0rem'
  },
  bestOfferName: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#1D2126',
    includeFontPadding: false
  },
  wrapperBestOfferPrice: {
    marginTop: '10rem',
    marginHorizontal: '8rem'
  },
  bestOfferPrice: {
    fontFamily: 'Nunito-Bold',
    fontSize: '12rem',
    color: '#1D2126',
    marginLeft: '-7rem',
    includeFontPadding: false
  },
  bestOfferRate: {
    marginTop: '5rem',
    paddingRight: '40rem'
  },
  floatAction: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    width: '45rem',
    height: '45rem',
    elevation: 4,
    borderRadius: '8rem',
    position: 'absolute',
    bottom: '25rem',
    right: '15rem',
    marginHorizontal: '0rem'
  },
  notif: {
    borderRadius: '20rem',
    position: 'absolute',
    elevation: '4rem',
    top: '-5rem',
    right: '-5rem',
    height: '20rem',
    width: '20rem',
    backgroundColor: '#ff4747',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleNotif: {
    fontFamily: 'Nunito-Bold',
    fontSize: '10rem',
    color: '#FFFFFF',
    includeFontPadding: false
  }
})
