import React from 'react'
import { StyleSheet, Text, View, TextInput, FlatList } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'

const data = [
  {
    title: 'Provinsi',
    placeholder: 'Provinsi'
  },
  {
    title: 'Kota/Kabupaten',
    placeholder: 'Kota/Kabupaten'
  },
  {
    title: 'Kecamatan',
    placeholder: 'Kecamatan'
  },
  {
    title: 'Kode Pos',
    placeholder: 'Kode Pos'
  }
]

const ChangeAddress = () => {
  return (
    <FlatList
      data={[0]}
      showsVerticalScrollIndicator={false}
      keyExtractor={(item, index) => item + index.toString()}
      renderItem={() => (
        <View style={styles.container}>
          <View style={styles.wraper}>
            <FlatList
              data={data}
              listKey='input-address'
              keyExtractor={(item, index) => item + index.toString()}
              renderItem={({ item }) => (
                <View style={styles.inputWrap}>
                  <Text style={styles.inputTitle}>
                    {item.title}
                  </Text>
                  <TextInput
                    placeholder={item.placeholder}
                    style={styles.inputBox}
                  />
                </View>
              )}
            />

            <View style={styles.fullAddress}>
              <Text style={styles.inputTitle}>
                Alamat Lengkap
              </Text>
              <Text style={styles.inputSubtitle}>
                Nama gedung, jalan, dan lainnya...
              </Text>
              <TextInput
                style={styles.inputBoxFullAddress}
                placeholder='Atur Alamat Lengkap'
                multiline
                numberOfLines={3}
              />
            </View>

            <View style={styles.mapWrapper}>
              <MapView
                provider={PROVIDER_GOOGLE}
                style={styles.map}
                region={{
                  latitude: -7.9311342,
                  longitude: 110.4314679,
                  latitudeDelta: 0.015,
                  longitudeDelta: 0.0121
                }}
              >
                <Marker
                  draggable
                  coordinate={{
                    latitude: -7.9311342,
                    longitude: 110.4314679
                  }}
                />
              </MapView>
            </View>
          </View>
        </View>
      )}
    />
  )
}

export default ChangeAddress

const styles = EStyleSheet.create({
  container: {
    flex: 1
  },
  wraper: {
    backgroundColor: '#FFFFFF'
  },
  inputWrap: {
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '16rem',
    paddingVertical: '5rem'
  },
  fullAddress: {
    padding: '13rem'
  },
  inputTitle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem'
  },
  inputBox: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    textAlign: 'right',
    width: '75%'
  },
  inputBoxFullAddress: {
    padding: 0,
    width: '100%',
    textAlign: 'left'
  },
  inputSubtitle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem'
  },
  mapWrapper: {
    justifyContent: 'flex-end',
    alignItems: 'center',
    width: '100%',
    height: '280rem'
  },
  map: {
    ...StyleSheet.absoluteFillObject,
    width: '100%',
    height: '280rem'
  }
})
