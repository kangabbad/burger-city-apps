import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Text, View, Image, StatusBar, FlatList } from 'react-native'
import { Appbar } from 'react-native-paper'
import EStyleSheet from 'react-native-extended-stylesheet'
import { useSelector, useDispatch } from 'react-redux'
import { showMessage } from 'react-native-flash-message'
import { setCountItemsMenu, clearMenu } from '../../redux/actions/menu'
import { addToCart } from '../../redux/actions/cart'

import IncludesMenu from '../../components/IncludesMenu'
import MenuHeader from '../../components/MenuHeader'
import ActionButton from '../../components/ActionButton'

const ChangePacket = ({ navigation, route }) => {
  const dispatch = useDispatch()
  const { index, menuName } = route.params
  const remakeItemsMenu = useSelector(state => state.menu.remakeItemsMenu)
  const [color, setColor] = useState(false)
  // eslint-disable-next-line camelcase
  const { count, package_item } = remakeItemsMenu[index]

  return (
    <View style={styles.container}>
      <StatusBar translucent backgroundColor='transparent' />
      <MenuHeader
        icon='bell'
        navigation={navigation}
        rightPress='NotificationScreen'
        onBackCallback={() => {
          navigation.navigate('MenuPacket', { prevScreen: 'ChangePacket' })
        }}
      />
      <FlatList
        showsVerticalScrollIndicator={false}
        data={[0]}
        renderItem={() => (
          <View style={styles.content}>
            <View style={styles.title}>
              <View>
                <Text style={styles.textTitle}>
                  {route.params.name}
                </Text>
                <Text style={styles.subText}>
                  Please customize your packet
                </Text>
              </View>
              <Appbar.Action
                icon='star'
                size={EStyleSheet.value('21rem')}
                color={color !== true ? 'gray' : '#FF9F1C'}
                onPress={() => setColor(!color)}
              />
            </View>
            <View style={styles.imageWrap}>
              <Image source={{ uri: route.params.image }} style={styles.image} />
              <View style={styles.plusMinus}>
                <View style={styles.left}>
                  <View style={styles.plusMinusWrap}>
                    <Appbar.Action
                      icon='minus'
                      size={14}
                      style={styles.plusMinusButton}
                      onPress={() => dispatch(setCountItemsMenu(index, '-'))}
                    />
                    <Text style={styles.counterTitle}>
                      {count}
                    </Text>
                    <Appbar.Action
                      icon='plus'
                      size={14}
                      style={styles.plusMinusButton}
                      onPress={() => dispatch(setCountItemsMenu(index, '+'))}
                    />
                  </View>
                </View>
                <View style={styles.right}>
                  <ActionButton
                    styleButton={styles.styleButton}
                    titleButton='Add to Cart'
                    titleButtonStyle={styles.titleButtonStyle}
                    onHandlePress={() => {
                      if (count > 0) {
                        dispatch(addToCart(menuName))
                        dispatch(clearMenu())
                        navigation.navigate('OurBurgerScreen')
                      } else {
                        showMessage({
                          animationDuration: 150,
                          duration: 2000,
                          message: 'Oops!',
                          description: 'Looks like your not addinglooks like you have not added to the amount yet',
                          type: 'none',
                          position: 'bottom',
                          backgroundColor: '#FF9F1C',
                          color: '#FFFFFF',
                          titleStyle: {
                            fontSize: EStyleSheet.value('16rem')
                          }
                        })
                      }
                    }}
                  />
                </View>
              </View>
            </View>
            <View style={styles.includes}>
              <Text style={styles.textInclude}>Includes</Text>
              <View>
                <IncludesMenu
                  thumbnail={package_item.main_course.chosen.img_url}
                  title={package_item.main_course.chosen.name}
                  options={package_item.main_course.option}
                  onPressChange={() => {
                    navigation.navigate('ChangeDrink', {
                      index,
                      category: 'main_course',
                      options: package_item.main_course.option
                    })
                  }}
                />
                <IncludesMenu
                  thumbnail={package_item.beverage.chosen.img_url}
                  title={package_item.beverage.chosen.name}
                  options={package_item.beverage.option}
                  onPressChange={() => {
                    navigation.navigate('ChangeDrink', {
                      index,
                      category: 'beverage',
                      options: package_item.beverage.option
                    })
                  }}
                />
                <IncludesMenu
                  thumbnail={package_item.side_dish.chosen.img_url}
                  title={package_item.side_dish.chosen.name}
                  options={package_item.side_dish.option}
                  onPressChange={() => {
                    navigation.navigate('ChangeDrink', {
                      index,
                      category: 'side_dish',
                      options: package_item.side_dish.option
                    })
                  }}
                />
              </View>
            </View>
          </View>
        )}
        keyExtractor={index => index.toString()}
      />
    </View>
  )
}

export default ChangePacket

ChangePacket.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1
  },
  content: {
    marginTop: '20rem',
    paddingHorizontal: '20rem'
  },
  title: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  textTitle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '16rem'
  },
  subText: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem'
  },
  imageWrap: {
    paddingTop: '10rem'
  },
  image: {
    alignSelf: 'center',
    width: '163rem',
    height: '118rem',
    resizeMode: 'contain'
  },
  plusMinus: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '26rem',
    alignItems: 'center'
  },
  left: {
    flex: 1,
    flexGrow: 1,
    width: '40%',
    height: '38rem',
    paddingRight: '4rem'
  },
  right: {
    // flex: 1,
    // flexGrow: 2,
    width: '60%',
    paddingLeft: '4rem'
  },
  plusMinusWrap: {
    flexDirection: 'row',
    backgroundColor: 'white',
    borderRadius: '4rem',
    alignItems: 'center',
    justifyContent: 'space-around',
    flex: 1
  },
  plusMinusButton: { backgroundColor: '#E3E5E8' },
  button: {
    paddingVertical: '8rem',
    backgroundColor: '#FF9F1C',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1
  },
  styleButton: {
    // paddingVertical: '14rem',
    backgroundColor: '#ff9f1c',
    borderRadius: '4rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    flex: 1,
    alignItems: 'center'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Bold',
    fontSize: '12rem',
    color: 'white'
  },
  includes: {
    marginTop: '28rem',
    marginBottom: '25rem'
  },
  textInclude: {
    fontFamily: 'Nunito-Regular',
    fontSize: '18rem',
    marginBottom: '22rem'
  },
  counterTitle: {
    fontSize: '12rem',
    paddingVertical: '6rem'
  }
})
