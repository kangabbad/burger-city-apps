import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, FlatList } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import MenuHeader from '../../components/MenuHeader'
import CardWallet from '../../components/CardWallet'
import OrderDeliveryOrder from '../../components/OrderDeliveryOrder'
import ActionButton from '../../components/ActionButton'

const PaymentMethod = ({ navigation }) => {
  return (
    <View style={{ flex: 1 }}>
      <MenuHeader
        navigation={navigation}
        icon='bell'
        rightPress='NotificationScreen'
      />
      <FlatList
        data={[0]}
        keyExtractor={(item, index) => item + index.toString()}
        contentContainerStyle={styles.wrapperContent}
        renderItem={() => {
          return (
            <View>
              <Text style={styles.titleSelectWallet}>
                Sellect Wallet
              </Text>

              <CardWallet
                leftItemWallet='paymentMethod'
              />

              <OrderDeliveryOrder />

              <View style={styles.wrapperButton}>
                <ActionButton
                  styleButton={styles.styleButton}
                  titleButton='ORDER'
                  titleButtonStyle={styles.titleButtonStyle}
                  onHandlePress={() => navigation.navigate('OrderConfirm')}
                />
              </View>
            </View>
          )
        }}
      />
    </View>
  )
}

export default PaymentMethod

PaymentMethod.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  wrapperContent: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    marginTop: '10rem'
  },
  titleSelectWallet: {
    marginTop: '15rem',
    marginBottom: '5rem',
    marginHorizontal: '20rem',
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem'
  },
  wrapperButton: {
    paddingHorizontal: '20rem',
    marginTop: '40rem'
  },
  styleButton: {
    paddingVertical: '16rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '16rem',
    color: '#FFFFFF',
    includeFontPadding: false
  }
})
