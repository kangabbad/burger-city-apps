import React, { useEffect } from 'react'
import { Text, View, TouchableOpacity, StatusBar, BackHandler } from 'react-native'
import PropTypes from 'prop-types'
import EStyleSheet from 'react-native-extended-stylesheet'
import { WebView } from 'react-native-webview'

import Feather from 'react-native-vector-icons/Feather'

const Webview = ({ navigation }) => {
  useEffect(() => {
    const backAction = () => {
      navigation.navigate('Login')
      return true
    }

    const backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      backAction
    )

    return () => backHandler.remove()
  }, [])
  return (
    <>
      <RenderStatusBar />
      <RenderHeader navigation={navigation} />
      <RenderWebView />
    </>
  )
}

const RenderStatusBar = () => {
  return (
    <StatusBar
      translucent
      barStyle='dark-content'
      backgroundColor='#FFFFFF'
    />
  )
}

const RenderHeader = ({ navigation }) => {
  return (
    <View style={styles.header}>
      <View>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Feather
            name='chevron-left'
            color='#ff9f1c'
            size={20}
            style={{ marginRight: 5 }}
          />
        </TouchableOpacity>
        <Text style={styles.text}>Back to Login</Text>
      </View>
    </View>
  )
}

const RenderWebView = () => {
  return (
    <WebView source={{ uri: 'https://mail.google.com/mail/u/1/#inbox' }} />
  )
}

Webview.propTypes = {
  navigation: PropTypes.object
}

RenderHeader.propTypes = {
  navigation: PropTypes.object
}

export default Webview

const styles = EStyleSheet.create({
  header: {
    flexDirection: 'row',
    paddingHorizontal: '8rem',
    paddingVertical: '10rem',
    marginTop: '18rem',
    backgroundColor: '#FFFFFF',
    alignItems: 'center'
  },
  text: {
    fontFamily: 'Nunito-Bold',
    color: '#ff9f1c'
  }
})
