import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  ImageBackground,
  Text,
  StatusBar
} from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Carousel, { Pagination } from 'react-native-snap-carousel'
import { useDispatch } from 'react-redux'
import { getData } from '../../../util/LocalStorage'
import { restoreFavorite } from '../../redux/actions/favorite'

import Loading from '../../components/Loading'
import ActionButton from '../../components/ActionButton'
import Logo from '../../components/Logo'
import Bg from '../../assets/images/background-image.webp'

const { width } = Dimensions.get('window')

const FirstScreen = ({ navigation }) => {
  const dispatch = useDispatch()
  const [isLoading, setLoading] = useState(true)

  const restoreUser = async () => {
    const getDataUser = await getData('user')
    if (getDataUser !== undefined) {
      return JSON.parse(getDataUser)
    }
    return null
  }

  const restoreFavoritesLS = async () => {
    const favorites = await getData('favorites')
    if (favorites !== undefined) {
      const dataFavorites = JSON.parse(favorites)
      dispatch(restoreFavorite(dataFavorites))
    }
  }

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', async () => {
      try {
        const resUser = await restoreUser()
        await restoreFavoritesLS()
        setLoading(false)
        if (resUser !== null) {
          if (resUser.isLogin) navigation.replace('InApp')
        }
      } catch (error) {
        console.log('dari FirstScreen didMount()')
      }
    })
    return unsubscribe
  }, [navigation])

  return (
    <>
      <View style={styles.container}>
        <StatusBar translucent backgroundColor='transparent' />
        <ImageBackground source={Bg} style={styles.imgBg}>
          <RenderOverlay />
          <View style={styles.contentWrapper}>
            <RenderLogo />
            <View style={styles.wrapper}>
              <RenderCarousel />
              <RenderButton navigation={navigation} />
            </View>
          </View>
        </ImageBackground>
      </View>
      {isLoading && <Loading type='splash' />}
    </>
  )
}

const RenderOverlay = () => {
  return (
    <View style={styles.bgOverlay} />
  )
}

const RenderCarousel = () => {
  const [hero] = useState([
    "World's\nGreatest\nBurgers",
    "World's\nDelicious\nBurgers",
    "World's\nPopular\nBurgers"
  ])

  const [activeSlide, setActiveSlide] = useState(0)
  const { width } = Dimensions.get('window')
  return (
    <View style={styles.carousel}>
      <Carousel
        data={hero}
        decelerationRate='fast'
        sliderWidth={width}
        itemWidth={width}
        renderItem={item => {
          return (
            <View style={styles.heroWrapper}>
              <Text style={styles.titleHero}>
                {item.item}
              </Text>
            </View>
          )
        }}
        onSnapToItem={index => setActiveSlide(index)}
      />

      <Pagination
        dotsLength={hero.length}
        activeDotIndex={activeSlide}
        dotStyle={styles.paginationDot}
        dotContainerStyle={styles.paginationDotContainer}
        inactiveDotOpacity={0.5}
        inactiveDotScale={1}
        containerStyle={styles.paginationContainer}
      />
    </View>
  )
}

const RenderLogo = () => {
  return (
    <Logo />
  )
}

const RenderButton = ({ navigation }) => {
  return (
    <View style={styles.button}>
      <ActionButton
        styleButton={styles.styleButton}
        titleButton='Get start here'
        titleButtonStyle={styles.titleButtonStyle}
        onHandlePress={() => navigation.replace('LoginScreen')}
      />
    </View>
  )
}

FirstScreen.propTypes = {
  navigation: PropTypes.object
}

RenderButton.propTypes = {
  navigation: PropTypes.object
}

export default FirstScreen

const styles = EStyleSheet.create({
  container: {
    ...EStyleSheet.absoluteFill
  },
  imgBg: {
    borderWidth: 1,
    borderColor: 'green',
    flex: 1
  },
  bgOverlay: {
    ...EStyleSheet.absoluteFill,
    backgroundColor: '#000',
    opacity: 0.5
  },
  carousel: {
    flex: 1
  },
  contentWrapper: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: '15%'
  },
  wrapper: {
    ...EStyleSheet.absoluteFill,
    width: width,
    justifyContent: 'flex-end',
    paddingHorizontal: '15rem',
    paddingBottom: '15%'
  },
  heroWrapper: {
    paddingHorizontal: '20rem',
    marginTop: 'auto'
  },
  titleHero: {
    fontSize: '26rem',
    color: '#ffffff',
    fontFamily: 'Nunito-Bold'
  },
  paginationDotContainer: {
    marginHorizontal: '5rem'
  },
  paginationDot: {
    width: '7rem',
    height: '7rem',
    borderRadius: '7rem',
    backgroundColor: '#FFFFFF'
  },
  paginationContainer: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  button: {
    paddingHorizontal: '15rem',
    marginTop: '15rem'
  },
  styleButton: {
    paddingVertical: '16rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '10rem',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff',
    includeFontPadding: false
  }
})
