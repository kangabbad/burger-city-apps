import React from 'react'
import PropTypes from 'prop-types'
import { Text, View, FlatList, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import MenuHeader from '../../components/MenuHeader'
import OrderMethod from '../../components/OrderMethod'
import MapBox from '../../components/MapBox'
import CostDetails from '../../components/CostDetails'

import IconRight from '../../assets/icons/icon-right.svg'
import ActionButton from '../../components/ActionButton'

const CartScreen = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <MenuHeader
        navigation={navigation}
        icon='bell'
        rightPress='NotificationScreen'
      />
      <FlatList
        data={[0, 1, 2, 3]}
        showsVerticalScrollIndicator={false}
        keyExtractor={index => index.toString()}
        renderItem={({ item, index }) => {
          switch (index) {
            case 0:
              return (
                <>
                  <Text style={styles.title}>
                    Order Method
                  </Text>
                  <View style={styles.wrapper}>
                    <OrderMethod />
                  </View>
                </>
              )
            case 1:
              return (
                <>
                  <Text style={styles.title}>Delivery Address</Text>
                  <MapBox navigation={navigation} />
                </>
              )
            case 2:
              return (
                <>
                  <View style={styles.itemList}>
                    <Text style={styles.text}>
                      Item List
                    </Text>
                    <TouchableOpacity onPress={() => navigation.navigate('OurBurgerScreen')}>
                      <Text style={{ ...styles.text, ...styles.fontItem }}>
                        Add Items
                      </Text>
                    </TouchableOpacity>
                  </View>
                  <CostDetails />
                </>
              )
            case 3:
              return (
                <TouchableOpacity
                  style={styles.wrapperButton}
                  onPress={() => navigation.navigate('PaymentMethod')}
                >
                  <ActionButton
                    styleButton={styles.styleButton}
                    itemLeft='Payment Method'
                    itemLeftStyle={styles.itemLeftStyle}
                    itemRight={
                      <IconRight
                        height={EStyleSheet.value('24rem')}
                        width={EStyleSheet.value('24rem')}
                      />
                    }
                    onHandlePress={() => navigation.navigate('PaymentMethod')}
                  />
                </TouchableOpacity>
              )
            default:
              break
          }
        }}
      />
    </View>
  )
}

export default CartScreen

CartScreen.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1
  },
  wrapper: {
    paddingHorizontal: '20rem',
    paddingVertical: '5rem',
    backgroundColor: '#FFFFFF',
    marginTop: '16rem'
  },
  title: {
    marginTop: '16rem',
    paddingHorizontal: '25rem',
    fontFamily: 'Nunito-Regular',
    fontSize: '15rem'
  },
  itemList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: '16rem',
    paddingHorizontal: '25rem'
  },
  text: {
    fontSize: '15rem'
  },
  fontTitle: {
    fontFamily: 'Nunito-Regular'
  },
  fontItem: {
    fontFamily: 'Nunito-Bold',
    color: '#FF9F1C'
  },
  styleButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: '30rem',
    paddingVertical: '20rem',
    paddingHorizontal: '25rem',
    backgroundColor: '#ff9f1c'
  },
  itemLeftStyle: {
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem',
    color: '#FFFFFF'
  },
  button: {
    paddingHorizontal: '50rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButton: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    color: '#FFFFFF'
  }
})
