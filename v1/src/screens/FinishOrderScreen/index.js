import React from 'react'
import { View, Text, StatusBar, Image, ImageBackground } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import StarRating from 'react-native-star-rating'
import { useNavigation } from '@react-navigation/native'
import { useDispatch } from 'react-redux'
import { clearCart } from '../../redux/actions/cart'
import ActionButton from '../../components/ActionButton'

import bgFavoriteScreen from '../../assets/images/bg-favorite-screen.webp'
import burgerLogo from '../../assets/icons/logo-white.webp'
import imgTerompet from '../../assets/images/terompet.webp'

const FinishOrderScreen = () => {
  return (
    <ImageBackground
      source={bgFavoriteScreen}
      style={styles.container}
    >
      <RenderStatusBar />
      <RenderLogo />
      <RenderMessage />
      <RenderRating />
    </ImageBackground>
  )
}

const RenderStatusBar = () => {
  return (
    <StatusBar
      translucent
      barStyle='light-content'
      backgroundColor='transparent'
    />
  )
}

const RenderLogo = () => {
  return (
    <View style={{ justifyContent: 'center', alignItems: 'center' }}>
      <Image
        source={burgerLogo}
        style={styles.logo}
      />
    </View>
  )
}

const RenderMessage = () => {
  return (
    <View style={styles.message}>
      <Text style={styles.textMessage}>
        Your Order is Complete!
      </Text>

      <Image
        source={imgTerompet}
        style={styles.imgTerompet}
      />
    </View>
  )
}

const RenderRating = () => {
  const navigation = useNavigation()
  const dispatch = useDispatch()
  return (
    <View style={styles.rating}>
      <StarRating
        disabled
        maxStars={5}
        rating={4}
        starSize={30}
        fullStarColor='#ffc800'
        emptyStar='star'
        emptyStarColor='#cecece'
        containerStyle={styles.starRating}
      />

      <ActionButton
        styleButton={styles.styleButtonSubmit}
        titleButton='SUBMIT'
        titleButtonStyle={styles.titleButtonStyle}
        onHandlePress={() => {
          dispatch(clearCart())
          navigation.navigate('HomeScreen')
        }}
      />

      <ActionButton
        styleButton={styles.styleButtonSkipRate}
        titleButton='SKIP RATE'
        titleButtonStyle={styles.titleButtonStyle}
        onHandlePress={() => {
          dispatch(clearCart())
          navigation.navigate('HomeScreen')
        }}
      />
    </View>
  )
}

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  logo: {
    marginTop: '50rem',
    height: '24rem',
    width: '75rem'
  },
  message: {
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: '0.2rem',
    borderBottomColor: '#ffffff',
    marginHorizontal: '20rem',
    paddingBottom: '55rem'
  },
  textMessage: {
    marginTop: '24rem',
    color: '#FFFFFF',
    fontFamily: 'Nunito-Bold',
    fontSize: '26rem',
    textAlign: 'center'
  },
  imgTerompet: {
    height: '125rem',
    width: '125rem',
    marginTop: '35rem'
  },
  rating: {
    width: '80%'
  },
  starRating: {
    paddingHorizontal: '40rem'
  },
  styleButtonSubmit: {
    marginTop: '50rem',
    paddingVertical: '14rem',
    backgroundColor: '#383838',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  styleButtonSkipRate: {
    marginTop: '10rem',
    marginBottom: '30rem',
    paddingVertical: '14rem',
    backgroundColor: 'rgba(255, 255, 255, 0.15)',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: 'white'
  }
})

export default FinishOrderScreen
