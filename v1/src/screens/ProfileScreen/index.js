import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { View, Text, StatusBar, FlatList, Image } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import Modal from 'react-native-modal'
import { showMessage } from 'react-native-flash-message'
import Fire from '../../../util/Fire'
import { getData, storeData } from '../../../util/LocalStorage'

import ActionButton from '../../components/ActionButton'
import MenuHeader from '../../components/MenuHeader'
import CardWallet from '../../components/CardWallet'

import QuestionMark from '../../assets/icons/question-mark.svg'
import profilePicture from '../../assets/images/ProfilePicture.webp'

const ProfileScreen = ({ navigation }) => {
  return (
    <View>
      <RenderStatusBar />
      <RenderHeader navigation={navigation} />
      <FlatList
        showsVerticalScrollIndicator={false}
        data={[0, 1, 2]}
        renderItem={({ index }) => {
          switch (index) {
            case 0:
              return (
                <RenderUserAccount />
              )
            case 1:
              return (
                <RenderWallet />
              )
            case 2:
              return (
                <RenderRecentTransaction />
              )
            default:
              break
          }
        }}
        keyExtractor={index => index.toString()}
      />
    </View>
  )
}

const RenderStatusBar = () => {
  return (
    <StatusBar
      translucent
      barStyle='dark-content'
      backgroundColor='#FFFFFF'
    />
  )
}

const RenderHeader = ({ navigation }) => {
  const [isModalVisible, setModalVisible] = useState(false)

  const onLogout = async () => {
    const getDataUser = await getData('user')
    const dataUser = JSON.parse(getDataUser)
    Fire.auth()
      .signOut()
      .then(async () => {
        if (getDataUser.rememberMe === false) {
          storeData('user', {
            ...dataUser,
            rememberMe: false,
            isLogin: false
          })
        } else {
          storeData('user', {
            ...dataUser,
            isLogin: false
          })
        }

        setModalVisible(false)
        setTimeout(() => {
          navigation.replace('AuthStack')
        }, 100)
      })
      .catch(err => {
        showMessage({
          message: err.message,
          type: 'default',
          position: 'top',
          backgroundColor: '#c0392b',
          color: '#FFFFFF',
          style: {
            marginTop: 20,
            alignItems: 'center'
          }
        })
      })
  }

  useEffect((prevState) => {
    if (isModalVisible !== prevState) {
      if (isModalVisible) {
        setTimeout(() => {
          StatusBar.setBackgroundColor('rgba(0, 0, 0, 0.7)', true)
        }, 10)
      } else {
        setTimeout(() => {
          StatusBar.setBackgroundColor('#FFFFFF', true)
        }, 10)
      }
    }
  })

  return (
    <View>
      <MenuHeader
        leftPress={() => setModalVisible(true)}
        onRightPress={() => navigation.navigate('SettingProfile')}
        left='logout'
        right='profile'
      />

      <Modal
        hardwareAccelerated
        useNativeDriver
        backdropTransitionOutTiming={100}
        isVisible={isModalVisible}
        onBackButtonPress={() => setModalVisible(false)}
        onBackdropPress={() => setModalVisible(false)}
      >
        <View style={styles.modal}>
          <View style={styles.topImageWrapper}>
            <View style={styles.imageModal}>
              <QuestionMark />
            </View>
          </View>

          <Text style={styles.titleModal}>
            Are You Sure?
          </Text>

          <Text style={styles.descModal}>
            Are you sure want to logout from this account?
          </Text>

          <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
            <ActionButton
              styleButton={styles.styleButtonCancel}
              titleButton='CANCEL'
              titleButtonStyle={styles.titleButtonStyleCancel}
              onHandlePress={() => setModalVisible(false)}
            />

            <ActionButton
              styleButton={styles.styleButtonLogout}
              titleButton='LOGOUT'
              titleButtonStyle={styles.titleButtonStyleLogout}
              onHandlePress={onLogout}
            />
          </View>
        </View>
      </Modal>
    </View>
  )
}

const RenderUserAccount = () => {
  return (
    <View style={styles.wrapperUserAccount}>
      <Image
        source={profilePicture}
        style={styles.profilePicture}
      />

      <Text style={styles.nameUser}>
        Darrell Steward
      </Text>

      <Text style={styles.detailUser}>
        081988238237
      </Text>

      <Text style={styles.detailUser}>
        email@example.com
      </Text>
    </View>
  )
}

const RenderWallet = () => {
  return (
    <View style={styles.wrapperWallet}>
      <Text style={styles.titleWallet}>
        Your Wallet
      </Text>

      <CardWallet
        leftItemWallet='profileScreen'
      />
    </View>
  )
}

const RenderRecentTransaction = () => {
  return (
    <View style={styles.wrapperRecentTransaction}>
      <Text style={styles.titleHeaderTransaction}>
        Recent Transaction
      </Text>

      <View style={styles.wrapperTransaction}>
        <View>
          <Text style={styles.titleTransaction}>
            20/07/2020
          </Text>

          <Text style={styles.subTitleTransaction}>
            Chicken Big Burger
          </Text>
        </View>

        <Text style={styles.priceTransaction}>
          Rp 52.000
        </Text>
      </View>
    </View>
  )
}

ProfileScreen.propTypes = {
  navigation: PropTypes.object
}

RenderHeader.propTypes = {
  navigation: PropTypes.object
}

export default ProfileScreen

const styles = EStyleSheet.create({
  modal: {
    borderRadius: '10rem',
    backgroundColor: '#FFFFFF',
    width: '250rem',
    alignSelf: 'center'
  },
  topImageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    width: 100,
    borderRadius: 100 / 2,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    marginTop: '-44rem'
  },
  imageModal: {
    height: 80,
    width: 80,
    borderRadius: 80 / 2,
    backgroundColor: '#EA3A3A',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleModal: {
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem',
    textAlign: 'center'
  },
  descModal: {
    fontFamily: 'Nunito-Regular',
    fontSize: '14rem',
    textAlign: 'center',
    marginTop: '10rem'
  },
  styleButtonCancel: {
    flex: 1,
    marginTop: '30rem',
    paddingVertical: '14rem',
    backgroundColor: '#EA3A3A',
    borderBottomLeftRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  titleButtonStyleCancel: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  },
  styleButtonLogout: {
    flex: 1,
    marginTop: '30rem',
    paddingVertical: '14rem',
    backgroundColor: '#FFFFFF',
    borderBottomRightRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#f2f2f2'
  },
  titleButtonStyleLogout: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#2c2c2c'
  },
  wrapperUserAccount: {
    backgroundColor: '#ffffff',
    marginTop: '5rem',
    paddingVertical: '25rem',
    alignItems: 'center',
    justifyContent: 'center'
  },
  profilePicture: {
    height: '80rem',
    width: '80rem'
  },
  nameUser: {
    fontFamily: 'Nunito-Bold',
    fontSize: '22rem',
    color: '#3F3F3F',
    marginTop: '25rem'
  },
  detailUser: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: '#AEAEAE'
  },
  wrapperWallet: {
    backgroundColor: '#ffffff',
    paddingVertical: '15rem',
    marginTop: '12rem'
  },
  titleWallet: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: '#AEAEAE',
    marginBottom: '5rem',
    paddingLeft: '20rem'
  },
  wrapperRecentTransaction: {
    marginBottom: '150rem'
  },
  titleHeaderTransaction: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: '#AEAEAE',
    marginTop: '12rem',
    marginLeft: '20rem'
  },
  wrapperTransaction: {
    backgroundColor: '#ffffff',
    marginHorizontal: '20rem',
    marginTop: '10rem',
    paddingVertical: '10rem',
    paddingHorizontal: '12rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  titleTransaction: {
    fontFamily: 'Nunito-Regular',
    fontSize: '10rem',
    color: '#AEAEAE'
  },
  subTitleTransaction: {
    fontFamily: 'Nunito-Bold',
    fontSize: '13rem',
    color: '#494343'
  },
  priceTransaction: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    color: '#494343'
  }
})
