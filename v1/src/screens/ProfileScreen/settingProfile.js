import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { View, Text, StatusBar, ImageBackground, TouchableOpacity } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'
import Modal from 'react-native-modal'

import ActionButton from '../../components/ActionButton'

import CrossOutline from '../../assets/icons/cross-outline.svg'
import CheckOutline from '../../assets/icons/check-outline.svg'
import Camera from '../../assets/icons/camera.svg'
import CircleRight from '../../assets/icons/circle-right.svg'
import QuestionMark from '../../assets/icons/question-mark.svg'

import profilePicture from '../../assets/images/ProfilePicture.webp'

const SettingProfile = ({ navigation }) => {
  return (
    <View>
      <RenderStatusBar />
      <RenderHeaderProfile navigation={navigation} />
      <RenderContentProfile navigation={navigation} />
    </View>
  )
}

const RenderStatusBar = () => {
  return (
    <StatusBar
      translucent
      barStyle='dark-content'
      backgroundColor='#FFFFFF'
    />
  )
}

const RenderHeaderProfile = ({ navigation }) => {
  const [isModalVisible, setModalVisible] = useState(false)

  const changeSetting = () => {
    setModalVisible(false)
    setTimeout(() => {
      navigation.navigate('ProfileScreen')
    }, 300)
  }

  useEffect((prevState) => {
    if (isModalVisible !== prevState) {
      if (isModalVisible) {
        setTimeout(() => {
          StatusBar.setBackgroundColor('rgba(0, 0, 0, 0.7)', true)
        }, 10)
      } else {
        setTimeout(() => {
          StatusBar.setBackgroundColor('#FFFFFF', true)
        }, 10)
      }
    }
  })

  return (
    <View style={styles.wrapperHeaderProfile}>
      <View style={styles.iconHeader}>
        <CrossOutline
          onPress={() => navigation.navigate('ProfileScreen')}
        />

        <CheckOutline
          onPress={() => setModalVisible(true)}
        />

        <Modal
          useNativeDriver
          isVisible={isModalVisible}
          onBackButtonPress={changeSetting}
          onBackdropPress={() => setModalVisible(false)}
        >
          <View style={styles.modal}>
            <View style={styles.topImageWrapper}>
              <View style={styles.imageModal}>
                <QuestionMark />
              </View>
            </View>

            <Text style={styles.titleModal}>
              Are You Sure?
            </Text>

            <Text style={styles.descModal}>
              Are you sure to change this setting on your profile with the new setting?
            </Text>

            <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
              <ActionButton
                styleButton={styles.styleButtonCancel}
                titleButton='CANCEL'
                titleButtonStyle={styles.titleButtonStyleCancel}
                onHandlePress={() => setModalVisible(false)}
              />

              <ActionButton
                styleButton={styles.styleButtonLogout}
                titleButton='CHANGE'
                titleButtonStyle={styles.titleButtonStyleLogout}
                onHandlePress={changeSetting}
              />
            </View>
          </View>
        </Modal>
      </View>

      <View style={styles.wrapperProfile}>
        <ImageBackground
          source={profilePicture}
          style={styles.profilePicture}
        >
          <Camera
            style={{ zIndex: 1 }}
          />

          <View style={styles.overlay} />
        </ImageBackground>

        <Text style={styles.nameUser}>
          Darrell Steward
        </Text>
      </View>
    </View>
  )
}

const RenderContentProfile = ({ navigation }) => {
  const [phoneNumber, setPhoneNumber] = useState('')

  const DataPhoneNumber = () => {
    if (phoneNumber !== '') {
      return (
        <Text style={styles.itemPhoneNumber}>
          {phoneNumber}
        </Text>
      )
    } else {
      return (
        <Text style={styles.addPhoneNumber}>
          Add Phone Number
        </Text>
      )
    }
  }

  return (
    <View style={styles.wrapperContentProfile}>
      <View style={styles.wrapperEmail}>
        <Text style={styles.titleEmail}>
          Email
        </Text>

        <Text style={styles.isEmail}>
          example@gmail.com
        </Text>
      </View>

      <TouchableOpacity
        onPress={() => navigation.navigate('SavePhoneNumber', {
          setPhoneNumber: setPhoneNumber
        })}
        style={styles.wrapperPhoneNumber}
      >
        <View>
          <Text style={styles.titlePhoneNumber}>
            No. Handphone
          </Text>

          <DataPhoneNumber />
        </View>

        <View style={styles.iconRight}>
          <CircleRight />
        </View>
      </TouchableOpacity>
    </View>
  )
}

SettingProfile.propTypes = {
  navigation: PropTypes.object
}

RenderHeaderProfile.propTypes = {
  navigation: PropTypes.object
}

RenderContentProfile.propTypes = {
  navigation: PropTypes.object
}

export default SettingProfile

const styles = EStyleSheet.create({
  wrapperHeaderProfile: {
    backgroundColor: '#ffffff',
    marginTop: '5rem',
    paddingVertical: '25rem',
    paddingHorizontal: '15rem'
  },
  iconHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  modal: {
    borderRadius: '10rem',
    backgroundColor: '#FFFFFF',
    marginHorizontal: '19rem',
    paddingHorizontal: '10rem'
  },
  topImageWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 100,
    width: 100,
    borderRadius: 100 / 2,
    backgroundColor: '#FFFFFF',
    alignSelf: 'center',
    marginTop: '-44rem'
  },
  imageModal: {
    height: 80,
    width: 80,
    borderRadius: 80 / 2,
    backgroundColor: '#EA3A3A',
    justifyContent: 'center',
    alignItems: 'center'
  },
  titleModal: {
    fontFamily: 'Nunito-Bold',
    fontSize: '16rem',
    textAlign: 'center'
  },
  descModal: {
    fontFamily: 'Nunito-Regular',
    fontSize: '14rem',
    textAlign: 'center',
    marginTop: '10rem'
  },
  styleButtonCancel: {
    marginTop: '30rem',
    paddingVertical: '14rem',
    paddingHorizontal: '33rem',
    backgroundColor: '#EA3A3A',
    borderBottomLeftRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  titleButtonStyleCancel: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  },
  styleButtonLogout: {
    marginTop: '30rem',
    paddingVertical: '14rem',
    paddingHorizontal: '33rem',
    backgroundColor: '#FFFFFF',
    borderBottomRightRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#f2f2f2'
  },
  titleButtonStyleLogout: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#2c2c2c'
  },
  wrapperProfile: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: '20rem'
  },
  profilePicture: {
    height: '100rem',
    width: '100rem',
    alignItems: 'center',
    justifyContent: 'center'
  },
  overlay: {
    height: '100rem',
    width: '100rem',
    borderRadius: '100rem',
    backgroundColor: 'rgba(208, 208, 208, 0.65)',
    position: 'absolute'
  },
  nameUser: {
    fontFamily: 'Nunito-Bold',
    fontSize: '22rem',
    color: '#3F3F3F',
    marginTop: '25rem'
  },
  wrapperContentProfile: {
    backgroundColor: '#ffffff',
    marginTop: '20rem',
    paddingTop: '3rem',
    paddingBottom: '250rem',
    paddingHorizontal: '15rem'
  },
  wrapperEmail: {
    borderBottomWidth: '1rem',
    borderBottomColor: '#C5C5C5',
    paddingBottom: '16rem'
  },
  titleEmail: {
    fontFamily: 'Nunito-Regular',
    fontSize: '16rem',
    color: '#3F3F3F'
  },
  isEmail: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#ff9f1c',
    textDecorationLine: 'underline',
    marginTop: '3rem'
  },
  wrapperPhoneNumber: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: '1rem',
    borderBottomColor: '#C5C5C5',
    paddingBottom: '10rem',
    marginTop: '5rem'
  },
  titlePhoneNumber: {
    fontFamily: 'Nunito-Regular',
    fontSize: '16rem',
    color: '#3f3f3f'
  },
  addPhoneNumber: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#a4a4a4'
  },
  itemPhoneNumber: {
    fontFamily: 'Nunito-Regular',
    fontSize: '12rem',
    color: '#FFA51E'
  },
  iconRight: {
    justifyContent: 'center',
    marginRight: '12rem'
  }
})
