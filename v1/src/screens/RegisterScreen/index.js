import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { showMessage } from 'react-native-flash-message'
import EStyleSheet from 'react-native-extended-stylesheet'
import {
  View,
  Dimensions,
  ImageBackground,
  StatusBar,
  ScrollView,
  Text,
  TouchableOpacity
} from 'react-native'

import Bg from '../../assets/images/background-image.webp'

import InputForm from '../../components/InputForm'
import ActionButton from '../../components/ActionButton'
import Logo from '../../components/Logo'
import TitleScreen from '../../components/TitleScreen'
import Loading from '../../components/Loading'

import { useForm } from '../../../util/UseForm'
import Fire from '../../../util/Fire'
import { storeData } from '../../../util/LocalStorage'

const { height, width } = Dimensions.get('window')
const RegisterScreen = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  const [form, setForm] = useForm({
    name: '',
    email: '',
    phoneNumber: '',
    password: '',
    confirmPassword: ''
  })

  const onContinue = () => {
    if (form.password !== form.confirmPassword) {
      showMessage({
        message: 'password and confirm password are not the same',
        type: 'default',
        position: 'top',
        backgroundColor: '#c0392b',
        color: '#FFFFFF',
        style: {
          marginTop: 20,
          alignItems: 'center'
        }
      })
    } else if (form.password.length < 6 && form.confirmPassword.length < 6) {
      showMessage({
        message: 'Password should be at least 6 characters !!',
        type: 'default',
        position: 'top',
        backgroundColor: '#c0392b',
        color: '#FFFFFF',
        style: {
          marginTop: 20,
          alignItems: 'center'
        }
      })
    } else if (form.confirmPassword === form.password) {
      setLoading(true)
      Fire.auth()
        .createUserWithEmailAndPassword(form.email, form.password)
        .then(success => {
          setLoading(false)
          setForm('reset')
          const data = {
            name: form.name,
            password: form.password,
            email: form.email,
            phoneNumber: '',
            uid: success.user.uid,
            img_url: '',
            wallets: [
              {
                name: 'linkaja',
                owner: {
                  phone_number: '',
                  name: '',
                  balance: 0
                }
              },
              {
                name: 'dana',
                owner: {
                  phone_number: '',
                  name: '',
                  balance: 0
                }
              },
              {
                name: 'gopay',
                owner: {
                  phone_number: '',
                  name: '',
                  balance: 0
                }
              },
              {
                name: 'ovo',
                owner: {
                  phone_number: '',
                  name: '',
                  balance: 0
                }
              }
            ],
            history_transaction: []
          }
          Fire.database()
            .ref('users/' + success.user.uid + '/')
            .set(data)

          storeData('user', data)
          navigation.replace('LoginScreen')
        })
        .catch(error => {
          setLoading(false)
          console.log(error.code)
          showMessage({
            message: error.message,
            type: 'default',
            position: 'top',
            backgroundColor: '#c0392b',
            color: '#FFFFFF',
            style: {
              marginTop: 20,
              alignItems: 'center'
            }
          })
        })
    }
  }
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <ImageBackground source={Bg} style={{ ...styles.imgBg, height: height }}>
        <StatusBar translucent backgroundColor='transparent' />
        <View style={styles.bgOverlay} />
        <View style={{ ...styles.contentWrapper }}>
          <Logo />
          <View style={styles.wrapper}>
            <TitleScreen
              mainTitle='Sign Up'
              secondTitle='Create Your Burger City Account'
            />
            <View style={styles.form}>
              <InputForm
                iconName='user-circle'
                placeholder='Username'
                iconSize={18}
                value={form.name}
                onChangeText={value => setForm('name', value)}
              />
              <InputForm
                iconName='envelope'
                placeholder='Email Address'
                iconSize={18}
                value={form.email}
                onChangeText={value => setForm('email', value)}
              />
              <InputForm
                secureTextEntry
                iconName='lock'
                placeholder='Password'
                iconSize={25}
                value={form.password}
                onChangeText={value => setForm('password', value)}
              />
              <InputForm
                secureTextEntry
                iconName='lock'
                placeholder='Confirm Password'
                iconSize={25}
                value={form.confirmPassword}
                onChangeText={value => setForm('confirmPassword', value)}
              />
              <TouchableOpacity style={styles.link} onPress={() => navigation.navigate('LoginScreen')}>
                <Text style={styles.linkTitle}>Have an account ? <Text style={styles.linkTitleBold}>Sign In</Text></Text>
              </TouchableOpacity>
              <ActionButton
                styleButton={styles.styleButton}
                titleButton='NEXT'
                titleButtonStyle={styles.titleButtonStyle}
                onHandlePress={onContinue}
              />
            </View>
          </View>
        </View>
      </ImageBackground>
      {loading && <Loading />}
    </ScrollView>
  )
}

export default RegisterScreen

RegisterScreen.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  imgBg: {
    flex: 1,
    width: '100%',
    resizeMode: 'contain'
  },
  bgOverlay: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5
  },
  contentWrapper: {
    alignItems: 'center',
    position: 'absolute',
    paddingVertical: '10%'
  },
  wrapper: {
    height: height / 1.15,
    width: width,
    marginTop: '5rem'
  },
  form: {
    paddingHorizontal: '20rem',
    paddingVertical: '5rem'
  },
  wrapperResetPass: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 0
  },
  wrapperRadio: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  button: {
    paddingTop: '10rem'
  },
  actionButton: {
    marginTop: '25rem',
    borderRadius: '8rem'
  },
  link: {
    paddingTop: '22rem',
    justifyContent: 'center',
    alignItems: 'center'
  },
  linkTitle: {
    fontSize: '12rem',
    fontFamily: 'Nunito-Bold',
    color: '#FFFFFF'
  },
  linkTitleBold: {
    fontSize: '12rem',
    fontFamily: 'Nunito-Bold',
    color: '#FF9F1C',
    textDecorationLine: 'underline'
  },
  styleButton: {
    marginTop: '25rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  }
})
