import React, { useState, useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import { SafeAreaView, View, Text, Image, StatusBar } from 'react-native'
import EStyleSheet from 'react-native-extended-stylesheet'

import ReactNativePinView from 'react-native-pin-view'
import Feather from 'react-native-vector-icons/Feather'
import Icon from 'react-native-vector-icons/Ionicons'

import Loading from '../../../components/Loading'

import danaIcon from '../../../assets/icons/dana.webp'
import gopayIcon from '../../../assets/icons/go-pay-logo.png'
import ovoIcon from '../../../assets/icons/ovo.webp'
import linkajaIcon from '../../../assets/icons/link-aja.webp'

const InputPinWallet = ({ navigation, route }) => {
  const pinView = useRef(null)
  const [showRemoveButton, setShowRemoveButton] = useState(false)
  const [enteredPin, setEnteredPin] = useState('')
  const [isLoading, setLoading] = useState(false)

  useEffect(() => {
    if (enteredPin.length === 0) {
      setShowRemoveButton(true)
    } else if (enteredPin.length === 4) {
      setLoading(true)
      route.params.setDataWallet((prevState) => {
        prevState[route.params.index] = {
          ...prevState[route.params.index],
          pin: enteredPin
        }

        return [...prevState]
      })
      setTimeout(() => {
        setLoading(false)
        navigation.navigate('ProfileScreen')
      }, 3000)
    }
  }, [enteredPin])

  useEffect((prevState) => {
    if (isLoading !== prevState) {
      if (isLoading) {
        setTimeout(() => {
          StatusBar.setBackgroundColor('rgba(0, 0, 0, 0)', true)
        }, 10)
      } else {
        setTimeout(() => {
          StatusBar.setBackgroundColor('transparent', true)
        }, 10)
      }
    }
  })

  const iconWallet = route.params.index === 1
    ? danaIcon
    : route.params.index === 2
      ? gopayIcon
      : route.params.index === 3
        ? ovoIcon
        : route.params.index === 4
          ? linkajaIcon
          : null

  const titleWallet = route.params.index === 1
    ? 'Dana'
    : route.params.index === 2
      ? 'GoPay'
      : route.params.index === 3
        ? 'OVO'
        : route.params.index === 4
          ? 'LinkAja'
          : null

  const heightIcon = route.params.index === 1
    ? 81
    : route.params.index === 2
      ? 70
      : route.params.index === 3
        ? 30
        : route.params.index === 4
          ? 70
          : null

  const widhtIcon = route.params.index === 1
    ? 80
    : route.params.index === 2
      ? 80
      : route.params.index === 3
        ? 90
        : route.params.index === 4
          ? 78
          : null

  const colorWallet = route.params.index === 1
    ? '#108DE8'
    : route.params.index === 2
      ? '#00ACD4'
      : route.params.index === 3
        ? '#8256CD'
        : route.params.index === 4
          ? '#E75558'
          : null

  return (
    <>
      <SafeAreaView style={styles.container}>
        <StatusBar
          translucent
          barStyle='dark-content'
          backgroundColor='transparent'
        />

        <View style={styles.badge}>
          <Feather
            onPress={() => navigation.navigate('SendOTPWallet')}
            name='arrow-left'
            color='#3C3833'
            size={24}
          />
        </View>

        <View style={styles.wrapperLogo}>
          <Image
            source={iconWallet}
            style={{
              height: heightIcon,
              width: widhtIcon,
              marginBottom: EStyleSheet.value('2rem')
            }}
          />

          {(route.params.index === 1 || route.params.index === 2) && (
            <Text style={{ ...styles.textLogoWallet, color: colorWallet }}>
              {titleWallet}
            </Text>
          )}
        </View>

        <Text style={styles.textDescWallet}>
          Masukan Pin {titleWallet} Anda
        </Text>

        <ReactNativePinView
          inputSize={15}
          ref={pinView}
          pinLength={4}
          buttonSize={50}
          onValueChange={value => setEnteredPin(value)}
          inputAreaStyle={styles.inputAreaStyle}
          inputViewEmptyStyle={styles.inputViewEmptyStyle}
          inputViewFilledStyle={styles.inputViewFilledStyle}
          buttonAreaStyle={styles.buttonAreaStyle}
          buttonViewStyle={styles.buttonViewStyle}
          buttonTextStyle={styles.buttonTextStyle}
          onButtonPress={key => {
            if (key === 'custom_right') {
              pinView.current.clear()
            }
          }}
          customRightButton={
            showRemoveButton
              ? <Icon name='ios-backspace' size={22} color='#141414' />
              : undefined
          }
        />
      </SafeAreaView>
      {isLoading && <Loading />}
    </>
  )
}

InputPinWallet.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object
}

export default InputPinWallet

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: '20rem'
  },
  badge: {
    position: 'absolute',
    top: '32rem',
    left: '22rem'
  },
  wrapperLogo: {
    alignItems: 'center',
    marginTop: '65rem'
  },
  textLogoWallet: {
    fontFamily: 'Nunito-Bold',
    fontSize: '24rem',
    marginTop: '2rem'
  },
  textDescWallet: {
    fontFamily: 'Nunito-Bold',
    fontSize: '14rem',
    color: '#8F8F8F',
    textAlign: 'center',
    marginTop: '30rem',
    paddingHorizontal: '20rem'
  },
  inputAreaStyle: {
    marginTop: '30rem'
  },
  inputViewEmptyStyle: {
    backgroundColor: '#c4c4c4',
    margin: '25rem'
  },
  inputViewFilledStyle: {
    backgroundColor: '#141414',
    margin: '25rem'
  },
  buttonAreaStyle: {
    marginTop: '10rem'
  },
  buttonViewStyle: {
    marginTop: '6rem'
  },
  buttonTextStyle: {
    color: '#141414',
    fontSize: '24rem'
  }
})
