/* eslint-disable import/no-duplicates */
import React, { useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { Text, View, StatusBar, Dimensions, ImageBackground, TouchableHighlight, FlatList, Image, TouchableOpacity } from 'react-native'
import AwesomeIcon from 'react-native-vector-icons/MaterialCommunityIcons'
import EStyleSheet from 'react-native-extended-stylesheet'
import LinearGratient from 'react-native-linear-gradient'
import Dash from 'react-native-dash'
import Modal from 'react-native-modal'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { useDispatch, useSelector } from 'react-redux'
import { setDataMenu, setRemakeItemsMenu, clearMenu } from '../../redux/actions/menu'
import { addToCart } from '../../redux/actions/cart'

import Delivery from '../../assets/images/Delivery.png'
import Bottle from '../../assets/images/Bottle.png'
import Fire from '../../../util/Fire'
import { getData } from '../../../util/LocalStorage'
import ItemList from '../../components/ItemList'
import Loading from '../../components/Loading'
import imgBanner from '../../assets/images/ourBurgerSlider.webp'

const { width } = Dimensions.get('window')

const MenuPacket = ({ navigation, route }) => {
  const dispatch = useDispatch()

  const cart = useSelector(state => state.cart)
  const data = useSelector(state => state.menu.data)

  const { menu: menuName, name } = route.params
  const [loading, setLoading] = useState(false)
  const [modalVisible, setModalVisible] = useState(false)

  const onGetData = async () => {
    try {
      setLoading(true)
      const response = await Fire.database()
        .ref(`menu/category/${menuName}`)
        .once('value')

      const data = response.val()

      const newItems = []
      data.items.map((item) => {
        if (menuName === 'meals') {
          const remakeData = {
            ...item,
            count: 0,
            category: menuName,
            package_item: {
              beverage: {
                chosen: item.package_item.beverage[0],
                option: item.package_item.beverage
              },
              main_course: {
                chosen: item.package_item.main_course[0],
                option: item.package_item.main_course
              },
              side_dish: {
                chosen: item.package_item.side_dish[0],
                option: item.package_item.side_dish
              }
            }
          }
          newItems.push(remakeData)
        } else {
          newItems.push({ ...item, count: 0, category: menuName })
        }
      })

      const cartItemsId = new Set(cart[menuName].map(({ id }) => id))
      const filterItemsMenu = newItems.filter(({ id }) => !cartItemsId.has(id))
      const concatItems = [...filterItemsMenu, ...cart[menuName]]
      const sortItems = concatItems.sort((a, b) => a.id - b.id)

      dispatch(setDataMenu(data))
      dispatch(setRemakeItemsMenu(sortItems))

      setLoading(false)
    } catch (error) {
      console.log('dari getDataMenu')
      console.log(error)
      setLoading(false)
    }
  }

  const toggleModal = () => {
    setModalVisible(false)
  }

  useEffect(() => {
    let unsubscribe
    unsubscribe = navigation.addListener('focus', () => {
      const isLoadApiCheck = async () => {
        const isLoadApi = await getData('isLoadApi')
        if (isLoadApi !== undefined) {
          if (JSON.parse(isLoadApi)) onGetData()
        }
      }
      isLoadApiCheck()
    })
    unsubscribe = navigation.addListener('blur', () => {
      dispatch(addToCart(menuName))
      dispatch(clearMenu())
    })
    return unsubscribe
  }, [navigation])

  let imgSrc = imgBanner
  if (typeof data.thumbnail === 'string') {
    imgSrc = { uri: data.thumbnail }
  }

  const itemTypeIcon = menuName !== 'meals' ? 'star' : 'chevron-right'

  return (
    <>
      <View style={styles.container}>
        <StatusBar
          translucent
          backgroundColor='transparent'
          barStyle='light-content'
        />
        <FlatList
          showsVerticalScrollIndicator={false}
          data={[0, 1, 2]}
          keyExtractor={index => index.toString()}
          renderItem={({ item, index }) => {
            switch (index) {
              case 0:
                return (
                  <ImageBackground
                    source={imgSrc}
                    style={styles.imageHeader}
                  >
                    <LinearGratient
                      colors={['black', 'transparent']}
                      style={styles.gradient}
                    >
                      <TouchableHighlight
                        onPress={() => {
                          navigation.goBack()
                          dispatch(clearMenu())
                        }}
                        style={styles.backAction}
                      >
                        <AwesomeIcon
                          name='arrow-left'
                          color='white'
                          size={EStyleSheet.value('20rem')}
                        />
                      </TouchableHighlight>

                    </LinearGratient>
                  </ImageBackground>
                )
              case 1:
                return (
                  <View style={styles.itemTitle}>
                    <Text style={styles.title}>
                      {name}
                    </Text>
                    <Text style={styles.subTitle}>
                      {data.tags[0]}
                    </Text>
                    <Dash
                      dashColor='#CACACA'
                      style={{ marginVertical: '5%' }}
                      dashStyle={{ height: 1 }}
                      dashGap={3}
                    />
                    <View style={styles.cost}>
                      <View>
                        <Text style={styles.costLeft}>
                          Delevery cost
                        </Text>
                        <Text style={styles.costLeft}>
                          Check for available promos
                        </Text>
                      </View>
                      <View>
                        <Text style={styles.costRight}>
                          Rp 15.000
                        </Text>
                        <Text
                          style={styles.costRight}
                          onPress={() => setModalVisible(true)}
                        >
                          See Promos
                        </Text>
                      </View>
                    </View>
                  </View>
                )
              case 2:
                return (
                  <View style={styles.itemList}>
                    <Text style={styles.listTitle}>
                      Item List
                    </Text>
                    <ItemList
                      menuName={menuName}
                      itemTypeIcon={itemTypeIcon}
                    />
                  </View>
                )
              default:
                break
            }
          }}
        />
        <Modal useNativeDriver isVisible={modalVisible} onBackButtonPress={toggleModal} onBackdropPress={toggleModal} style={styles.wrapperModal}>
          <View style={styles.modal}>
            <View style={styles.wrapperTitle}>
              <Text style={styles.titleModal}>If you want to change</Text>
              <Text style={styles.titleModal}>Your promos code</Text>
            </View>
            <View style={styles.cardModal}>
              <Image source={Delivery} style={styles.imageModal} />
              <View style={styles.contentModal}>
                <Text style={styles.titleContentModal}>Free Delivery Fee</Text>
                <View style={styles.wrapperSubTitleContentModal}>
                  <MaterialCommunityIcons name='calendar-check' size={16} color='#9D9D9D' style={styles.iconSubTitleContentModal} />
                  <Text style={styles.subTitleContentModal}>valid until 30 Jul 2020</Text>
                </View>
              </View>
              <TouchableOpacity style={styles.buttonModal}>
                <Text style={styles.titleButtonModal}>USE</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.cardModal}>
              <Image source={Bottle} style={styles.imageModal} />
              <View style={styles.contentModal}>
                <Text style={styles.titleContentModal}>Free Small Coca - Cola</Text>
                <View style={styles.wrapperSubTitleContentModal}>
                  <MaterialCommunityIcons name='calendar-check' size={16} color='#9D9D9D' style={styles.iconSubTitleContentModal} />
                  <Text style={styles.subTitleContentModal}>valid until 30 Jul 2020</Text>
                </View>
              </View>
              <TouchableOpacity style={styles.buttonModal}>
                <Text style={styles.titleButtonModal}>USE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </Modal>
      </View>
      {loading && <Loading type='splash' />}
    </>
  )
}

export default MenuPacket

MenuPacket.propTypes = {
  navigation: PropTypes.object,
  route: PropTypes.object
}

const styles = EStyleSheet.create({
  container: {
    flex: 1
  },
  imageHeader: {
    width: width,
    height: '193rem',
    position: 'relative'
  },
  gradient: { height: '100%' },
  backAction: {
    height: '30rem',
    width: '30rem',
    backgroundColor: 'rgba(168,168,168,.7)',
    borderRadius: '25rem',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '35rem',
    marginLeft: '20rem'
  },
  itemTitle: {
    padding: '20rem',
    paddingVertical: '13rem',
    backgroundColor: 'white'
  },
  title: {
    fontFamily: 'Nunito-Regular',
    fontSize: '20rem',
    fontWeight: 'bold',
    color: '#1D2126'
  },
  subTitle: {
    fontFamily: 'Nunito-Regular',
    fontSize: '15rem',
    color: '#727C8E'
  },
  cost: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  costLeft: {
    fontFamily: 'Nunito-Regular',
    fontSize: '13rem',
    color: '#171717',
    marginBottom: '5rem',
    fontWeight: 'normal'
  },
  costRight: {
    fontFamily: 'Nunito-Bold',
    color: '#FF9F1C',
    fontSize: '14rem',
    marginBottom: '5rem',
    textAlign: 'right'
  },
  itemList: {
    backgroundColor: 'white',
    padding: '20rem',
    paddingVertical: '10rem',
    marginTop: '10rem'
  },
  listTitle: {
    fontFamily: 'Nunito-Bold',
    fontSize: '15rem',
    color: '#121212'
  },
  wrapperModal: {
    marginTop: '90%',
    marginHorizontal: '0rem'
  },
  modal: {
    borderRadius: '8rem',
    backgroundColor: '#E5E5E5',
    paddingTop: '10rem',
    paddingBottom: '120rem',
    paddingHorizontal: '10rem'
  },
  wrapperTitle: {
    paddingTop: '5rem'
  },
  titleModal: {
    fontSize: '12rem',
    fontFamily: 'Nunito-Black',
    color: '#222222'
  },
  cardModal: {
    marginTop: '10rem',
    paddingLeft: '10rem',
    paddingRight: '10rem',
    paddingVertical: '10rem',
    backgroundColor: '#FFFFFF',
    borderRadius: '5rem',
    flexDirection: 'row',
    alignItems: 'center'
  },
  imageModal: {
    marginLeft: '10rem',
    marginRight: '20rem',
    width: '70rem',
    height: '70rem'
  },
  contentModal: {
    paddingHorizontal: '10rem',
    marginBottom: 'auto',
    paddingTop: '5rem'
  },
  titleContentModal: {
    fontSize: '12rem',
    fontFamily: 'Nunito-Black'
  },
  wrapperSubTitleContentModal: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: '5rem'
  },
  subTitleContentModal: {
    fontSize: '9rem',
    fontFamily: 'Nunito-Regular',
    color: '#222222'
  },
  iconSubTitleContentModal: {
    paddingRight: '7rem'
  },
  buttonModal: {
    paddingHorizontal: '10rem',
    paddingVertical: '5rem',
    backgroundColor: 'orange',
    borderRadius: '5rem',
    alignSelf: 'flex-end',
    marginLeft: 'auto'
  },
  titleButtonModal: {
    fontSize: '10rem',
    color: '#FFFFFF',
    fontFamily: 'Nunito-Black'
  }
})
