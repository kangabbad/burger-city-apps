import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import Bg from '../../assets/images/background-image.webp'
import { RadioButton } from 'react-native-paper'
import EStyleSheet from 'react-native-extended-stylesheet'
import {
  View,
  Dimensions,
  ImageBackground,
  Text,
  StatusBar,
  TouchableOpacity
} from 'react-native'
import InputForm from '../../components/InputForm'
import ActionButton from '../../components/ActionButton'
import Logo from '../../components/Logo'
import TitleScreen from '../../components/TitleScreen'
import { useForm } from '../../../util/UseForm'
import Fire from '../../../util/Fire'
import { showMessage } from 'react-native-flash-message'
import Loading from '../../components/Loading'
import { storeData, getData } from '../../../util/LocalStorage'

const { height, width } = Dimensions.get('window')

const LoginScreen = ({ navigation }) => {
  const [form, setForm] = useForm({ email: '', password: '', isError: false })
  const [loading, setLoading] = useState(false)
  const [checked, setChecked] = useState(false)
  const isLogin = () => {
    setLoading(true)
    Fire.auth()
      .signInWithEmailAndPassword(form.email, form.password)
      .then(res => {
        Fire.database()
          .ref(`users/${res.user.uid}/`)
          .once('value')
          .then(resDB => {
            setLoading(false)
            storeData('user', {
              ...form,
              rememberMe: checked,
              isLogin: true
            })

            if (resDB.val()) {
              navigation.replace('InApp')
              setForm('reset')
            }
          })
      })
      .catch(err => {
        setLoading(false)
        showMessage({
          message: err.message,
          type: 'default',
          position: 'top',
          backgroundColor: '#c0392b',
          color: '#FFFFFF',
          style: {
            marginTop: 20,
            alignItems: 'center'
          }
        })
      })
  }

  const isCheck = () => {
    setChecked(!checked)
  }

  useEffect(() => {
    const fetchData = async () => {
      const data = await getData('user')
      if (data !== undefined) {
        const dataUser = JSON.parse(data)
        if (dataUser.rememberMe) {
          setForm('email', dataUser.email)
          setForm('password', dataUser.password)
          setChecked(dataUser.rememberMe)
        }
      }
    }
    fetchData()
  }, [])

  return (
    <>
      <ImageBackground source={Bg} style={{ ...styles.imgBg }}>
        <StatusBar translucent backgroundColor='transparent' />
        <View style={styles.bgOverlay} />
        <View style={{ ...styles.contentWrapper }}>
          <Logo />
          <View style={styles.wrapper}>
            <TitleScreen
              mainTitle='Welcome Back!'
              secondTitle='Login to continue Burger City'
            />
            <View style={styles.form}>
              <InputForm
                iconName='envelope'
                placeholder='Email Address'
                iconSize={18}
                value={form.email}
                onChangeText={value => setForm('email', value)}
              />
              <InputForm
                secureTextEntry
                iconName='lock'
                placeholder='Password'
                iconSize={25}
                value={form.password}
                onChangeText={value => setForm('password', value)}
              />
              <View style={styles.wrapperResetPass}>
                <View style={styles.wrapperRadio}>
                  <RadioButton
                    value='true'
                    status={checked === true ? 'checked' : 'unchecked'}
                    onPress={isCheck}
                    color='white'
                    uncheckedColor='white'
                  />
                  <Text
                    style={styles.rememberText}
                  >
                    Remember me
                  </Text>
                </View>
                <Text
                  style={styles.forgotText}
                  onPress={() => navigation.navigate('ForgetByEmail')}
                >
                  Forget Password ?
                </Text>
              </View>
              <ActionButton
                styleButton={styles.styleButton}
                titleButton='Log In'
                titleButtonStyle={styles.titleButtonStyle}
                onHandlePress={isLogin}
              />
              <TouchableOpacity onPress={() => navigation.navigate('RegisterScreen')}>
                <Text style={styles.signUpText}>New user? Sign up</Text>
              </TouchableOpacity>
              <View>
                <Text style={styles.privacyText}>
                  By signing up you indicate that you have read and{' '}
                  <Text style={styles.agreeText}>agreed to the</Text>{' '}
                  <Text style={styles.termText}>Patch Terms of Service</Text>
                </Text>
              </View>
            </View>
          </View>
        </View>
      </ImageBackground>
      {loading && <Loading type='splash' />}
    </>
  )
}

export default LoginScreen

LoginScreen.propTypes = {
  navigation: PropTypes.object
}

const styles = EStyleSheet.create({
  FontHome: {
    fontFamily: 'Nunito-Black',
    color: 'white',
    fontSize: '24rem'
  },
  imgBg: {
    flex: 1,
    width: undefined,
    resizeMode: 'contain'
  },
  bgOverlay: {
    flex: 1,
    backgroundColor: 'black',
    opacity: 0.5
  },
  contentWrapper: {
    top: 0,
    left: 0,
    alignItems: 'center',
    position: 'absolute',
    marginTop: '15%'
  },
  wrapper: {
    height: height / 1.15,
    width: width,
    marginTop: '10rem'
  },
  form: {
    paddingHorizontal: '20rem',
    marginTop: '5%'
  },
  wrapperResetPass: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 0
  },
  wrapperRadio: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  rememberText: {
    color: 'white',
    fontFamily: 'Nunito-Regular'
  },
  forgotText: {
    color: 'white',
    fontFamily: 'Nunito-Regular'
  },
  styleButton: {
    marginTop: '20rem',
    paddingVertical: '14rem',
    backgroundColor: '#FF9F1C',
    borderRadius: '8rem',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  titleButtonStyle: {
    fontFamily: 'Nunito-Black',
    fontSize: '14rem',
    color: '#ffffff'
  },
  signUpText: {
    color: '#FF9F1C',
    textAlign: 'center',
    marginTop: '10rem',
    fontFamily: 'Nunito-Bold'
  },
  touch: {
    paddingHorizontal: '35rem',
    marginTop: '80rem'
  },
  textStart: {
    fontFamily: 'Nunito-Black',
    color: 'white',
    backgroundColor: '#FF9F1C',
    padding: '15rem',
    textAlign: 'center',
    borderRadius: '8rem'
  },
  privacyText: {
    color: 'white',
    textAlign: 'center',
    marginTop: '15rem',
    fontFamily: 'Nunito-Light',
    fontSize: '11rem'
  },
  agreeText: {
    fontFamily: 'Nunito-Black'
  },
  termText: {
    fontFamily: 'Nunito-Black',
    textDecorationLine: 'underline'
  }
})
