import { ADD_TO_CART, CLEAR_CART } from '../../reducers/cart/type'

export const addToCart = (category) => {
  return (dispatch, getState) => {
    console.log('addToCart terpanggil')
    const { remakeItemsMenu } = getState().menu
    const result = []

    remakeItemsMenu.filter((item, index) => {
      if (item.count > 0) {
        result.push(item)
      }
    })

    const formattedResult = new Set([...result])

    dispatch({
      type: ADD_TO_CART,
      payload: {
        key: category,
        value: [...formattedResult]
      }
    })
  }
}

export const clearCart = () => {
  return {
    type: CLEAR_CART,
    payload: {
      meals: [],
      salads: [],
      desserts: [],
      beverages: [],
      burgers: [],
      payment: {
        order_method: 'delivery',
        delivery_address: {
          latitude: -7.9311342,
          longitude: 110.4314679,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
          province: 'Yogyakarta',
          regency: 'Bantul',
          district: 'Mangunan',
          detail_address: 'Jalan Mangunan, Sukorame, Mangunan',
          notes: ''
        },
        delivery_fee: 12000,
        subtotal: 60000,
        promo: {
          use_code: 'FREESANTRI',
          discount: 30000
        },
        total_payment: 30000,
        selected_wallet: 'cod'
      }
    }
  }
}
