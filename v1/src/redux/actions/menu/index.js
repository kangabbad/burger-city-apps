import {
  GET_MENU,
  SET_DATA_MENU,
  REMAKE_ITEMS_MENU,
  COUNT_ITEMS_MENU,
  CLEAR_MENU,
  CHANGE_PACKAGE_OPTION_MENU
} from '../../reducers/menu/type'

export const isGetMenu = (status) => {
  console.log('isGetApi terpanggil')
  return {
    type: GET_MENU,
    payload: status
  }
}

export const setDataMenu = (data) => {
  console.log('setDataMenu terpanggil')
  return {
    type: SET_DATA_MENU,
    payload: data
  }
}

export const setRemakeItemsMenu = (remakeList) => {
  console.log('setRemakeItemsMenu terpanggil')
  return {
    type: REMAKE_ITEMS_MENU,
    payload: remakeList
  }
}

export const setCountItemsMenu = (index, countType) => {
  return (dispatch, getState) => {
    console.log('setCountItemsMenu terpanggil')
    const { remakeItemsMenu } = getState().menu
    const newCount = remakeItemsMenu[index]
    if (countType === '+') {
      newCount.count += 1
    } else if (countType === '-' && newCount.count > 0) {
      newCount.count -= 1
    }
    const newItems = new Set([...remakeItemsMenu, newCount])
    const result = [...newItems]
    dispatch({
      type: COUNT_ITEMS_MENU,
      payload: result
    })
  }
}

export const changePackageOption = (data) => {
  console.log('changePackageOption terpanggil')
  return {
    type: CHANGE_PACKAGE_OPTION_MENU,
    payload: data
  }
}

export const clearMenu = () => {
  return { type: CLEAR_MENU }
}
