import { ADD_TO_FAVORITE, REMOVE_FAVORITE, RESTORE_FAVORITE } from '../../reducers/favorite/type'
import { storeData, getData } from '../../../../util/LocalStorage'

const favoriteLocalStorage = async (category, data) => {
  let prevFavorites = {
    meals: [],
    salads: [],
    desserts: [],
    beverages: [],
    burgers: []
  }

  try {
    const resFavorites = await getData('favorites')
    if (resFavorites !== undefined) {
      prevFavorites = JSON.parse(resFavorites)
    }
  } catch (error) {
    console.log('error favoriteLocalStorage')
    console.log(error.message)
  }

  const newFavorites = {
    ...prevFavorites,
    [category]: data
  }

  storeData('favorites', newFavorites)
}

export const addToFavorite = (category, item) => {
  console.log('addToFavorite terpanggil')
  return (dispatch, getState) => {
    const favorites = getState().favorite
    const filterNewFavorite = favorites[category].filter(({ id }) => {
      return id !== item.id
    })
    const concatFavorite = [...filterNewFavorite, item]
    favoriteLocalStorage(category, concatFavorite)

    dispatch({
      type: ADD_TO_FAVORITE,
      payload: {
        key: category,
        value: concatFavorite
      }
    })
  }
}

export const removeFavorite = (id, category) => {
  console.log('removeFavorite terpanggil')
  return (dispatch, getState) => {
    const favorites = getState().favorite
    const newFavorites = favorites[category]
    newFavorites.splice(id, 1)

    favoriteLocalStorage(category, newFavorites)

    dispatch({
      type: REMOVE_FAVORITE,
      payload: {
        key: category,
        value: newFavorites
      }
    })
  }
}

export const restoreFavorite = (data) => {
  console.log('restoreFavorite terpanggil')
  return {
    type: RESTORE_FAVORITE,
    payload: data
  }
}
