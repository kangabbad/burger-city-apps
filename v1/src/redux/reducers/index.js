import { combineReducers } from 'redux'
import cart from './cart'
import menu from './menu'
import favorite from './favorite'

export default combineReducers({
  cart,
  menu,
  favorite
})
