import {
  GET_MENU,
  SET_DATA_MENU,
  REMAKE_ITEMS_MENU,
  CLEAR_MENU,
  COUNT_ITEMS_MENU,
  CHANGE_PACKAGE_OPTION_MENU
} from './type'
import imgBanner from '../../../assets/images/ourBurgerSlider.webp'

const initialState = {
  isLoadMenu: true,
  data: {
    items: [0, 0, 0],
    tags: [],
    thumbnail: imgBanner
  },
  remakeItemsMenu: [0, 0, 0]
}

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_MENU: {
      console.log('GET_MENU terpanggil')
      console.log('action.payload', action.payload)
      return {
        ...state,
        isLoadMenu: action.payload
      }
    }
    case SET_DATA_MENU: {
      console.log('SET_DATA_MENU terpanggil')
      return {
        ...state,
        data: action.payload
      }
    }
    case REMAKE_ITEMS_MENU:
    case COUNT_ITEMS_MENU:
    case CHANGE_PACKAGE_OPTION_MENU: {
      console.log(`${action.type} terpanggil`)
      return {
        ...state,
        remakeItemsMenu: action.payload
      }
    }
    case CLEAR_MENU: {
      console.log('CLEAR_MENU terpanggil')
      return {
        ...state,
        data: state.data,
        remakeItemsMenu: state.remakeItemsMenu
      }
    }
    default:
      return state
  }
}
