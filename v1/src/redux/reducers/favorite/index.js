import { ADD_TO_FAVORITE, REMOVE_FAVORITE, RESTORE_FAVORITE } from './type'

const initialState = {
  meals: [],
  salads: [],
  desserts: [],
  beverages: [],
  burgers: []
}

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_TO_FAVORITE:
    case REMOVE_FAVORITE:
      console.log(`${action.type} terpanggil`)
      return {
        ...state,
        [action.payload.key]: action.payload.value
      }
    case RESTORE_FAVORITE:
      console.log('RESTORE_FAVORITE terpanggil')
      return {
        ...state,
        ...action.payload
      }
    default:
      return state
  }
}
