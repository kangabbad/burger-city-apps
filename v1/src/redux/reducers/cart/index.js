import { ADD_TO_CART, CLEAR_CART } from './type'

const initialState = {
  meals: [],
  salads: [],
  desserts: [],
  beverages: [],
  burgers: [],
  payment: {
    order_method: 'delivery',
    delivery_address: {
      latitude: -7.9311342,
      longitude: 110.4314679,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121,
      province: 'Yogyakarta',
      regency: 'Bantul',
      district: 'Mangunan',
      detail_address: 'Jalan Mangunan, Sukorame, Mangunan',
      notes: ''
    },
    delivery_fee: 12000,
    subtotal: 60000,
    promo: {
      use_code: 'FREESANTRI',
      discount: 30000
    },
    total_payment: 30000,
    selected_wallet: 'cod'
  }
}

export default function (state = initialState, action) {
  switch (action.type) {
    case ADD_TO_CART: {
      return {
        ...state,
        [action.payload.key]: action.payload.value
      }
    }
    case CLEAR_CART: {
      return {
        ...action.payload
      }
    }
    default:
      return state
  }
}
