import React from 'react'
import PropTypes from 'prop-types'
import { Dimensions, LogBox } from 'react-native'
import FlashMessage from 'react-native-flash-message'

import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Provider as PaperProvider } from 'react-native-paper'
import EStyleSheet from 'react-native-extended-stylesheet'

import FirstScreen from './screens/FirstScreen'
import LoginScreen from './screens/LoginScreen'
import RegisterScreen from './screens/RegisterScreen'
import RegisterScreenNumber from './screens/RegisterNumberScreen'
import HomeScreen from './screens/Homescreen'
import ProfileScreen from './screens/ProfileScreen'
import SettingProfile from './screens/ProfileScreen/settingProfile'
import ForgetByPhone from './screens/ForgotPassword/ForgotByPhone'
import ForgetByEmail from './screens/ForgotPassword/ForgotByEmail'
import SendOTP from './screens/ForgotPassword/SendOTP'
import ResetPassword from './screens/ForgotPassword/ResetPassword'
import MenuPacket from './screens/MenuPacket'
import FavoriteScreen from './screens/FavoriteScreen'
import FinishOrderScreen from './screens/FinishOrderScreen'
import WebView from './screens/WebView'
import InputPhoneNumber from './screens/LoginWalletScreen/InputPhoneNumber'
import SendOTPWallet from './screens/LoginWalletScreen/SendOTPWallet'
import InputPinWallet from './screens/LoginWalletScreen/InputPinWallet'
import SavePhoneNumber from './screens/SavePhoneNumberScreen/SavePhoneNumber'
import SaveSendOTP from './screens/SavePhoneNumberScreen/SaveSendOTP'
import NotificationScreen from './screens/NotificationScreen'

import HomeIconInactive from '../src/assets/icons/home-icon.svg'
import HomeIconActive from '../src/assets/icons/home-icon-active.svg'
import OurBurgersIconInactive from '../src/assets/icons/our-burger-icon.svg'
import OurBurgersIconActive from '../src/assets/icons/our-burger-icon-active.svg'
import favoritesIconInactive from '../src/assets/icons/star-icon.svg'
import favoritesIconActive from '../src/assets/icons/star-icon-active.svg'
import ProfileIconInactive from '../src/assets/icons/profile-icon.svg'
import ProfileIconActive from '../src/assets/icons/profile-icon-active.svg'
import ChangePacket from './screens/ChangePacket'
import OurBurgerScreen from './screens/OursBurgerScreen'
import ChangeDrink from './screens/ChangeDrink'
import CartScreen from './screens/CartScreen'
import PaymentMethod from './screens/PaymentMethod'
import OrderConfirm from './screens/OrderConfirm'
import ChangeAddress from './screens/ChangeAddress'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const App = () => {
  const entireScreenWidth = Dimensions.get('window').width
  EStyleSheet.build({ $rem: entireScreenWidth / 320 })
  LogBox.ignoreLogs(['Setting a timer for a long period'])

  return (
    <PaperProvider>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='AuthStack'>
          <Stack.Screen
            name='AuthStack'
            component={AuthStack}
            options={{
              headerShown: false
            }}
          />
          <Stack.Screen
            name='InApp'
            component={InApp}
            options={{
              headerShown: false
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
      <FlashMessage position='bottom' />
    </PaperProvider>
  )
}

const AuthStack = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS
      }}
    >
      <Stack.Screen
        name='FirstScreen'
        component={FirstScreen}
        options={{
          headerShown: false
        }}
      />
      <Stack.Screen
        name='RegisterScreen'
        component={RegisterScreen}
        options={{
          headerShown: false
        }}
      />
      <Stack.Screen
        name='ForgetByPhone'
        component={ForgetByPhone}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='ForgetByEmail'
        component={ForgetByEmail}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SendOTP'
        component={SendOTP}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='ResetPassword'
        options={{ headerShown: false }}
        component={ResetPassword}
      />
      <Stack.Screen
        name='LoginScreen'
        component={LoginScreen}
        options={{
          headerShown: false
        }}
      />
      <Stack.Screen
        name='RegisterScreenNumber'
        component={RegisterScreenNumber}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='WebView'
        component={WebView}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

const InApp = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        ...TransitionPresets.SlideFromRightIOS
      }}
    >
      <Stack.Screen
        name='HomeTab'
        component={HomeTab}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='MenuPacket'
        component={MenuPacket}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='FavoriteScreen'
        component={FavoriteScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='FinishOrderScreen'
        component={FinishOrderScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='ChangePacket'
        component={ChangePacket}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='ChangeDrink'
        component={ChangeDrink}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='InputPhoneNumber'
        component={InputPhoneNumber}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SendOTPWallet'
        component={SendOTPWallet}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='InputPinWallet'
        component={InputPinWallet}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='CartScreen'
        component={CartScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='PaymentMethod'
        component={PaymentMethod}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='OrderConfirm'
        component={OrderConfirm}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='ChangeAddress'
        component={ChangeAddress}
        // options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SettingProfile'
        component={SettingProfile}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SavePhoneNumber'
        component={SavePhoneNumber}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='SaveSendOTP'
        component={SaveSendOTP}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name='NotificationScreen'
        component={NotificationScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  )
}

const allTab = [
  {
    name: 'HomeScreen',
    component: HomeScreen,
    label: 'Home',
    icon: {
      active: HomeIconActive,
      inactive: HomeIconInactive
    }
  },
  {
    name: 'OurBurgerScreen',
    component: OurBurgerScreen,
    label: 'Our Burgers',
    icon: {
      active: OurBurgersIconActive,
      inactive: OurBurgersIconInactive
    }
  },
  {
    name: 'favoriteScreen',
    component: FavoriteScreen,
    label: 'Favorites',
    icon: {
      active: favoritesIconActive,
      inactive: favoritesIconInactive
    }
  },
  {
    name: 'ProfileScreen',
    component: ProfileScreen,
    label: 'Profile',
    icon: {
      active: ProfileIconActive,
      inactive: ProfileIconInactive
    }
  }
]

const HomeTab = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#FF9F1C',
        style: {
          height: 60
        },
        labelStyle: {
          fontFamily: 'Nunito-SemiBold',
          fontSize: 10,
          marginBottom: 10
        },
        tabStyle: {
          marginTop: 10
        },
        keyboardHidesTabBar: true
      }}
    >
      {allTab.map((item, index) => (
        <Tab.Screen
          key={index}
          name={item.name}
          component={item.component}
          options={{
            tabBarLabel: item.label,
            // eslint-disable-next-line react/display-name
            tabBarIcon: ({ focused }) => {
              const Icon = focused ? item.icon.active : item.icon.inactive
              const Size = index === 2 ? 20 : index === 3 ? 24 : 20
              return (
                <Icon
                  width={Size}
                  height={Size}
                />
              )
            }
          }}
        />
      ))}
    </Tab.Navigator>
  )
}

HomeTab.propTypes = {
  // eslint-disable-next-line react/no-unused-prop-types
  focused: PropTypes.object
}

export default App
